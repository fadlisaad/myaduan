<?php
// Defines
define('ROOT_DIR', realpath(dirname(__FILE__)) .'/');
define('APP_DIR', ROOT_DIR .'application/');
require(APP_DIR.'config/config.php');
global $config;

$mysqli = new mysqli($config['db_host'], $config['db_username'], $config['db_password'], $config['db_name']);

$action = $_GET['action'];
if(isset($_GET['state_id'])){
	$state_id = $_GET['state_id'];
}
if(isset($_GET['sector_id'])){
	$sector_id = $_GET['sector_id'];
}
if(isset($_GET['category_id'])){
	$category_id = $_GET['category_id'];
}
switch ($action) {
	case 'sector':
		$sql = "SELECT id, title as text FROM complaint_sectors"; 
		break;

	case 'category':
		$sql = "SELECT id, title as text FROM complaint_categories WHERE sector_id = '$sector_id'"; 
		break;

	case 'subcategory':
		$sql = "SELECT id, title as text FROM complaint_subcategories WHERE sector_id = '$sector_id' AND category_id = '$category_id'"; 
		break;

	case 'type':
		$sql = "SELECT id, title as text FROM complaint_types WHERE sector_id = '$sector_id'"; 
		break;

	case 'country':
		$sql = "SELECT id, name AS text FROM countries"; 
		break;

	case 'staff':
		$sql = "SELECT id, concat(`full_name`,' [',`permission`,']') AS text FROM view_roles WHERE sector_id = '$sector_id'"; 
		break;

	case 'user':
		$sql = "SELECT id, concat(`full_name`,' [',`permission`,']') AS text FROM view_users WHERE permission != 'user'"; 
		break;

	case 'state':
		$sql = "SELECT id, title AS text FROM states"; 
		break;
	
	case 'city':
		$sql = "SELECT id, title AS text FROM cities WHERE state_id = '$state_id'"; 
		break;

	default:
		# code...
		break;
}
$result = $mysqli->query($sql);
$json = [];
while($row = $result->fetch_assoc()){
     $json[] = ['id'=>$row['id'], 'text'=>$row['text']];
}

echo json_encode($json);