<?php
/*
*  Title: Password Helper
*  Version: 1.0 from 13 September 2016
*  Author: Fadli Saad
*  Website: https://fadli.my
*/
class Password_helper {

	function resetPasswordLink($email)
	{
		global $config;
		$base_url = $config['base_url'];
		$hash = md5($config['pw_hash'].$email);
		return $base_url.'auth/self_reset_password/'.$hash;
	}

	function verifyEmailLink($email)
	{
		global $config;
		$base_url = $config['base_url'];
		return $base_url.'auth/verify/'.$email;
	}

	// random generated password
	public function randomPassword() 
	{
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		
		return implode($pass); //turn the array into a string
	}

}