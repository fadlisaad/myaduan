<?php
/*
*  Title: Pusher Notification Helper
*  Version: 1.0 from 29 January 2017
*  Author: Fadli Saad
*  Website: https://fadli.my
*/

Class Pusher_helper extends Controller {
	
	function send($data)
	{
		$options = array(
			'cluster' => 'ap1',
			'encrypted' => true
		);

		$pusher = new Pusher\Pusher(
			'a9e60cc998e026c009c8',
			'b1698a7054eff94434cd',
			'518205',
			$options
		);

		$push = array(
			'message' => $data['message'],
			'tracking_no' => $data['tracking_no'],
			'full_name' => $data['full_name'],
			'user_id' => $data['user_id']
		);

		// set user ID as channel
		$pusher->trigger($data['channel_id'], 'reply', $push);
	}
}