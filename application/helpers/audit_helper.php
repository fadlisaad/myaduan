<?php
/*
*  Title: Audit Helper
*  Version: 1.0 from 25 December 2016
*  Author: Fadli Saad
*  Website: https://fadli.my
*/
Class Audit_helper extends Controller {
	
	function create($action)
	{
		$session = $this->loadHelper('session_helper');
		$model = $this->loadModel('Audit_model');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}else{
			$data = array(
				'user_id' => $h_session->get('user_id'),
				'action' => $action
			);
			$model->add($data);
		}
	}
}