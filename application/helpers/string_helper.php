<?php
class String_helper
{
	function process($string) {
		$result = array();
		$result = preg_split('/[^a-zA-Z\d\s]/i', $string);

		foreach ($result as $res => $val) {
			$result[$res] = trim($val);
			if($val == "") unset($result[$res]);
		}

		return $result;
	}
}