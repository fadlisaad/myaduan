<?php
/*
*  Title: IMAP Helper
*  Version: 1.0 from 2 July 2017
*  Author: Fadli Saad
*  Website: https://fadli.my
*/

use SSilence\ImapClient\ImapClientException;
use SSilence\ImapClient\ImapConnect;
use SSilence\ImapClient\ImapClient as Imap;

class Imap_helper extends Controller {
	
	function connect($mailbox, $username, $password)
	{
		try{

		    $imap = new Imap($mailbox, $username, $password);
		    return $imap;

		}catch (ImapClientException $error){

		    echo $error->getMessage().PHP_EOL;
		    die();
		}
	}
	
}