<?php
/*
*  Title: Email Helper
*  Version: 1.0 from 29 January 2017
*  Author: Fadli Saad
*  Website: https://fadli.my
*/
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

Class Email_helper extends Controller {
	
	function send($data)
	{
		global $config;
		$email = $data['email'];
		$subject = $data['subject'];
		$content = $data['content'];

		$mail = new PHPMailer;

		$mail->isSMTP();
		$mail->Host = $config['smtp_host'];
		//$mail->SMTPDebug = 2;
		$mail->SMTPAuth = true;
		$mail->Username = $config['smtp_username'];
		$mail->Password = $config['smtp_password'];
		$mail->SMTPSecure = 'tls'; 
		$mail->Port = $config['smtp_port'];

		$mail->setFrom($config['from_email'], $config['from_name']);
		$mail->addAddress($email);
		$mail->IsHTML(true);
		$mail->Subject = $subject;
		$mail->Body    = $content;
		$mail->AltBody = "Please use a compatible email viewer to display this email.";

		# Attachments
		if(isset($data['attachment'])) $mail->addAttachment('/tmp/'.$data['attachment']); 
		
		if(!$mail->send()) {
			$msg = false;
		} else {
			$msg = true;
 		}
 		return $msg;
	}
}