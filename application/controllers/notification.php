<?php

class Notification extends Controller {
	
	function create()
	{
		$model = $this->loadModel('Notification_model');
		if(empty($_SESSION['loggedin'])){
			$this->redirect('auth/login');
		}else{
			$data = array(
				'user_id' => $_POST['user_id'],
				'notify_type' => $_POST['type'],
				'title' => $_POST['title'],
				'content' => $_POST['content']
				'status' => 'unread'
			);
			$model->add($data);
		}
	}
	
}