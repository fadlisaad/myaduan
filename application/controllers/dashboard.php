<?php
/*
*  Title: Dashboard Controller
*  Version: 1.0 from 28 July 2016
*  Author: Fadli Saad
*  Website: https://fadli.my
*/

class Dashboard extends Controller {

	function index()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$template = $this->loadView('dashboard');
		$model = $this->loadModel('Dashboard_model');

		$count_daily_open = $model->countDaily('open', 'open_date', $session->get('permission'), $session->get('user_id'));
		$count_daily_draft = $model->countDaily('draft', 'last_update', $session->get('permission'), $session->get('user_id'));
		$count_daily_processing = $model->countDaily('processing', 'last_update', $session->get('permission'), $session->get('user_id'));
		$count_daily_submitted = $model->countDaily('submitted', 'last_update', $session->get('permission'), $session->get('user_id'));
		$count_daily_closed = $model->countDaily('closed', 'close_date', $session->get('permission'), $session->get('user_id'));

		$template->set('count_daily_open', $count_daily_open);
		$template->set('count_daily_draft', $count_daily_draft);
		$template->set('count_daily_processing', $count_daily_processing);
		$template->set('count_daily_submitted', $count_daily_submitted);
		$template->set('count_daily_closed', $count_daily_closed);

		$count_total_open = $model->countTotal('open', $session->get('permission'), $session->get('user_id')); 
		$count_total_draft = $model->countTotal('draft', $session->get('permission'), $session->get('user_id'));
		$count_total_processing = $model->countTotal('processing', $session->get('permission'), $session->get('user_id'));
		$count_total_submitted = $model->countTotal('submitted', $session->get('permission'), $session->get('user_id'));
		$count_total_closed = $model->countTotal('closed', $session->get('permission'), $session->get('user_id'));
		$total = $model->countTotalTicket();
		$total_user = $model->countTotalUser();
		$user = $model->countUser();

		$template->set('count_total_open', $count_total_open);
		$template->set('count_total_draft', $count_total_draft);
		$template->set('count_total_processing', $count_total_processing);
		$template->set('count_total_submitted', $count_total_submitted);
		$template->set('count_total_closed', $count_total_closed);
		$template->set('total', $total);
		$template->set('total_user', $total_user);
		$template->set('user', $user);

		$count_staff_open = $model->countStaffRow($session->get('permission'), $session->get('user_id'));
		$template->set('count_staff_open', $count_staff_open);

		$template->render();
	}
	
}