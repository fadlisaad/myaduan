<?php
/*
*  Title: Aunthentication Controller
*  Version: 1.0 from 28 July 2016
*  Author: Fadli Saad
*  Website: https://fadli.my
*/

class Auth extends Controller {

	// Registration page
	public function register()
	{
		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
			'assets/css/jquery.loading.min.css'
		);

		$js = array(
			'assets/plugins/select2/select2.min.js',
			'assets/plugins/maskedinput/jquery.maskedinput.min.js',
			'assets/js/validator.min.js',
			'assets/js/jquery.loading.min.js'
		);

		$custom_js = "<script>

			var country_url = '".BASE_URL."search.php?action=country';
			var state_url = '".BASE_URL."search.php?action=state';

			$(document).ready(function() {

				$('#notice').modal('show');

				$('.select2').select2({
					theme: 'bootstrap'
				});

				$('#email').change(function(){

					var email = $(this).val();
					var check_url = '".BASE_URL."auth/check_email';
					form_data = 'email='+ email;
					
					$.ajax({
						type: 'POST',
						dataType: 'html',
						url: check_url,
						data: form_data,
						success: function(responseText) {
	                        if(responseText == 1) {
								swal('Oops', 'The email is already registered', 'error');
								$(this).val('');
	                        } else {
	                            if(responseText == 0) {
	                                swal('Yeah!', 'The email is valid', 'info');
	                            }
	                        }
	                    }
				    });
				});

				$('#country_id').select2({
					placeholder: 'Choose country',
					theme: 'bootstrap',
					allowClear: true,
					cache: true,
					delay: 250,
				    ajax: {
				        url: country_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#state').select2({
					placeholder: 'Choose state',
					theme: 'bootstrap',
					allowClear: true,
					cache: true,
				    ajax: {
				        url: state_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#state').change(function(){

					var state_id = $(this).val();
					var city_url = '".BASE_URL."search.php?action=city&state_id='+state_id;

					$('#city').select2({
						placeholder: 'Choose nearest city',
						theme: 'bootstrap',
						allowClear: true,
						cache: true,
						delay: 250,
					    ajax: {
					        url: city_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				// hide passport by default
				$('#passport').hide();

				// ic masking
				$('#ic_passport').mask('999999-99-9999',{
					placeholder: 'XXXXXX-XX-XXXX'
				});

				$('#state-other').hide();
				$('#city-other').hide();

				// on change nationality, toggle required field
				$('#country_id').change(function(){

					var country =  $('#country_id').val();
					if(country != '135'){

						$('#passport').show();
						$('#ic').hide();
						$('#phone-no').mask('+999999?99999');
						$('#state-my').hide().val('');
						$('#city-my').hide().val('');
						$('#ethnicity-my').hide().val('');
						$('#state-other').show();
						$('#city-other').show();
						$('#income-my').hide().val('');

					}else{

						$('#passport').hide().val('');
						$('#ic').show();
						$('#state-my').show();
						$('#city-my').show();
						$('#income-my').show();
						$('#state-other').hide().val('');
						$('#city-other').hide().val('');
						$('#ethnicity-my').show();
					}
				});

				function submitRegistration(status){

					$('body').loading('start');
					var submit_url = '".BASE_URL."auth/process_register';
					form_data = $('form#register').serialize();
					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: submit_url,
						data: form_data
				    })
				}

				$('#register').validator().on('submit', function (e) {

					if (e.isDefaultPrevented()) {

						// handle the invalid form...

						swal('Oops', 'We could not complete the action! Some field are required field. Please fill them accordingly.', 'error');

					} else {

						e.preventDefault();

						submitRegistration()
						.done(function(data){
							$('body').loading('stop');
							swal({
								title: 'Success',
								text: 'The registration is successfully submitted. Please check your email for registration details.',
								type: 'success'
							},function(){
								window.location.href = '".BASE_URL."';
							});
						})
						.error(function(){
							swal('Oops', 'We could not complete the action!', 'error');
						});
					}
				});

			});
		</script>";
		

		$header = $this->loadView('header');
		$footer = $this->loadView('footer');
        $template = $this->loadView('register');

		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);

		$header->render();
		$template->render();
		$footer->render();
	}
	
	// Process registration
	public function process_register()
	{
		global $config;
		$model = $this->loadModel('Auth_model');

		# load PHP filter helper
		$filter = $this->loadHelper('filter_helper');
			
		$userdata = array(
			'email' => $_POST['email'],
			'password' => $_POST['password'], 
			'permission' => 'user'
		);

		$model->processRegisterUser($userdata);
		$user = $model->lastInsertedID();

		if($_POST['country_id'] == '135'){

			$ethnicity = $_POST['ethnicity'];
			$income = $_POST['income'];
			$city_id = $filter->isInt($_POST['city_id']);
			$state_id = $filter->isInt($_POST['state_id']);
			$address2 = $_POST['address2'];

		}else{

			$ethnicity = 'Others';
			$income = 'No income';
			$city_id = '650'; // Others
			$state_id = '17';
			$address2 = $_POST['address2'].' '.$_POST['city_id'].' '.$_POST['state_id'];
		}

		$contactdata = array(
			'user_id' => $user[0]['id'],
			'full_name' => $_POST['full_name'], 
			'age' => $_POST['age'], 
			'ic_passport' => $_POST['ic_passport'], 
			'ethnicity' => $ethnicity, 
			'income' => $income, 
			'address1' => $_POST['address1'], 
			'address2' => $address2, 
			'postcode' => $_POST['postcode'], 
			'city_id' => $city_id, 
			'state_id' => $state_id, 
			'country_id' => $_POST['country_id'],
			'phone_no' => $_POST['phone_no']
		);

		$model->processRegisterContact($contactdata);
		
		// load mailer
		$email = $this->loadHelper('Email_helper');
		$verify = $this->loadHelper('Password_helper');
		
		// choose email template
		$e_model = $this->loadModel('Mailer_model');
		$template = $e_model->listSingle(2);
		$body = $template[0]['body'];
		$subject = $template[0]['subject'];

		$full_name = $_POST['full_name'];
		$verify_link = $verify->verifyEmailLink($_POST['email']);

		$vars = array(
			"{{EMAIL}}" => $_POST['email'],
			"{{FULLNAME}}" => $full_name,
			"{{PASSWORD}}" => $_POST['password'],
			"{{VERIFY_LINK}}" => $verify_link,
		);

		$content = strtr($body, $vars);

		$email_data = array(
			'email' => $_POST['email'], 
			'subject' => $subject, 
			'content' => $content 
		);

		if($email->send($email_data)){
			echo "Registered and email sent.";
		}else{
			echo "Registered but Failed to send email with error.";
		}
	}

	// Process registration
	public function process_first_login()
	{
		global $config;
		$model = $this->loadModel('Auth_model');

		if(isset($_POST['first_time'])){

			$data['email'] = $_POST['email'];
			$user = $model->getuser($data);

			$userpass = array(
				'id' => $user[0]['id'], 
				'password' => $_POST['password'], 
			);

			$model->resetPassword($userpass);

			$contactdata = array(
				'user_id' => $user[0]['id'],
				'full_name' => $_POST['full_name'], 
				'age' => $_POST['age'], 
				'ic_passport' => $_POST['ic_passport'], 
				'ethnicity' => 'Others', 
				'income' => 'No income', 
				'address1' => $_POST['address1'], 
				'address2' => $_POST['address2'], 
				'postcode' => $_POST['postcode'], 
				'city_id' => $_POST['city_id'], 
				'state_id' => $_POST['state_id'], 
				'country_id' => 135,
				'phone_no' => $_POST['phone_no']
			);

			$model->processRegisterContact($contactdata);
			
			$this->redirect('auth/verify/'.$_POST['email']);

		}else{
			die('Error!');
		}
	}

	// Verification page
	public function verify()
	{
		$email = $this->loadHelper('Email_helper');

		$header = $this->loadView('header');
		$footer = $this->loadView('footer');
        $template = $this->loadView('verify');

		$header->render();
		$template->render();
		$footer->render();
	}

	// Login page
	public function login()
	{
		$template = $this->loadView('login');
		$template->render();
	}
	
	// Process login
	public function process_login()
	{
		$model = $this->loadModel('Auth_model');

		if(isset($_POST['submit'])){
			
			$data = array(
				'email' => $_POST['email'],
				'password' => $_POST['password']
			);
			$return = $model->processLogin($data);
			
			if(!is_array($return)){

				$msg = array(
					'error_msg' => $return,
					'error_type' => 'danger',
					'error_code' => '300'
				);
				
				$header = $this->loadView('header');
				$footer = $this->loadView('footer');
		        $template = $this->loadView('error/view');
				$template->set('data', $msg);

				$header->render();
				$template->render();
				$footer->render();
			}else{
				$user = $model->getUser($data);

				$session = $this->loadHelper('session_helper');
				$session->set('loggedin', '1');
				$session->set('email', $user[0]['email']);
				$session->set('password', '*.RsuD(TX=%{');
				$session->set('user_id', $user[0]['id']);
				$session->set('full_name', $user[0]['full_name']);
				$session->set('user_email', $_POST['email']);
				$session->set('permission', $user[0]['permission']);
				$session->set('avatar', $user[0]['avatar']);
				$this->redirect('dashboard/index');
			}

		}else{
			die('Error!');
		}
	}

	// First time Login page
	public function first_login()
	{
		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
		);

		$js = array(
			'assets/plugins/select2/select2.min.js',
			'assets/plugins/maskedinput/jquery.maskedinput.min.js',
		);

		$custom_js = "<script>
			var country_url = '".BASE_URL."search.php?action=country';
			var state_url = '".BASE_URL."search.php?action=state';

			$(document).ready(function() {

				$('.select2').select2({
					theme: 'bootstrap'
				});

				$('#country_id').select2({
					placeholder: 'Choose country',
					theme: 'bootstrap',
					delay: 250,
				    ajax: {
				        url: country_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#state').select2({
					placeholder: 'Choose state',
					theme: 'bootstrap',
				    ajax: {
				        url: state_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#state').change(function(){

					var state_id = $(this).val();
					var city_url = '".BASE_URL."search.php?action=city&state_id='+state_id;

					$('#city').select2({
						placeholder: 'Choose nearest city',
						theme: 'bootstrap',
						delay: 250,
					    ajax: {
					        url: city_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				// hide passport by default
				$('#passport').hide();

				// on change nationality, toggle passport and IC
				$('#country_id').change(function(){
					var country =  $('#country_id').val();
					console.log(country);
					if(country != '135'){
						$('#passport').show();
						$('#ic').hide();
					}else{
						$('#passport').hide();
						$('#ic').show();
					}
				});

				// ic masking
				$('#ic_passport').mask('999999-99-9999',{
					placeholder: 'XXXXXX-XX-XXXX'
				});

				$('#postcode').mask('99999');

				// PHONE masking
				$('#phone_no').mask('+6019999?99999');

			});
		</script>";

		$header = $this->loadView('header');
		$footer = $this->loadView('footer');
        $template = $this->loadView('first_login');

        $header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);

		$header->render();
		$template->render();
		$footer->render();
	}

	// Reset page
	public function reset_password()
	{
		$template = $this->loadView('reset_password');
		$template->render();
	}

	// check if user exist
	public function check_user($email)
	{
		$model = $this->loadModel('Auth_model');
		$check = $model->checkEmail($email);

		if($check){
			// user exist, do not proceed with registration
			http_response_code(400);
		}else{
			// proceed
			http_response_code(200);
		}
	}

	// Reset password function
	public function process_reset_password()
	{
		$model = $this->loadModel('Auth_model');

		if(isset($_POST)){
			
			$data = array(
				'email' => $_POST['email']
			);

			$check = $model->checkEmail($data);

			if($check){
				// load PhpMailer
				$email = $this->loadHelper('Email_helper');
				$reset = $this->loadHelper('Password_helper');
				
				// choose email template
				$e_model = $this->loadModel('Mailer_model');
				$template = $e_model->listSingle(1);
				$body = $template[0]['body'];
				$subject = $template[0]['subject'];

				$user = $model->getUser($data);

				$full_name = $user[0]['full_name'];
				$reset_link = $reset->resetPasswordLink($_POST['email']);

				$vars = array(
					"{{EMAIL}}" => $_POST['email'],
					"{{FULLNAME}}" => $full_name,
					"{{RESET_LINK}}" => $reset_link,
				);

				$content = strtr($body, $vars);

				$email_data = array(
					'email' => $_POST['email'], 
					'subject' => $subject, 
					'content' => $content 
				);

				// store hash
				$hash = basename($reset_link);
				$hash_data = array(
					'user_id' => $user[0]['id'],
					'email' => $_POST['email'],
					'password_hash' => $hash
				);

				if($model->storeHash($hash_data) == false){
					echo('New hash stored!');
				}else{
					die('Failed to save new hash.');
				}

				$email->send($email_data);

				$this->redirect('site/page/reset-password-success');
				
			}else{
				$this->redirect('site/page/no-email-account');
			}
			
		}else{
			die('Error 102: Unable to reset password.');
		}
	}

	// self reset password
	public function self_reset_password()
	{
		$model = $this->loadModel('Auth_model');
		$hash = basename($_SERVER['REQUEST_URI']);

		// check and verify hash
		$check = $model->checkHash($hash);

		if ($check){
			// reset password
			$reset = $this->loadHelper('Password_helper');
			$new_password = $reset->randomPassword();

			$check_data = array(
				'id' => $check[0]['user_id'],
				'password' => $new_password
			);
			$model->resetPassword($check_data);

			$header = $this->loadView('header');
			$footer = $this->loadView('footer');
	        $template = $this->loadView('reset_done');
			$template->set('password', $new_password);

			$header->render();
			$template->render();
			$footer->render();
			
		}else{
			$this->redirect('site/page/password-hash-404');
		}
	}
	
	public function logout()
	{
		$session = $this->loadHelper('session_helper');
		$session->destroy();
		$this->redirect('site');
	}
}