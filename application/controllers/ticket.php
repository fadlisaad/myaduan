<?php
/**
* Ticket Controller
*
* Base controller for ticket system
*
* @package    	NCCC
* @subpackage 	Ticket
* @author 		Fadli Saad (https://fadli.my)
*/

class Ticket extends Controller {

	/**
	* List all tickets
	*
	* @return 	Return list of ticket based on incidents or null if not found
	*/

	function index()
	{
		$session = $this->loadHelper('session_helper');
		
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/datatables/jquery.dataTables.min.css',
			'assets/plugins/datatables/buttons.bootstrap.min.css',
			'assets/plugins/datatables/responsive.bootstrap.min.css',
		);

		$js = array(
			'assets/plugins/datatables/media/js/jquery.dataTables.min.js',
			'assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
			'assets/plugins/datatables/dataTables.responsive.min.js',
			'assets/plugins/datatables/responsive.bootstrap.min.js',
			'assets/pages/datatables.init.js'
		);

		/**
		* Custom script
		*/
		$custom_js = "<script type=\"text/javascript\">

			var active_url = '".BASE_URL."ticket/process';
			var user_id = '".$session->get('user_id')."';
			var columnOrder = 'desc';
			var permission = '".$session->get('permission')."';
			
			$(document).ready(function(){
				
    			var table = $('#table').dataTable({
    				responsive: true,
    				serverSide : true,
    				processing : true,
    				ajax : {
    					url : active_url,
    					type : 'POST'
    				},
    				deferRender : true,
    				error : true,
    				columns: [
    					{ data: 'user_id' },
			            { data: 'tracking_no' },
			            { data: 'company_name' },
			            { data: 'complaint_date' },
			            { data: 'incident_date' },
			            { data: 'incident_time' },
			            { data: 'estimated_loss' },
			            { data: 'status' },
			            { data: 'view' },
			            { data: 'unread' }
			        ],
			        columnDefs: [
			            {
			                targets: [ 0 ],
			                visible: false,
			                searchable: true
			            },
			            { 
			            	responsivePriority: 1,
			            	targets: 0 
			            },
			            { 
			            	responsivePriority: 2,
			            	targets: -1
			            },
			        ],
			        order : [[ 3, columnOrder ]]
    			});

    			if(permission == 'user'){
    				table.fnFilter(user_id, 0);
    			}

    			$('#table_info, #table_length').hide();
			});

		</script>";

		/**
		* Load and render template
		*/
		$helper = $this->loadHelper('date_helper');

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('ticket/index');
		
		$template->set('helper', $helper);
		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}

	function admin()
	{
		$session = $this->loadHelper('session_helper');
		
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/datatables/jquery.dataTables.min.css',
			'assets/plugins/datatables/buttons.bootstrap.min.css',
			'assets/plugins/datatables/responsive.bootstrap.min.css',
		);

		$js = array(
			'assets/plugins/datatables/media/js/jquery.dataTables.min.js',
			'assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
			'assets/plugins/datatables/dataTables.responsive.min.js',
			'assets/plugins/datatables/responsive.bootstrap.min.js',
			'assets/pages/datatables.init.js'
		);

		/**
		* Custom script
		*/
		$custom_js = "<script type=\"text/javascript\">

			var active_url = '".BASE_URL."ticket/process_admin';
			var columnOrder = 'desc';
			
			$(document).ready(function(){
				
    			var table = $('#table').dataTable({
    				responsive: true,
    				serverSide : true,
    				processing : true,
    				ajax : {
    					url : active_url,
    					type : 'POST'
    				},
    				deferRender : true,
    				error : true,
    				columns: [
			            { data: 'id' },
			            { data: 'tracking_no' },
			            { data: 'complaint_date' },
			            { data: 'company_name' },
			            { data: 'full_name' },
			            { data: 'phone_no' },
			            { data: 'ic_passport' },
			            { data: 'email' },
			            { data: 'status' },
			            { data: 'view' },
			            { data: 'unread' }
			        ],
			        columnDefs: [
			            { 
			            	responsivePriority: 1,
			            	targets: 0,
			            	visible: false,
			            	searchable: false
			            },
			            { 
			            	responsivePriority: 2,
			            	targets: -1
			            }
			        ],
			        order : [[ 0, columnOrder ]]
    			});
			});

		</script>";

		/**
		* Load and render template
		*/
		$helper = $this->loadHelper('date_helper');
		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('ticket/admin');
		
		$template->set('helper', $helper);
		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}

	function assign($complaint_id)
	{
		$session = $this->loadHelper('session_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
			'assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css'
		);

		$js = array(
			'assets/plugins/select2/select2.min.js',
			'assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js'
		);

		$custom_js = "<script type=\"text/javascript\">
			var state_url = '".BASE_URL."search.php?action=state';
			
			$(document).ready(function() {                       

				$('#priority').select2({
					theme: 'bootstrap'
				});

				$('#state_id').select2({
					placeholder: 'Choose state',
					theme: 'bootstrap',
				    ajax: {
				        url: state_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#state_id').change(function(){

					var state_id = $(this).val();
					var city_url = '".BASE_URL."search.php?action=city&state_id='+state_id;

					$('#city_id').select2({
						placeholder: 'Choose nearest city',
						theme: 'bootstrap',
					    ajax: {
					        url: city_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				var sector_url = '".BASE_URL."search.php?action=sector';

				$('#sector_id').select2({
					placeholder: 'Choose sector',
					theme: 'bootstrap',
					delay: 250,
				    ajax: {
				        url: sector_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#sector_id').on('change', function () {

					sector_id = $(this).val();
					console.log('sector_id:'+sector_id);

					var category_url = '".BASE_URL."search.php?action=category&sector_id='+sector_id;

					$('#category_id').select2({
						placeholder: 'Chooose category',
						theme: 'bootstrap',
					    ajax: {
					        url: category_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});

					var staff_url = '".BASE_URL."search.php?action=staff&sector_id='+sector_id;
					$('#assigned_to').select2({
						placeholder: 'Choose officer',
						theme: 'bootstrap',
					    ajax: {
					        url: staff_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});

				});

				$('#category_id').on('change', function () {
					sector_id = $('#sector_id').val();
					category_id = $(this).val();
					console.log('category_id:'+category_id);
					var subcategory_url = '".BASE_URL."search.php?action=subcategory&sector_id='+sector_id+'&category_id='+category_id;

					$('#subcategory_id').select2({
						placeholder: 'Chooose sub category',
						theme: 'bootstrap',
					    ajax: {
					        url: subcategory_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				$('#subcategory_id').on('change', function () {
					sector_id = $('#sector_id').val();
					console.log('sector_id:'+sector_id);

					var type_url = '".BASE_URL."search.php?action=type&sector_id='+sector_id;

					$('#type_id').select2({
						placeholder: 'Choose complaint type',
						theme: 'bootstrap',
						delay: 250,
					    ajax: {
					        url: type_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				function createTicket(){
					var create_url = '".BASE_URL."ticket/create';
					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: create_url,
						data: $('form.create-ticket').serialize()
				    })
				}

				$('button#submit-create-ticket').bind('click', function(e){
					e.preventDefault(); // Prevent Default Submission
					createTicket()
					.done(function(data){
						swal({
							title: 'Success',
							text: 'The record is successfully updated.',
							type: 'success'
						},function() {
							//close modal
						});
					})
					.error(function(){
						swal('Oops', 'We could not complete the action!', 'error');
					});
					
				});

				function uploadFile(){
					var upload_url = '".BASE_URL."complaint/attachment';
					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: upload_url,
						data: $('form.upload-file').serialize()
				    })
				}

				$('button#upload-file').bind('click', function(e){
					e.preventDefault(); // Prevent Default Submission
					uploadFile()
					.done(function(data){
						swal({
							title: 'Success',
							text: 'The file is successfully attached.',
							type: 'success'
						},function() {
							//close modal
						});
					})
					.error(function(){
						swal('Oops', 'We could not complete the action!', 'error');
					});
					
				});

			});
        </script>";

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$template = $this->loadView('ticket/assign');
		$footer = $this->loadView('footer');
		$model = $this->loadModel('Complaint_model');
		$complaint_data= $model->listSingle($complaint_id);

		$header->set('css', $css);
		$template->set('session', $session);
		$template->set('complaint', $complaint_data);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);

		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}

	function create()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		//  create ticket
		$model = $this->loadModel('Ticket_model');
		$data = array(
			'tracking_no' => $_POST['tracking_no'],
			'complaint_id' => $_POST['complaint_id'],
			'assigned_by' => $_POST['assigned_by'],
			'assigned_date' => $_POST['assigned_date'],
			'assigned_to' => $_POST['assigned_to'],
			'open_date' => $_POST['open_date'],
			'sector_id' => $_POST['sector_id'],
			'category_id' => $_POST['category_id'],
			'subcategory_id' => $_POST['subcategory_id'],
			'type_id' => $_POST['type_id'],
			'priority' => $_POST['priority']
		);
		$model->insertTicket($data);

		# update complaint status to open
		$c_model = $this->loadModel('Complaint_model');
		$c_data = array(
			'status' => 'open'
		);
		$c_model->editRecord($c_data, $_POST['complaint_id']);

		# send email to notify admin on new email created
		global $config;

		$email = $this->loadHelper('Email_helper');
		$e_model = $this->loadModel('Mailer_model');
		$template = $e_model->listSingle(5);
		$body = $template[0]['body'];
		$subject = $template[0]['subject'];

		$user = $model->getUser($_POST['id']);

		$vars = array(
			"{{EMAIL}}" => $admin[0]['email'],
			"{{TRACKING_LINK}}" => $config['base_url'].'auth/login',
		);

		$content = strtr($body, $vars);

		$email_data = array(
			'email' => $admin[0]['email'], 
			'subject' => $subject, 
			'content' => $content 
		);

		$check_send = $email->send($email_data);

		if($check_send == true){
			echo('New ticket E-mail sent!');
		}else{
			die('Failed to send new e-mail ticket.');
		}
	}

	function createMessage()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		//  create message
		$model = $this->loadModel('Ticket_model');
		$data = array(
			'ticket_id' => $_POST['ticket_id'],
			'user_id' => $_POST['user_id'],
			'message' => $_POST['message']
		);
		
		$model->insertTicketMessage($data);
	}

	/**
	* Edit new tickets
	*
	* @param int $id Ticket ID
	*/
	function edit($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
			'assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css'
		);

		$js = array(
			'assets/plugins/timer/timer.jquery.min.js',
			'assets/plugins/select2/select2.min.js',
			'assets/js/moment.js',
			'assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js',
			'assets/plugins/maskedinput/jquery.maskedinput.min.js',
			'assets/js/ticket.js'
		);

		# get the sector first
		$ticket_model = $this->loadModel('Ticket_model');
		$ticket = $ticket_model->listSingle($id);

		switch ($session->get('permission')) {
			case 'user':
				$disable_action = "#assigned_to, #status";
				$disable_button = "button#submit-update-ticket, button#submit-status-ticket";
				$readonly_input = "";
				$redirect_url = BASE_URL."complaint/edit/".$ticket[0]['complaint_id'];
				$channel_id = $ticket[0]['assigned_to'];
				break;
			case 'trainee':
				$disable_action = '#assigned_to';
				$disable_button = '';
				$readonly_input = "";
				$redirect_url = BASE_URL."ticket/edit/".$id;
				$channel_id = $ticket[0]['user_id'];
				break;
			case 'officer':
				$disable_action = '#assigned_to';
				$disable_button = '';
				$readonly_input = "";
				$redirect_url = BASE_URL."ticket/edit/".$id;
				$channel_id = $ticket[0]['user_id'];
				break;
			case 'admin':
				$disable_action = '';
				$disable_button = '#timer';
				$readonly_input = 'input';
				$redirect_url = BASE_URL."dashboard";
				$channel_id = $ticket[0]['user_id'];
				break;
			default:
				$disable_action = '';
				$disable_button = '';
				$readonly_input = "";
				$redirect_url = BASE_URL."dashboard";
				$channel_id = $ticket[0]['user_id'];
				break;
		}
		
		# if user id equal to assigned_by, change status to processing and update open date
		$sector_id = $ticket[0]['sector_id'];
		$category_id = $ticket[0]['category_id'];
		$subcategory_id = $ticket[0]['subcategory_id'];
		$type_id = $ticket[0]['type_id'];
		$time_worked = $ticket[0]['time_worked'];
		$status = $ticket[0]['status'];

		$custom_css = "<style>
			select[readonly].select2 + .select2-container {
				pointer-events: none;
				touch-action: none;

				.select2-selection {
					background: #eee;
					box-shadow: none;
				}

				.select2-selection__arrow, .select2-selection__clear {
					display: none;
				}

			}

			.dl-horizontal dt {
				width: 100px;
			}

			.dl-horizontal dd {
				margin-left: 110px;
			}

			 .dl-larger dt {
			 	width: 200px;
			 }

			 .dl-larger dd {
				margin-left: 220px;
			}

			.chat-avatar {
				width: 40px !important;
			}
		</style>";

		$custom_js = "<script type=\"text/javascript\">

			var state_url = '".BASE_URL."search.php?action=state';
			var staff_url = '".BASE_URL."search.php?action=staff';
			var redirect_url = '".$redirect_url."';
			var disable_action = '".$disable_action."';
			var disable_button = '".$disable_button."';
			var readonly_input = '".$readonly_input."';
			var sector_id = '".$sector_id."';
			var category_id = '".$category_id."';
			var subcategory_id = '".$subcategory_id."';
			var type_id = '".$type_id."';
			
			$(document).ready(function() {

				if(disable_action != ''){
					$(disable_action).attr('readonly', true);
				}

				if(readonly_input != ''){
					$(readonly_input).prop('readonly', true);
				}

				if(disable_button != ''){
					$(disable_button).addClass('hide');
				}

				$('.select2').select2({
					theme: 'bootstrap',
					allowClear: true,
					cache: true,
				});

				var staff_url = '".BASE_URL."search.php?action=staff&sector_id='+sector_id;
				$('#assigned_to').select2({
					placeholder: 'Choose officer/trainee',
					theme: 'bootstrap',
					allowClear: true,
					cache: true,
				    ajax: {
				        url: staff_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				var sector_url = '".BASE_URL."search.php?action=sector';

				$('#sector_id').select2({
					placeholder: 'Choose sector',
					theme: 'bootstrap',
					allowClear: true,
					cache: true,
					delay: 250,
				    ajax: {
				        url: sector_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				var category_url = '".BASE_URL."search.php?action=category&sector_id='+sector_id;

				$('#category_id').select2({
					placeholder: 'Chooose category',
					theme: 'bootstrap',
					allowClear: true,
					cache: true,
				    ajax: {
				        url: category_url,
				        dataType: 'json',
				        processResults: function (data) {                                        
				            return {
				            	results: data
				            };                                            
				        },
				    }
				});

				$('#sector_id').on('change', function () {

					// clear other field
					$('#category_id').val('');
					$('#subcategory_id').val('');
					$('#type_id').val('');

					sector_id = $(this).val();

					category_url = '".BASE_URL."search.php?action=category&sector_id='+sector_id;

					$('#category_id').select2({
						placeholder: 'Chooose category',
						theme: 'bootstrap',
						allowClear: true,
						cache: true,
					    ajax: {
					        url: category_url,
					        dataType: 'json',
					        processResults: function (data) {                                                
					            return {
					            	results: data
					            };
					        },
					    }
					});
					$('#subcategory_id').select2({
						placeholder: 'Chooose sub category',
						theme: 'bootstrap',
						allowClear: true,
						cache: true
					});
					$('#type_id').select2({
						placeholder: 'Chooose Type',
						theme: 'bootstrap',
						allowClear: true,
						cache: true
					}); 

				});

				var subcategory_url = '".BASE_URL."search.php?action=subcategory&sector_id='+sector_id+'&category_id='+category_id;

				$('#subcategory_id').select2({
					placeholder: 'Chooose sub category',
					theme: 'bootstrap',
				    ajax: {
				        url: subcategory_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#category_id').on('change', function () {

					sector_id = $('#sector_id').val();
					category_id = $(this).val();                                        

					subcategory_url = '".BASE_URL."search.php?action=subcategory&sector_id='+sector_id+'&category_id='+category_id;

					$('#subcategory_id').select2({
						placeholder: 'Chooose sub category',
						theme: 'bootstrap',
						allowClear: true,
						cache: true,
					    ajax: {
					        url: subcategory_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				var type_url = '".BASE_URL."search.php?action=type&sector_id='+sector_id;

				$('#type_id').select2({
					placeholder: 'Choose complaint type',
					theme: 'bootstrap',
					allowClear: true,
					cache: true,
					delay: 250,
				    ajax: {
				        url: type_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#subcategory_id').on('change', function () {
					sector_id = $('#sector_id').val();

					type_url = '".BASE_URL."search.php?action=type&sector_id='+sector_id;

					$('#type_id').select2({
						placeholder: 'Choose complaint type',
						theme: 'bootstrap',
						allowClear: true,
						cache: true,
						delay: 250,
					    ajax: {
					        url: type_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				function updateTicket(){
                   	var sector_id= $('#sector_id').val();
					var category_id= $('#category_id').val();
					var subcategory_id= $('#subcategory_id').val();
					var type_id= $('#type_id').val(); 

					if(sector_id == null){
                   		sweetAlert('Please Select Sector');
                   		return false;
               		}
               		else if(category_id == null){                    
                   		sweetAlert('Please Select Category');
                   		return false;
               		}
               		else if(subcategory_id == null){                    
                   		sweetAlert('Please Select Subcategory');
                   		return false;
               		}
               		else if(type_id == null){                    
                   		sweetAlert('Please Select Complaint Type!');
                   		return false;
               		}
                    else{
					var update_url = '".BASE_URL."ticket/update';
					var mohit = $('form.update-ticket').serialize();
					
					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: update_url,
						data: $('form.update-ticket').serialize()
				    })
				}
				}

				$('button#submit-update-ticket').bind('click', function(e){
					e.preventDefault(); // Prevent Default Submission
					updateTicket()
					.done(function(data){
						swal({
							title: 'Success',
							text: 'The record is successfully updated.',
							type: 'success'
						},function() {
							location.reload();
						});
					})
					.error(function(){
						swal('Oops', 'We could not complete the action!', 'error');
					});
					
				});

				function statusTicket(str){
					var update_status_url = '".BASE_URL."ticket/updateStatus';
					
					form_data = $('form.update-ticket-status').not('#status').serialize();
					form_data = form_data+'&status='+str;

					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: update_status_url,
						data: form_data
				    })
				}

				$('button#submit-status-ticket').bind('click', function(e){
					e.preventDefault(); // Prevent Default Submission
					var rel = $(this).attr('rel');
					if(rel=='reopen'){
						var txt;
						var r = confirm('Are you sure want to reopen this ticket?');
						if (r == true) {
							statusTicket('processing')
							.done(function(data){
								swal({
									title: 'Success',
									text: 'The ticket status is successfully reopen.',
									type: 'success'
								},function() {
									location.reload();
								});
							})
							.error(function(){
								swal('Oops', 'We could not complete the action!', 'error');
							});
						}
					}else{
						if($('#status').val()=='closed'){
							var r = confirm('Are you sure want to close this ticket?');
							if (r == true) {
								statusTicket()
								updateComplaint()
								updateTicket()
								.done(function(data){
									swal({
										title: 'Success',
										text: 'The ticket status is successfully updated.',
										type: 'success'
									},function() {
										location.reload();
									});
								})
								.error(function(){
									swal('Oops', 'We could not complete the action!', 'error');
								});
							} 
						}else{
							statusTicket('other')
							.done(function(data){
								swal({
									title: 'Success',
									text: 'The ticket status is successfully updated.',
									type: 'success'
								},function() {
									location.reload();
								});
							})
							.error(function(){
								swal('Oops', 'We could not complete the action!', 'error');
							});
						}
					}
				});

				function updateComplaint(){
					var update_complaint_url = '".BASE_URL."ticket/updateComplaint';
					form_data = $('form.update-complaint').serialize();
					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: update_complaint_url,
						data: form_data
				    })
				}

				$('button#submit-update-complaint').bind('click', function(e){
					e.preventDefault(); // Prevent Default Submission
					updateComplaint();
					updateTicket()
					.done(function(data){
						swal({
							title: 'Success',
							text: 'The complaint is successfully updated.',
							type: 'success'
						},function(){
							window.location.href = redirect_url;
						});
					})
					.error(function(){
						swal('Oops', 'We could not complete the action!', 'error');
					});
					
				});

				$('.datepicker').datepicker({
					setValue: new Date(),
					autoclose: true,
					format: 'yyyy-mm-dd',
					todayBtn: 'linked',
					clearBtn: true,
				});

				$('#state_id').select2({
					placeholder: 'Choose state',
					theme: 'bootstrap',
					allowClear: true,
					cache: true,
				    ajax: {
				        url: state_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#city_id').select2();

				$('#state_id').on('change', function(){

					var state_id = $(this).val();
					var city_url = '".BASE_URL."search.php?action=city&state_id='+state_id;

					$('#city_id').select2({
						placeholder: 'Choose nearest city',
						theme: 'bootstrap',
						allowClear: true,
						cache: true,
					    ajax: {
					        url: city_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				$('#remarks-text').hide();
				$('#res3').click(function () {
		            if ($(this).is(':checked')) {
		            	$('#res1, #res2').prop('checked', false);
		                $('#remarks-text').show();
		            } else {
		                $('#remarks-text').hide().val('');
		            }
		        });

		        $('#res1').click(function () {
		        	$('#remarks-text').hide().val('Resolved with Respondent');
		        	$('#re2, #res3').prop('checked', false);
		        });

		        $('#res2').click(function () {
		        	$('#remarks-text').hide().val('Not Resolved with Respondent');
		        	$('#res1, #res3').prop('checked', false);
		        });

				function updateRemarks(){
					var update_remarks_url = '".BASE_URL."ticket/updateRemarks';
					form_data = $('form#update-remarks').serialize();
					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: update_remarks_url,
						data: form_data
				    })
				}

				// close ticket on resolution
				$('button#submit-resolution-ticket').bind('click', function(e){
					e.preventDefault(); // Prevent Default Submission
					var r = confirm('Are you sure want to close this ticket?');
					if (r == true) {
						updateRemarks()
						.done(function(data){
							swal({
								title: 'Success',
								text: 'The ticket status is successfully updated. Please wait for the confirmation of closing from the admin.',
								type: 'success'
							},function() {
								window.location.href = redirect_url;
							});
						})
						.error(function(){
							swal('Oops', 'We could not complete the action!', 'error');
						});
					}
				});

				function submitMessage(){
					var submitMessage_url = '".BASE_URL."ticket/insertMessage';
					form_data = $('form#add-message').serialize();
					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: submitMessage_url,
						data: form_data
				    })
				}

				$('button#submit-message').bind('click', function(e){
					e.preventDefault(); // Prevent Default Submission
					submitMessage()
					.done(function(data){
						swal({
							title: 'Success',
							text: 'The message is successfully sent.',
							type: 'success'
						},function(){
							$('#add-message').find('input:text').val('');
							window.location.href = redirect_url;
						});
					})
					.error(function(){
						swal('Oops', 'We could not complete the action!', 'error');
					});
					
				});

				$('button#print-modal').on('click', function(){
	                window.print();
	            });

	            $('.delete-file').click(function(){
			    	var fileId = $(this).attr('id');
					var delete_file = '".BASE_URL."ticket/deleteFile/' + fileId;
			        swal({
			            title: 'Are you sure?',
			            text: 'You will not be able to recover this attachment!',
			            type: 'warning',
			            showCancelButton: true,
			            confirmButtonText: 'Yes, delete it!',
			            cancelButtonText: 'Cancel',
			            closeOnConfirm: false,
			            closeOnCancel: true
			        },function(){
						$.ajax({
							type: 'POST',
							url: delete_file,
							success: function(){
								
							}
						})
						.done(function() {
							swal({
								title: 'Success',
								text: 'The attachment is successfully deleted.',
								type: 'success'
							},function() {
								window.location.reload();
							});
						})
						.error(function() {
							swal('Oops', 'Error connecting to the server!', 'error');
						});
					});
			    });

			    function markAsRead(id){
					var markAsRead_url = '".BASE_URL."ticket/markAsRead/'+id;

					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: markAsRead_url
				    })
				}

				$('button.read-message').bind('click', function(e){
					e.preventDefault(); // Prevent Default Submission
					var id = $(this).attr('data-id');
					markAsRead(id)
					.done(function(data){
						swal({
							title: 'Success',
							text: 'The message is mark as read.',
							type: 'success'
						},function(){
							window.location.reload();
						});
					})
					.error(function(){
						swal('Oops', 'We could not complete the action!', 'error');
					});
					
				});

			});
        </script>";

        $header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
		$template = $this->loadView('ticket/edit');
		$helper = $this->loadHelper('date_helper');

		$complaint_model = $this->loadModel('Complaint_model');
		$user_model = $this->loadModel('User_model');
		$complaint_id = $ticket[0]['complaint_id'];
		$complaint = $complaint_model->listSingle($complaint_id);
		$messages = $ticket_model->getReply($id);
		$user = $user_model->listSingleUser($complaint[0]['user_id']);
		
		$attachment = $complaint_model->listAttachment($complaint_id);

		$header->set('css', $css);
		$header->set('custom_css', $custom_css);
		$footer->set('js', $js);
		$template->set('helper', $helper);
		$footer->set('custom_js', $custom_js);
		$template->set('ticket', $ticket);
		$template->set('complaint', $complaint);
		$template->set('attachment', $attachment);
		$template->set('messages', $messages);
		$template->set('id', $id);
		$template->set('user', $user);
		$template->set('channel', $channel_id);

		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}

	function deleteFile($fileId)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		if(isset($fileId)){
			$model = $this->loadModel('Ticket_model');
			$file = $model->listSingleAttachment($fileId);
			# delete file record
			$model->deleteFile($fileId);
			# delete actual file
			unlink(ROOT_DIR.'files/'.$file[0]['filename']);
		}else{
			die('Error: Unable to delete the record.');
		}
	}

	/**
	* Insert ticket message
	*/
	function insertMessage()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Ticket_model');

		// insert message
		$data = array(
			'user_id' => $_POST['user_id'],
			'ticket_id' => $_POST['ticket_id'],
			'content' => filter_var($_POST['message'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH)
		);

		$model->insertTicketMessage($data);

		# send email to notify admin on new email created
		global $config;

		$email = $this->loadHelper('Email_helper');
		$e_model = $this->loadModel('Mailer_model');
		$template = $e_model->listSingle(5);
		$body = $template[0]['body'];
		$subject = $template[0]['subject'];

		# get Ticket details
		$ticket = $model->listSingle($_POST['ticket_id']);
		$user = $model->getUser($ticket[0]['user_id']);
		$officer = $model->getUser($ticket[0]['assigned_to']);

		# send email to complainant
		if($_POST['referer'] == 'ticket'){

			$email_details = "Dear ".$user[0]['full_name'].", <br><br>There is new conversation on your complaint sent to NCCC. The details is as below:<br><ul><li>Ticket ID: ".$ticket[0]['tracking_no']."</li><li>Officer in-charge: ".$ticket[0]['assigned_name']."</li><li>Message: ".filter_var($_POST['message'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH)."</li></ul><p>To reply on the message, please login</p>";

			$vars = array(
				"{{EMAIL}}" => $user[0]['email'],
				"{{SUBJECT}}" => 'Complaint details at NCCC',
				"{{DETAILS}}" => $email_details,
				"{{LOGIN_LINK}}" => $config['base_url']."ticket/edit/".$_POST['ticket_id'],
			);

			$content = strtr($body, $vars);

			$email_data = array(
				'email' => $user[0]['email'], 
				'subject' => 'Complaint details at NCCC', 
				'content' => $content 
			);

			$email->send($email_data);

		} else {

			# sent email to officer
			$email_details2 = "Dear ".$ticket[0]['assigned_name'].", <br><br>There is new reply on your assigned ticket. The details is as below:<br><ul><li>Ticket ID: ".$ticket[0]['tracking_no']."</li><li>From: ".$user[0]['full_name']."</li><li>Message: ".filter_var($_POST['message'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH)."</li></ul><p>To reply on the message, please login</p>";

			$vars2 = array(
				"{{EMAIL}}" => $officer[0]['email'],
				"{{SUBJECT}}" => "New reply for ticket ".$ticket[0]['tracking_no'],
				"{{DETAILS}}" => $email_details2,
				"{{LOGIN_LINK}}" => $config['base_url']."complaint/edit/".$_POST['ticket_id'],
			);

			$content2 = strtr($body, $vars2);

			$email_data2 = array(
				'email' => $officer[0]['email'], 
				'subject' => "New reply for ticket ".$ticket[0]['tracking_no'],
				'content' => $content2
			);

			$email->send($email_data2);
		}
	}

	/**
	* Mark message as read
	*
	* @param 	int $ticket_id Ticket ID
	* @param 	int $email_id Email ID
	*/
	function markAsRead($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}
		
		$model = $this->loadModel('Ticket_model');
		
		$data = array(
			'id' => $id
		);

		$model->markAsRead($data);
	}

	/**
	* Assign email to ticket
	*
	* @param 	int $ticket_id Ticket ID
	* @param 	int $email_id Email ID
	*/
	function assign_email()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}
		
		$model = $this->loadModel('Ticket_model');
		
		$data = array(
			'ticket_id' => $_POST['ticket_id'],
			'email_id' => $_POST['email_id']
		);

		$model->assignEmail($data);
	}

	/**
	* Update ticket
	*
	* @param 	int $id Ticket ID
	*/
	function update()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$assigned_date = $_POST['assigned_date'] ?: date('Y-m-d H:i:s');
		
		$model = $this->loadModel('Ticket_model');
		
		$data = array(
			'id' => $_POST['id'],
			'sector_id' => $_POST['sector_id'],
			'category_id' => $_POST['category_id'],
			'subcategory_id' => $_POST['subcategory_id'],
			'type_id' => $_POST['type_id'],
			'priority' => $_POST['priority'],
			'assigned_to' => $_POST['assigned_to'],
			'assigned_date' => $assigned_date,
			'assigned_by' => $_POST['assigned_by']
		);

		$model->updateTicket($data);

		// update ticket status
		$c_model = $this->loadModel('Complaint_model');
		if($session->get('permission')=='admin'){
			$c_data = array(
				'status' => 'open'
			);
		}
		if(in_array($session->get('permission'), array('officer', 'trainee'), true)){
			$c_data = array(
				'status' => 'processing'
			);
		}

		$c_model->editRecord($c_data, $_POST['complaint_id']);
	}

	function updateComplaint()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Complaint_model');
		
		if(isset($_POST)){
			
			$data = array(
				'user_id' => $_POST['user_id'],
				'details' => $_POST['details'],
				'incident_date' => $_POST['incident_date'],
				'incident_time' => $_POST['incident_time'],
				'estimated_loss' => $_POST['estimated_loss'],
				'company_name' => $_POST['company_name'],
				'respondent_name' => $_POST['respondent_name'],
				'respondent_phone_no' => $_POST['respondent_phone_no'],
				'company_email' => $_POST['company_email'],
				'branch' => $_POST['branch'],
				'address1' => $_POST['address1'],
				'address2' => $_POST['address2'],
				'postcode' => $_POST['postcode'],
				'city_id' => $_POST['city_id'],
				'state_id' => $_POST['state_id']
			);

			$model->editRecord($data,$_POST['id']);

			if(isset($_FILES['attachment'])){

				//upload attachment if exist
				if($_FILES['attachment']['size'] > 0){

					require(APP_DIR .'helpers/upload_helper.php');

					// insert attachment
					$uploader = Upload::factory('files');
					$uploader->file($_FILES['attachment']);
					$results = $uploader->upload();

					$model->insertAttachment($results, $_POST['id']);
				}
			}
			
		}else{
			die('Error: Unable to update the complaint.');
		}
	}

	function updateRemarks()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Complaint_model');
		
		if(isset($_POST)){
			
			$data = array(
				'status' => 'closed',
				'remarks' => $_POST['remarks']
			);

			$model->editRecord($data,$_POST['complaint_id']);

			# send email to notify admin on pending closing ticket
			global $config;

			$email = $this->loadHelper('Email_helper');
			$e_model = $this->loadModel('Mailer_model');
			$template = $e_model->listSingle(5);
			$body = $template[0]['body'];
			$subject = $template[0]['subject'];

			# get Ticket details
			$ticket_model = $this->loadModel('Ticket_model');
			$ticket = $ticket_model->listSingle($_POST['ticket_id']);

			# get user who assigned the ticket
			$user_model = $this->loadModel('User_model');
			$user = $user_model->listSingleUser($ticket[0]['assigned_by']);

			$email_details = "Dear ".$user[0]['full_name'].", <br><br>There is new ticket closing confirmation required. The details is as below:<br><ul><li>Ticket ID: ".$ticket[0]['tracking_no']."</li><li>Officer in-charge: ".$ticket[0]['assigned_name']."</li><li>Ticket Resolution: ".filter_var($_POST['remarks'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH)."</li></ul><p>To approve this ticket closing, please login.</p>";

			$vars = array(
				"{{EMAIL}}" => $user[0]['email'],
				"{{SUBJECT}}" => 'Ticket closing confirmation at NCCC',
				"{{DETAILS}}" => $email_details,
				"{{LOGIN_LINK}}" => $config['base_url']."ticket/edit/".$_POST['ticket_id'],
			);

			$content = strtr($body, $vars);

			$email_data = array(
				'email' => $user[0]['email'], 
				'subject' => 'Ticket closing confirmation at NCCC', 
				'content' => $content 
			);

			$check_send = $email->send($email_data);

			if($check_send == true){
				echo "Email sent";
			}else{
				die('Failed to send email to Admin.');
			}
			
		}else{
			die('Error: Unable to update the complaint.');
		}
	}

	function updateStatus()
	{
		//var_dump($_POST);die;
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		// update ticket
		$model = $this->loadModel('Ticket_model');

		// update complaint
		$c_model = $this->loadModel('Complaint_model');
		$complaint_data = array(
			'status' => $_POST['status']
		);

		$c_model->editRecord($complaint_data, $_POST['complaint_id']);

		if($_POST['status'] == 'closed'){

			function pluralize( $count, $text ) 
            {
                return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
            }

            function interval($datetime1, $datetime2)
            {
                $interval = $datetime1->diff($datetime2);
                if ( $v = $interval->y >= 1 ) return pluralize( $interval->y, 'year' );
                if ( $v = $interval->m >= 1 ) return pluralize( $interval->m, 'month' );
                if ( $v = $interval->d >= 1 ) return pluralize( $interval->d, 'day' );
                if ( $v = $interval->h >= 1 ) return pluralize( $interval->h, 'hour' );
                if ( $v = $interval->i >= 1 ) return pluralize( $interval->i, 'minute' );
                return pluralize( $interval->s, 'second' );
            }

			$start = new DateTime($_POST['open_date']);
            $end = new DateTime();
            $time_worked = interval($start, $end);

			$ticket_data = array(
				'id' => $_POST['id'],
				'close_date' => date('Y-m-d H:i:s'),
				'close_by' => $_POST['user_id'],
				'time_worked' => $time_worked
			);

			$model->updateClose($ticket_data);
		}

		# send notification to user on ticket update
		global $config;

		$email = $this->loadHelper('Email_helper');
		$e_model = $this->loadModel('Mailer_model');
		$template = $e_model->listSingle(6);
		$body = $template[0]['body'];
		$subject = $template[0]['subject'];

		$user = $model->getUser($_POST['user_id']);

		$email_details = "Dear ".$user[0]['full_name'].", <br><br>There is new status update on your complaint sent to NCCC. The details is as below:<br><ul><li>Ticket ID: ".$_POST['tracking_no']."</li><li>Status: ".strtoupper($_POST['status'])."</li></ul><p>To check this ticket, please click the button below:</p>";

		$vars = array(
			"{{EMAIL}}" => $user[0]['email'],
			"{{DETAILS}}" => $email_details,
			"{{LOGIN_LINK}}" => $config['base_url']."ticket/edit/".$_POST['id'],
		);

		$content = strtr($body, $vars);

		$email_data = array(
			'email' => $user[0]['email'], 
			'subject' => $subject, 
			'content' => $content 
		);

		$check_send = $email->send($email_data);

		if($check_send == true){
			echo('Status ticket E-mail sent!');
		}else{
			die('Failed to send status e-mail ticket.');
		}
	}

	/**
	* Add attachment
	*/
	function attachment()
	{
		$session = $this->loadHelper('session_helper');
		require(APP_DIR .'helpers/upload_helper.php');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		if(isset($_POST)){
			$id = $_POST['complaint_id'];

			if ($_FILES['attachment']['size'] > 0) {

				$uploader = Upload::factory('files');
				$uploader->file($_FILES['attachment']);
				$results = $uploader->upload();

				$model = $this->loadModel('Ticket_model');
				$model->insertAttachment($results, $id);

				$this->redirect('ticket/edit/'.$_POST['ticket_id']);

			}else{
				die('No file uploaded!');
			}
		}else{
			die('Error updating ticket incident.');
		}
	}

	/**
	* Process active ticket
	*/
	function process()
	{
		global $config;

		$session = $this->loadHelper('session_helper');
		$datatable = $this->loadHelper('datatable_helper');
		$date = $this->loadHelper('date_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		// DB table to use
		$table = 'view_tickets';
		
		// Table's primary key
		$primaryKey = 'id';

		$where = array("user_id='".$session->get('user_id')."'");

		$columns = array(
			array( 'db' => 'user_id', 'dt' => 'user_id' ),
			array( 'db' => 'tracking_no', 'dt' => 'tracking_no' ),
			array( 'db' => 'company_name', 'dt' => 'company_name' ),
			array( 'db' => 'incident_date', 'dt' => 'incident_date' ),
			array( 'db' => 'incident_time', 'dt' => 'incident_time' ),
			array( 'db' => 'complaint_date', 'dt' => 'complaint_date' ),
			array(
				'db' => 'estimated_loss',
				'dt' => 'estimated_loss',
				'formatter' => function( $d, $row ) {
					return number_format($d, 2);
				}
			),
			array(
				'db' => 'status',
				'dt' => 'status',
				'formatter' => function( $d, $row ) {
					switch($d){
						case 'draft': return "<span class=\"label label-default\">Draft</span>"; break;
						case 'submitted': return "<span class=\"label label-info\">Submitted</span>"; break;
						case 'open': return "<span class=\"label label-success label-block\">Open</span>"; break;
						case 'processing': return "<span class=\"label label-primary\">Processing</span>"; break;
						case 'closed': return "<span class=\"label label-danger\">Closed</span>"; break;
					}
				}
			),
			array(
				'db' => 'complaint_id',
				'dt' => 'view',
				'formatter' => function( $d, $row ) {
					return "<a href=\"".BASE_URL."complaint/edit/".$d."\" class=\"btn btn-default btn-xs\">View</a>";
				}
			),
			array(
				'db' => 'unread',
				'dt' => 'unread',
				'formatter' => function( $d, $row ) {
					return "<span class=\"badge\">".$d."</span>";
				}
			)
			
		);
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config['db_username'],
		    'pass' => $config['db_password'],
		    'db'   => $config['db_name'],
		    'host' => $config['db_host']
		);
		
		$data = json_encode(
		    $datatable::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where )
		);
		print_r($data);
	}

	function process_admin()
	{
		global $config;

		$session = $this->loadHelper('session_helper');
		$datatable = $this->loadHelper('datatable_helper');
		$date = $this->loadHelper('date_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		// DB table to use
		$table = 'view_ticket_admin';
		
		// Table's primary key
		$primaryKey = 'id';

		if(in_array($session->get('permission'), array('officer', 'trainee'), true)){
			$where = array("assigned_to='".$session->get('user_id')."' AND status!='draft' AND status!='submitted'");
		}else{
			$where = "status!='draft' AND status!='closed'";
		}

		$columns = array(
			array( 'db' => 'id', 'dt' => 'id' ),
			array( 'db' => 'tracking_no', 'dt' => 'tracking_no' ),
			array( 'db' => 'complaint_date', 'dt' => 'complaint_date' ),
			array( 'db' => 'company_name', 'dt' => 'company_name' ),
			array( 'db' => 'full_name', 'dt' => 'full_name' ),
			array( 'db' => 'phone_no', 'dt' => 'phone_no' ),
			array( 'db' => 'ic_passport', 'dt' => 'ic_passport' ),
			array( 'db' => 'email', 'dt' => 'email' ),
			array(
				'db' => 'status',
				'dt' => 'status',
				'formatter' => function( $d, $row ) {
					switch($d){
						case 'draft': return "<span class=\"label label-default\">Draft</span>"; break;
						case 'submitted': return "<span class=\"label label-info\">Submitted</span>"; break;
						case 'open': return "<span class=\"label label-success\">Open</span>"; break;
						case 'processing': return "<span class=\"label label-primary\">Processing</span>"; break;
						case 'closed': return "<span class=\"label label-danger\">Closed</span>"; break;
					}
				}
			),
			array(
				'db' => 'id',
				'dt' => 'view',
				'formatter' => function( $d, $row ) {
					return "<a href=\"".BASE_URL."ticket/edit/".$d."\" class=\"btn btn-default btn-xs\">View</a>";
				}
			),
			array(
				'db' => 'unread',
				'dt' => 'unread',
				'formatter' => function( $d, $row ) {
					return "<span class=\"badge\">".$d."</span>";
				}
			)
		);

		// SQL server connection information
		$sql_details = array(
		    'user' => $config['db_username'],
		    'pass' => $config['db_password'],
		    'db'   => $config['db_name'],
		    'host' => $config['db_host']
		);
		
		$data = json_encode(
		    $datatable::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where )
		);
		print_r($data);
	}

	// Download
	function downloadFile($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Ticket_model');
		$file = $model->listSingleAttachment($id);

		$realFile = ROOT_DIR.'1files/'.$file[0]['filename'];
		
		$fileDownload = "".$file[0]['original_filename']."";

        // Output headers.
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Type: ".$file[0]['mime']);
        header("Content-Disposition: attachment; filename=".$fileDownload);
        header("Content-Transfer-Encoding: binary");

        // Output file.
        readfile($realFile);
	}
	
}