<?php

class Audit extends Controller {
	
	function index()
	{
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}
		$model = $this->loadModel('Audit_model');
		$track = $model->listTrack();
		$template = $this->loadView('audit_index');
		$template->set('track', $track);
		$template->render();
	}
	
	function create($user_id, $action)
	{
		$model = $this->loadModel('Audit_model');
		$data = array(
			'user_id' => $user_id,
			'action' => $action
		);
		$model->add($data);
	}
	
}