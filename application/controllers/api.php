<?php
/**
* API Controller
*
* API controller for ticket system
*
* @package    	NCCC
* @subpackage 	Ticket
* @author 		Fadli Saad (https://fadli.my)
*/
class Api extends Controller {
	
	public function email()
	{
		$session = $this->loadHelper('session_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
			exit;
		}

		if($session->get('permission') == 'user'){
			$this->redirect('site/page/no-email-account');
			exit;
		}

		// Enable RainLoop Api and include index file 
		$_ENV['RAINLOOP_INCLUDE_AS_API'] = true;
		include '/home/myaduancom/public_html/email/index.php';

		$pattern = "/^[a-z0-9._%+-]+@[a-z0-9.-]*(" . implode('|', array('myaduan.com.my')) . ")$/i";
        if (preg_match($pattern, $session->get('email'))) {

			$ssoHash = \RainLoop\Api::GetUserSsoHash($session->get('email'), "*.RsuD(TX=%{");

			// redirect to webmail sso url
			header('Location: '.BASE_URL.'email/?sso&hash='.$ssoHash);

		}else{

			$this->redirect('site/page/no-access');
			exit;
		}
	}
}