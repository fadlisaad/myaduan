<?php
/**
* Error Controller
*
* Error controller for ticket system.
*
* @package    	Kabaz
* @subpackage 	Error
* @author 		Fadli Saad (https://fadli.my)
*/
class Oops extends Controller {
	
	function index()
	{
		$template = $this->loadView('error');
		$template->render();
	}

	function fileTooBig()
	{
		$template = $this->loadView('error-file');
		$template->render();
	}
}