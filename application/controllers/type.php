<?php

class Type extends Controller {

	function index()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/datatables/jquery.dataTables.min.css',
			'assets/plugins/datatables/buttons.bootstrap.min.css'
		);

		$js = array(
			'assets/plugins/datatables/media/js/jquery.dataTables.min.js',
			'assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
			'assets/pages/datatables.init.js'
		);

		$custom_js = "<script type=\"text/javascript\">
			var base_url = '".BASE_URL."type/process';
			
			$(document).ready(function() {
    			$('#datatable').dataTable({
    				serverSide : true,
    				processing : true,
    				ajax : {
    					url : base_url,
    					type : 'POST'
    				},
    				deferRender : true,
    				error : true,
    				columns: [
			            { data: 'sector' },
			            { data: 'title' },
			            { data: 'last_update' },
			            { data: 'action' }
			        ],
			        columnDefs: [
					    { width: '12%', 'targets': 2 },
					    { width: '5%', 'targets': 3 }
					]
    			});
    		});
		</script>";
		
		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('type/index');

		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}
	
	function add()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
		);

		$js = array(
			'assets/plugins/select2/select2.min.js'
		);

		$custom_js = "<script type=\"text/javascript\">
			var sector_url = '".BASE_URL."search.php?action=sector';
			$('#sector_id').select2({
				placeholder: 'Choose sector',
				theme: 'bootstrap',
				delay: 250,
			    ajax: {
			        url: sector_url,
			        dataType: 'json',
			        processResults: function (data) {
			            return {
			            	results: data
			            };
			        },
			    }
			});
		</script>";

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('type/insert');

        $header->set('css', $css);
		$footer->set('js', $js);
        $footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}
	
	function create()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('type_model');
		if(isset($_POST)){
			
			$data = $_POST;
			$model->addRecord($data);
			$this->redirect('type/index');
			
		}else{
			die('Error: Unable to add the record.');
		}
	}
	
	function edit($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
		);

		$js = array(
			'assets/plugins/select2/select2.min.js'
		);

		$custom_js = "<script type='text/javascript'>
			//Parameter
			var delete_url = '".BASE_URL."type/delete/".$id."';
			var main_url = '".BASE_URL."type/index/';
			var sector_url = '".BASE_URL."search.php?action=sector';

			$('#sector_id').select2({
				placeholder: 'Choose sector',
				theme: 'bootstrap',
				delay: 250,
			    ajax: {
			        url: sector_url,
			        dataType: 'json',
			        processResults: function (data) {
			            return {
			            	results: data
			            };
			        },
			    }
			});

		    $('#delete').click(function(){
		        swal({
		            title: 'Are you sure?',
		            text: 'You will not be able to recover this record!',
		            type: 'warning',
		            showCancelButton: true,
		            confirmButtonText: 'Yes, delete it!',
		            cancelButtonText: 'Cancel',
		            closeOnConfirm: false,
		            closeOnCancel: true
		        },function(){
					$.ajax({
						type: 'POST',
						url: delete_url,
						success: function(){
							
						}
					})
					.done(function() {
						swal({
							title: 'Success',
							text: 'The record is successfully deleted.',
							type: 'success'
						},function() {
							window.location.href = main_url;
						});
					})
					.error(function() {
						swal('Oops', 'Error connecting to the server!', 'error');
					});
				});
		    });
		</script>";

		$model = $this->loadModel('type_model');
		$data = $model->listSingle($id);

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('type/edit');

		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		$template->set('data', $data);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}
	
	function update()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('type_model');
		if(isset($_POST)){
			$id = $_POST['id'];
			$data = $_POST;
			$model->editRecord($data,$id);
			$this->redirect('type/index');
			
		}else{
			die('Error: Unable to update the record.');
		}
	}
	
	function delete($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		if(isset($id)){
			$model = $this->loadModel('type_model');
			$model->deleteRecord($id);
			$this->redirect('type/index');
		}else{
			die('Error: Unable to delete the record.');
		}
	}

	// process datatable
	function process()
	{
		global $config;

		$session = $this->loadHelper('session_helper');
		$datatable = $this->loadHelper('datatable_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		// DB table to use
		$table = 'view_types';
		 
		// Table's primary key
		$primaryKey = 'id';
		 
		// Array of database columns which should be read and sent back to DataTables. The `db` parameter represents the column name in the database, while the `dt` parameter represents the DataTables column identifier. In this case object parameter names

		$columns = array(
		    array( 'db' => 'sector', 'dt' => 'sector' ),
		    array( 'db' => 'title', 'dt' => 'title' ),
		    array( 'db' => 'last_update', 'dt' => 'last_update' ),
        	array(
		    	'db' => 'id',
		    	'dt' => 'action',
		    	'formatter' => function( $d, $row ) {
            		return "<a href=\"".BASE_URL."type/edit/".$d."\" class=\"btn btn-info btn-xs\">Edit</a>";
        		}
        	)
		);
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config['db_username'],
		    'pass' => $config['db_password'],
		    'db'   => $config['db_name'],
		    'host' => $config['db_host']
		);
		 
		$data = json_encode(
		    $datatable::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
		);
		print_r($data);
	}
}