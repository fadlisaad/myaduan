<?php

class Site extends Controller {

	function index()
	{
		if(isset($_SESSION['loggedin'])){
			$this->redirect('dashboard');
		}
		$header = $this->loadView('header');
		$navigation = $this->loadView('public-nav');
		$footer = $this->loadView('footer');
        $template = $this->loadView('site/index');

		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}
	
	function page($slug)
	{
		$model = $this->loadModel('Site_model');
		$data = $model->listSingle($slug);

		$header = $this->loadView('header');
		$navigation = $this->loadView('public-nav');
		$footer = $this->loadView('footer');
        $template = $this->loadView('site/page');

		$header->render();
		$navigation->render();
		$template->set('data', $data);
		$template->render();
		$footer->render();
	}
	
	function add()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Category_model');
		$template = $this->loadView('category_insert');
		$template->render();
	}
	
	function create()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Category_model');
		if(isset($_POST)){
			
			$data = $_POST;
			$model->addRecord($data);
			$this->redirect('category/index');
			
		}else{
			die('Error: Unable to add the record.');
		}
	}
	
	function edit($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$custom_js = "<script type='text/javascript'>
			//Parameter
			var delete_url = '".BASE_URL."category/delete/".$id."';
			var main_url = '".BASE_URL."category/index/';

			$('input, select, textarea').attr('readonly', true);
			$('#lock').hide();

			// Readonly toggle
			$('#unlock').click(function() {
				$('input, select, textarea').attr('readonly', false);
				$(this).hide();
				$('#lock').show();
			});

			// Readonly toggle
			$('#lock').click(function() {
				$('input, select, textarea').attr('readonly', true);
				$(this).hide();
				$('#unlock').show();
			});

		    $('#delete').click(function(){
		        swal({
		            title: 'Are you sure?',
		            text: 'You will not be able to recover this record!',
		            type: 'warning',
		            showCancelButton: true,
		            confirmButtonText: 'Yes, delete it!',
		            cancelButtonText: 'Cancel',
		            closeOnConfirm: false,
		            closeOnCancel: true
		        },function(){
					$.ajax({
						type: 'POST',
						url: delete_url,
						success: function(){
							
						}
					})
					.done(function() {
						swal({
							title: 'Success',
							text: 'The record is successfully deleted.',
							type: 'success'
						},function() {
							window.location.href = main_url;
						});
					})
					.error(function() {
						swal('Oops', 'Error connecting to the server!', 'error');
					});
				});
		    });
		</script>";

		$model = $this->loadModel('Category_model');
		$template = $this->loadView('category_edit');
		$data = $model->listSingle($id);
		$template->set('custom_js', $custom_js);
		$template->set('data', $data);
		$template->render();
	}
	
	function update()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Category_model');
		if(isset($_POST)){
			$id = $_POST['id'];
			$data = $_POST;
			$model->editRecord($data,$id);
			$this->redirect('category/index');
			
		}else{
			die('Error: Unable to update the record.');
		}
	}
	
	function delete($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		if(isset($id)){
			$model = $this->loadModel('Category_model');
			$model->deleteRecord($id);
			$this->redirect('category/index');
		}else{
			die('Error: Unable to delete the record.');
		}
	}

	// process datatable
	function process()
	{
		global $config;

		$session = $this->loadHelper('session_helper');
		$datatable = $this->loadHelper('datatable_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		// DB table to use
		$table = 'category';
		 
		// Table's primary key
		$primaryKey = 'id';
		 
		// Array of database columns which should be read and sent back to DataTables. The `db` parameter represents the column name in the database, while the `dt` parameter represents the DataTables column identifier. In this case object parameter names

		$columns = array(
		    array( 'db' => 'id', 'dt' => 'ID' ),
		    array( 'db' => 'title', 'dt' => 'title' ),
		    array( 'db' => 'last_update', 'dt' => 'last_update' ),
        	array(
		    	'db' => 'id',
		    	'dt' => 'action',
		    	'formatter' => function( $d, $row ) {
            		return "<a href=\"".BASE_URL."category/edit/".$d."\" class=\"btn btn-info btn-xs\">Edit</a>";
        		}
        	)
		);
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config['db_username'],
		    'pass' => $config['db_password'],
		    'db'   => $config['db_name'],
		    'host' => $config['db_host']
		);
		 
		$data = json_encode(
		    $datatable::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
		);
		print_r($data);
	}
}