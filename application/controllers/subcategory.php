<?php

class SubCategory extends Controller {

	function index()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/datatables/jquery.dataTables.min.css',
			'assets/plugins/datatables/buttons.bootstrap.min.css'
		);

		$js = array(
			'assets/plugins/datatables/media/js/jquery.dataTables.min.js',
			'assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
			'assets/pages/datatables.init.js'
		);

		$custom_js = "<script type=\"text/javascript\">
			var base_url = '".BASE_URL."subcategory/process';
			
			$(document).ready(function() {
    			$('#datatable').dataTable({
    				serverSide : true,
    				processing : true,
    				ajax : {
    					url : base_url,
    					type : 'POST'
    				},
    				deferRender : true,
    				error : true,
    				columns: [
			            { data: 'sector' },
			            { data: 'category' },
			            { data: 'title' },
			            { data: 'last_update' },
			            { data: 'action' }
			        ],
			        columnDefs: [
					    { width: '20%', 'targets': 0 },
					    { width: '20%', 'targets': 1 },
					    { width: '12%', 'targets': 3 },
					    { width: '5%', 'targets': 4 }
					]
    			});
    		});
		</script>";
		
		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('subcategory/index');

		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}
	
	function add()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
		);

		$js = array(
			'assets/plugins/select2/select2.min.js'
		);

		$custom_js = "<script type=\"text/javascript\">
			var sector_url = '".BASE_URL."search.php?action=sector';

			$('#sector_id').select2({
				placeholder: 'Choose sector',
				theme: 'bootstrap',
				delay: 250,
			    ajax: {
			        url: sector_url,
			        dataType: 'json',
			        processResults: function (data) {
			            return {
			            	results: data
			            };
			        },
			    }
			});

			$('.select2').select2({
				theme: 'bootstrap'
			});

			$('#sector_id').on('change', function () {
				sector_id = $(this).val();
				console.log(sector_id);
				var category_url = '".BASE_URL."search.php?action=category&sector_id='+sector_id;

				$('#category_id').select2({
					placeholder: 'Chooose category',
					theme: 'bootstrap',
				    ajax: {
				        url: category_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});
			});
		</script>";

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('subcategory/insert');

        $header->set('css', $css);
		$footer->set('js', $js);
        $footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}
	
	function create()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Subcategory_model');
		if(isset($_POST)){
			
			$data = $_POST;
			$model->addRecord($data);
			$this->redirect('subcategory/index');
			
		}else{
			die('Error: Unable to add the record.');
		}
	}
	
	function edit($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
		);

		$js = array(
			'assets/plugins/select2/select2.min.js'
		);

		$custom_js = "<script type='text/javascript'>
			//Parameter
			var delete_url = '".BASE_URL."subcategory/delete/".$id."';
			var main_url = '".BASE_URL."subcategory/index/';
			var sector_url = '".BASE_URL."search.php?action=sector';

			$('#sector_id').select2({
				placeholder: 'Choose sector',
				theme: 'bootstrap',
			    ajax: {
			        url: sector_url,
			        dataType: 'json',
			        processResults: function (data) {
			            return {
			            	results: data
			            };
			        },
			    }
			});

			$('#category_id').select2({
				theme: 'bootstrap',
				placeholder: 'Chooose category',
			});

			$('#sector_id').on('select2:select', function () {
				sector_id = $(this).val();
				console.log(sector_id);
				var category_url = '".BASE_URL."search.php?action=category&sector_id='+sector_id;

				$('#category_id').select2({
					placeholder: 'Chooose category',
					theme: 'bootstrap',
				    ajax: {
				        url: category_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});
			});

		    $('#delete').click(function(){
		        swal({
		            title: 'Are you sure?',
		            text: 'You will not be able to recover this record!',
		            type: 'warning',
		            showCancelButton: true,
		            confirmButtonText: 'Yes, delete it!',
		            cancelButtonText: 'Cancel',
		            closeOnConfirm: false,
		            closeOnCancel: true
		        },function(){
					$.ajax({
						type: 'POST',
						url: delete_url,
						success: function(){
							
						}
					})
					.done(function() {
						swal({
							title: 'Success',
							text: 'The record is successfully deleted.',
							type: 'success'
						},function() {
							window.location.href = main_url;
						});
					})
					.error(function() {
						swal('Oops', 'Error connecting to the server!', 'error');
					});
				});
		    });
		</script>";

		$model = $this->loadModel('Subcategory_model');
		$data = $model->listSingle($id);

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('subcategory/edit');

		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		$template->set('data', $data);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}
	
	function update()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Subcategory_model');
		if(isset($_POST)){
			$id = $_POST['id'];
			$data = $_POST;
			$model->editRecord($data,$id);
			$this->redirect('subcategory/index');
			
		}else{
			die('Error: Unable to update the record.');
		}
	}
	
	function delete($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		if(isset($id)){
			$model = $this->loadModel('Subcategory_model');
			$model->deleteRecord($id);
			$this->redirect('subcategory/index');
		}else{
			die('Error: Unable to delete the record.');
		}
	}

	// process datatable
	function process()
	{
		global $config;

		$session = $this->loadHelper('session_helper');
		$datatable = $this->loadHelper('datatable_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		// DB table to use
		$table = 'view_subcategories';
		 
		// Table's primary key
		$primaryKey = 'id';
		 
		// Array of database columns which should be read and sent back to DataTables. The `db` parameter represents the column name in the database, while the `dt` parameter represents the DataTables column identifier. In this case object parameter names

		$columns = array(
		    array( 'db' => 'sector', 'dt' => 'sector' ),
		    array( 'db' => 'category', 'dt' => 'category' ),
		    array( 'db' => 'title', 'dt' => 'title' ),
		    array( 'db' => 'last_update', 'dt' => 'last_update' ),
        	array(
		    	'db' => 'id',
		    	'dt' => 'action',
		    	'formatter' => function( $d, $row ) {
            		return "<a href=\"".BASE_URL."subcategory/edit/".$d."\" class=\"btn btn-info btn-xs\">Edit</a>";
        		}
        	)
		);
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config['db_username'],
		    'pass' => $config['db_password'],
		    'db'   => $config['db_name'],
		    'host' => $config['db_host']
		);
		 
		$data = json_encode(
		    $datatable::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
		);
		print_r($data);
	}
}