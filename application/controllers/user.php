<?php
/**
* 
* @package    	NCCC
* @subpackage 	User
* @author 		Fadli Saad (https://fadli.my)
*/

class User extends Controller {

	function index()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/datatables/jquery.dataTables.min.css',
			'assets/plugins/datatables/buttons.bootstrap.min.css'
		);

		$js = array(
			'assets/plugins/datatables/media/js/jquery.dataTables.min.js',
			'assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
			'assets/pages/datatables.init.js'
		);

		$custom_js = "<script type='text/javascript'>
			var base_url = '".BASE_URL."user/process';
			
			$(document).ready(function() {
    			$('#datatable').dataTable({
    				serverSide : true,
    				processing : true,
    				ajax : {
    					url : base_url,
    					type : 'POST'
    				},
    				error : true,
    				columns: [
			            { data: 'full_name' },
			            { data: 'email' },
			            { data: 'permission' },
			            { data: 'action' }
			        ]
    			});
    		});
		</script>";

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('user/index');

		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}

	function roles()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/datatables/jquery.dataTables.min.css',
			'assets/plugins/datatables/buttons.bootstrap.min.css',
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
		);

		$js = array(
			'assets/plugins/datatables/media/js/jquery.dataTables.min.js',
			'assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
			'assets/pages/datatables.init.js',
			'assets/plugins/select2/select2.min.js'
		);

		$custom_js = "<script type='text/javascript'>

			var base_url = '".BASE_URL."user/process_sector';
			var sector_url = '".BASE_URL."search.php?action=sector';
			var search_text = '';
			
			$(document).ready(function() {

				$('#sector').select2({
					dropdownParent: $('#con-close-modal'),
					placeholder: 'Choose sector',
					theme: 'bootstrap',
				    ajax: {
				        url: sector_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

    			var table = $('#datatable').dataTable({
    				serverSide : true,
    				processing : true,
    				ajax : {
    					url : base_url,
    					type : 'POST'
    				},
    				error : true,
    				columns: [
			            { data: 'sector' },
			            { data: 'full_name' },
			            { data: 'permission' },
			            { data: 'action' }
			        ]
    			});

    			$('#sector').change(function(){
    				search_text = $('#sector :selected').text();
					table.fnFilter(search_text, 0);
				});
    		});
		</script>";

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('user/roles');

		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}
	
	function profile()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
		);

		$js = array(
			'assets/plugins/select2/select2.min.js',
			'assets/plugins/maskedinput/jquery.maskedinput.min.js',
		);

		$model = $this->loadModel('User_model');
		$data = $model->listSingleUser($session->get('user_id'));

		$custom_js = "<script type='text/javascript'>

		$(document).ready(function() {
			
			//Parameter
			var update_url = '".BASE_URL."user/self_update/';
			var update_avatar_url = '".BASE_URL."user/avatar_update/';
			var user_id = $('#user_id').val();

		    $('button#self_update').click(function(){
		    	var user_password = $('#user_pass').val();
		    	if (user_password == '') return false;

		        swal({
		            title: 'Are you sure?',
		            text: 'You password will be updated to<br><strong> ' + user_password + '</strong>',
		            type: 'info',
		            html: true,
		            showCancelButton: true,
		            confirmButtonText: 'Yes, change it!',
		            cancelButtonText: 'Cancel',
		            closeOnConfirm: false,
		            closeOnCancel: true
		        },function(){
					$.ajax({
						type: 'POST',
						url: update_url,
						data: 'id=' + user_id + '&password=' + user_password,
						success: function(){
							
						}
					})
					.done(function() {
						swal({
							title: 'Success',
							text: 'The password is successfully changed.',
							type: 'success'
						},function() {
							self.location.href;
						});
					})
					.error(function() {
						swal('Oops', 'Error connecting to the server!', 'error');
					});
				});
		    });

		    $('.avatar').bind('click', function(){
		    	var user_avatar = $(this).val();
		    	console.log(user_avatar);
		    	if (user_avatar == '') return false;

		        swal({
		            title: 'Are you sure?',
		            text: 'You avatar will be updated',
		            type: 'info',
		            html: true,
		            showCancelButton: true,
		            confirmButtonText: 'Yes, change it!',
		            cancelButtonText: 'Cancel',
		            closeOnConfirm: false,
		            closeOnCancel: true
		        },function(){
					$.ajax({
						type: 'POST',
						url: update_avatar_url,
						data: 'user_id=' + user_id + '&avatar=' + user_avatar,
						success: function(){
							
						}
					})
					.done(function() {
						swal({
							title: 'Success',
							text: 'The avatar is successfully changed.',
							type: 'success'
						},function() {
							self.location.reload();
						});
					})
					.error(function() {
						swal('Oops', 'Error connecting to the server!', 'error');
					});
				});
		    });

		    var country_url = '".BASE_URL."search.php?action=country';
			var state_url = '".BASE_URL."search.php?action=state';

			var country_id = '".$data[0]['country_id']."';
			var country_name = '".$data[0]['country_name']."';

		    $('#country_id').select2({
				placeholder: 'Choose country',
				theme: 'bootstrap',
				cache: true,
				delay: 250,
			    ajax: {
			        url: country_url,
			        dataType: 'json',
			        processResults: function (data) {
			            return {
			            	results: data
			            };
			        },
			    }
			});

			$('#country_id').select2('data', {id: 135, a_key: 'Malaysia'});

			$('#state').select2({
				placeholder: 'Choose state',
				theme: 'bootstrap',
				cache: true,
			    ajax: {
			        url: state_url,
			        dataType: 'json',
			        processResults: function (data) {
			            return {
			            	results: data
			            };
			        },
			    }
			});

			$('#state').change(function(){

				var state_id = $(this).val();
				var city_url = '".BASE_URL."search.php?action=city&state_id='+state_id;

				$('#city').select2({
					placeholder: 'Choose nearest city',
					theme: 'bootstrap',
					cache: true,
					delay: 250,
				    ajax: {
				        url: city_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});
			});

			// hide passport by default
			$('#passport').hide();

			// ic masking
			$('#ic_passport').mask('999999-99-9999',{
				placeholder: 'XXXXXX-XX-XXXX'
			});

			$('#state-other').hide();
			$('#city-other').hide();

			// on change nationality, toggle required field
			$('#country_id').change(function(){
				var country =  $('#country_id').val();
				if(country != '135'){

					$('#passport').show();
					$('#ic').hide();
					$('#phone-no').mask('+999999?99999');
					$('#state-my').hide();
					$('#city-my').hide();
					$('#ethnicity-my').hide();
					$('#state-other').show();
					$('#city-other').show();
					$('#income-my').hide();

				}else{

					$('#passport').hide();
					$('#ic').show();
					$('#state-my').show();
					$('#city-my').show();
					$('#income-my').show();
					$('#state-other').hide();
					$('#city-other').hide();
					$('#ethnicity-my').show();
				}
			});

			var ethnicity = '".$data[0]['ethnicity']."';

			$('.ethnicity').each(function(){
				if($(this).val() == ethnicity){
					$(this).prop(\"checked\", \"checked\");
					console.log('ethnicity matched');
				}
			});

			var income = '".$data[0]['income']."';

			$('.income').each(function(){
				if($(this).val() == income){
					$(this).prop(\"checked\", \"checked\");
					console.log('income matched');
				}
			});

			var age = '".$data[0]['age']."';

			$('.age').each(function(){
				if($(this).val() == age){
					$(this).prop(\"selected\", \"selected\");
					console.log('age matched');
				}
			});

		});
		</script>";

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
		$template = $this->loadView('user/view');
		
		$template->set('data', $data);
		$footer->set('js', $js);
		$header->set('css', $css);
		$footer->set('custom_js', $custom_js);

		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}
	
	function add()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css'
		);

		$js = array(
			'assets/plugins/select2/select2.min.js'
		);

		$custom_js = "<script type='text/javascript'>
			$(document).ready(function() {

				$('.select2').select2({
					theme: 'bootstrap'
				});

				// confirmation
				$('#submit').click(function(){
			        swal({
			            title: 'Record saved',
			            text: 'Your record has been saved.',
			            type: 'success'
			        })
			    });
			});
		</script>";

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
		$template = $this->loadView('user/insert');

		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}
	
	function create()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}else{
			$model = $this->loadModel('User_model');

			if(isset($_POST)){

				// create login
				$loginData = array(
					'email' => $_POST['email'],
					'password' => $_POST['password'],
					'permission' => $_POST['permission']
				);
				$model->addUser($loginData);
				
				// add profile
				$lastId = $model->lastInsertedID();
				$profileData = array(
					'user_id' => $lastId[0]['id'],
					'full_name' => $_POST['full_name']
				);
				$model->addUserProfile($profileData);

				$this->redirect('user/index');
				
			}else{
				die('Error');
			}
		}
	}
	
	function edit($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}else{

			$model = $this->loadModel('User_model');
			$template = $this->loadView('user/edit');
			$data = $model->listSingleUser($id);

			$js = array(
				'assets/plugins/select2/select2.min.js'
				);

			$css = array(
				'assets/plugins/select2/select2.min.css',
				'assets/plugins/select2/select2-bootstrap.css'
				);

			$custom_js = "<script type='text/javascript'>
				//Parameter
				var delete_url = '".BASE_URL."user/delete/';
				var main_url = '".BASE_URL."user/index/';
				var user_id = '".$id."'

			    $('#delete').click(function(){
			        swal({
			            title: 'Are you sure?',
			            text: 'You will not be able to recover this record!',
			            type: 'warning',
			            showCancelButton: true,
			            confirmButtonText: 'Yes, delete it!',
			            cancelButtonText: 'Cancel',
			            closeOnConfirm: false,
			            closeOnCancel: true
			        },function(){
						$.ajax({
							type: 'POST',
							url: delete_url,
							data: 'id=' + user_id,
							success: function(){
								
							}
						})
						.done(function() {
							swal({
								title: 'Success',
								text: 'The record is successfully deleted.',
								type: 'success'
							},function() {
								window.location.href = main_url;
							});
						})
						.error(function() {
							swal('Oops', 'Error connecting to the server!', 'error');
						});
					});
			    });
			</script>";
			
	        $header = $this->loadView('header');
			$navigation = $this->loadView('navigation');
			$footer = $this->loadView('footer');

			$template->set('data', $data);
			$header->set('css', $css);
			$footer->set('js', $js);
			$footer->set('custom_js', $custom_js);

			$header->render();
			$navigation->render();
			$template->render();
			$footer->render();
		}
	}
	
	function update($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}else{
			$model = $this->loadModel('User_model');
			if(isset($id)){
				// update login
				$loginData = array(
					'password' => $_POST['password'],
					'permission' => $_POST['permission']
				);
				$model->editUser($loginData, $id);
				
				// update profile
				$profileData = array(
					'full_name' => $_POST['full_name']
				);
				$query = $model->selectSingleById('user_profile','user_id',$id);
				$model->editUserProfile($profileData, $query[0]['id']);
				$this->redirect('user/index');
				
			}else{
				die('Error');
			}
		}
	}

	// Ajax call to self update password
	function self_update()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}else{
			$model = $this->loadModel('User_model');
			if(isset($_POST)){
				$data = $_POST;
				$model->selfEditUser($data);
				return true;			
			}else{
				return false;
			}
		}
	}

	// Ajax call to self update avatar
	function avatar_update()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}else{
			$model = $this->loadModel('User_model');
			$session->set('avatar', $_POST['avatar']);
			if(isset($_POST)){
				$data = $_POST;
				$model->editAvatar($data);
				return true;			
			}else{
				return false;
			}
		}
	}

	# add roles
	function add_role()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css'
		);

		$js = array(
			'assets/plugins/select2/select2.min.js'
		);

		$custom_js = "<script type='text/javascript'>
			$(document).ready(function() {

				var user_url = '".BASE_URL."search.php?action=user';

				$('#user_id').select2({
					placeholder: 'Choose user',
					theme: 'bootstrap',
					delay: 250,
				    ajax: {
				        url: user_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				var sector_url = '".BASE_URL."search.php?action=sector';
				
				$('#sector_id').select2({
					placeholder: 'Choose sector',
					theme: 'bootstrap',
					delay: 250,
				    ajax: {
				        url: sector_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				// confirmation
				$('#submit').click(function(){
			        swal({
			            title: 'Record saved',
			            text: 'Your record has been saved.',
			            type: 'success'
			        })
			    });
			});
		</script>";

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
		$template = $this->loadView('user/insert_role');

		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}

	# create role
	function create_role()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}else{
			$model = $this->loadModel('User_model');

			if(isset($_POST)){

				if($_POST['sector_id'] == 0){

					# loop each sector and add one by one
					$sector = $model->listSector();
					foreach ($sector as $row) {
						$data = array(
							'user_id' => $_POST['user_id'],
							'sector_id' => $row->id
						);

						# check if the user already assigned the role
						$check_role = $model->checkRole($data);
						if($check_role[0]['id'] == '0'){
							$model->addRole($data);
						}
					}

				}else{

					$data = array(
						'user_id' => $_POST['user_id'],
						'sector_id' => $_POST['sector_id']
					);
					# check if the user already assigned the role
					$check_role = $model->checkRole($data);
					if($check_role[0]['id'] == '0'){
						$model->addRole($data);
					}
				}

				$this->redirect('user/roles');
				
			}else{
				die('Error');
			}
		}
	}
	
	function delete()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}
		
		$id = $_POST['id'];
		$model = $this->loadModel('User_model');
		$model->deleteUser($id);
	}

	function process()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}
		
		global $config;

		$datatable = $this->loadHelper('datatable_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		// DB table to use
		$table = 'view_users';
		 
		// Table's primary key
		$primaryKey = 'id';
		 
		if($session->get('permission') == 'admin'){
			$where = "permission = 'trainee'";
		}else{
			$where = "";
		}

		$columns = array(
		    array( 'db' => 'full_name', 'dt' => 'full_name' ),
		    array( 'db' => 'permission', 'dt' => 'permission' ),
		    array( 'db' => 'email', 'dt' => 'email' ),
        	array(
		    	'db' => 'id',
		    	'dt' => 'action',
		    	'formatter' => function( $d, $row ) {
            		return "<a href=\"".BASE_URL."user/edit/".$d."\" class=\"btn btn-info btn-xs\">Edit</a>";
        		}
        	)
		);
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config['db_username'],
		    'pass' => $config['db_password'],
		    'db'   => $config['db_name'],
		    'host' => $config['db_host']
		);
		 
		$data = json_encode(
		    $datatable::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where )
		);
		print_r($data);
	}

	function process_sector()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}
		
		global $config;

		$datatable = $this->loadHelper('datatable_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		// DB table to use
		$table = 'view_roles';
		 
		// Table's primary key
		$primaryKey = 'id';
		 
		// Array of database columns which should be read and sent back to DataTables. The `db` parameter represents the column name in the database, while the `dt` parameter represents the DataTables column identifier. In this case object parameter names

		$columns = array(
		    array( 'db' => 'sector', 'dt' => 'sector' ),
		    array( 'db' => 'full_name', 'dt' => 'full_name' ),
		    array( 'db' => 'permission', 'dt' => 'permission' ),
        	array(
		    	'db' => 'id',
		    	'dt' => 'action',
		    	'formatter' => function( $d, $row ) {
            		return "<a href=\"".BASE_URL."user/edit_role/".$d."\" class=\"btn btn-info btn-xs\">Edit</a>";
        		}
        	)
		);
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config['db_username'],
		    'pass' => $config['db_password'],
		    'db'   => $config['db_name'],
		    'host' => $config['db_host']
		);
		 
		$data = json_encode(
		    $datatable::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
		);
		print_r($data);
	}

}