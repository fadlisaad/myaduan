<?php
/**
* E-mail Controller
*
* @package    	NCCC
* @subpackage 	E-mail
* @author 		Fadli Saad (https://fadli.my)
*/

use SSilence\ImapClient\ImapClientException;
use SSilence\ImapClient\ImapConnect;
use SSilence\ImapClient\ImapClient as Imap;

class Mailer extends Controller {

	/**
	* List all mail template
	*
	* @return 	Return list of e-mail template or null if not found
	*/

	function index()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/datatables/jquery.dataTables.min.css',
			'assets/plugins/datatables/buttons.bootstrap.min.css'
		);

		$js = array(
			'assets/plugins/datatables/media/js/jquery.dataTables.min.js',
			'assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
			'assets/pages/datatables.init.js'
		);

		/**
		* Custom script
		*/
		$custom_js = "<script type=\"text/javascript\">
			var base_url = '".BASE_URL."mailer/process';
			
			$(document).ready(function() {
    			$('#datatable').dataTable({
    				serverSide : true,
    				processing : true,
    				ajax : {
    					url : base_url,
    					type : 'POST'
    				},
    				deferRender : true,
    				error : true,
    				columns: [
			            { data: 'recepient' },
			            { data: 'subject' },
			            { data: 'body' },
			            { data: 'last_update' },
			            { data: 'action' }
			        ]
    			});
    		});
		</script>";

		/**
		* Load Ticket Model
		*/
		$model = $this->loadModel('Mailer_model');
		$email = $model->listAll();

		/**
		* Load and render template
		*/
		$template = $this->loadView('mailer_index');
		$helper = $this->loadHelper('date_helper');
		$template->set('email', $email);
		$template->set('css', $css);
		$template->set('js', $js);
		$template->set('custom_js', $custom_js);
		$template->set('helper', $helper);
		$template->render();
	}

	/**
	* Process datatable
	*/
	function process()
	{
		global $config;

		$session = $this->loadHelper('session_helper');
		$datatable = $this->loadHelper('datatable_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		// DB table to use
		$table = 'email_template';
		 
		// Table's primary key
		$primaryKey = 'id';
		 
		// Array of database columns which should be read and sent back to DataTables. The `db` parameter represents the column name in the database, while the `dt` parameter represents the DataTables column identifier. In this case object parameter names

		$columns = array(
		    array( 'db' => 'recepient', 'dt' => 'recepient' ),
		    array( 'db' => 'subject', 'dt' => 'subject' ),
		    array( 'db' => 'body', 'dt' => 'body' ),
		    array(
		    	'db' => 'last_update',
		    	'dt' => 'last_update',
		    	'formatter' => function( $d, $row ) {
            		return date( 'jS M Y', strtotime($d));
        		}
        	),
        	array(
		    	'db' => 'id',
		    	'dt' => 'action',
		    	'formatter' => function( $d, $row ) {
            		return "<a href=\"".BASE_URL."mailer/create/".$d."\" class=\"btn btn-info btn-xs\">Edit</a>";
        		}
        	)
		);
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config['db_username'],
		    'pass' => $config['db_password'],
		    'db'   => $config['db_name'],
		    'host' => $config['db_host']
		);
		 
		$data = json_encode(
		    $datatable::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
		);
		print_r($data);
	}

	function getTemplate($id)
	{
		$model = $this->loadModel('Mailer_model');
		$email_template = $model->listSingle($id);
		return $email_template;
	}
}