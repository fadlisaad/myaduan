<?php
/**
* Complaint Controller
*
* Base controller for ticket system
*
* @package    	NCCC
* @subpackage 	Ticket
* @author 		Fadli Saad (https://fadli.my)
*/

class Complaint extends Controller {

	/**
	* List all complaint
	*
	* @return 	Return list of complaint
	*/

	function index()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/datatables/jquery.dataTables.min.css',
			'assets/plugins/datatables/buttons.bootstrap.min.css'
		);

		$js = array(
			'assets/plugins/datatables/media/js/jquery.dataTables.min.js',
			'assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
			'assets/pages/datatables.init.js'
		);

		$custom_js = "<script type=\"text/javascript\">
			var active_url = '".BASE_URL."complaint/process';
			var user_id = '".$session->get('user_id')."';
			var permission = '".$session->get('permission')."';
			
			$(document).ready(function() {

    			var table = $('#datatable').dataTable({
    				serverSide : true,
    				processing : true,
    				ajax : {
    					url : active_url,
    					type : 'POST'
    				},
    				deferRender : true,
    				error : true,
					bInfo : false,
    				columns: [
			            { data: 'user_id' },
			            { data: 'company_name' },
			            { data: 'incident_date' },
			            { data: 'estimated_loss' },
			            { data: 'full_name' },
			            { data: 'status' },
			            { data: 'action' }
			        ],
			        columnDefs: [
			            {
			                targets: [ 0 ],
			                visible: false,
			                searchable: true
			            }
			        ]
    			});

    			if(permission == 'user'){
    				table.fnFilter(user_id, 0);
    			}
    		});
		</script>";

		/**
		* Load and render template
		*/
		$helper = $this->loadHelper('date_helper');

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$footer = $this->loadView('footer');
        $template = $this->loadView('complaint/index');

		$template->set('helper', $helper);
		$header->set('css', $css);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		
		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}

	function add()
	{
		$session = $this->loadHelper('session_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
			'assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css',
			'assets/plugins/timepicker/bootstrap-timepicker.min.css',
			'assets/css/jquery.loading.min.css'
		);

		$js = array(
			'assets/plugins/select2/select2.min.js',
			'assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js',
			'assets/plugins/maskedinput/jquery.maskedinput.min.js',
			'assets/plugins/timepicker/bootstrap-timepicker.min.js',
			'assets/plugins/maskedmoney/jquery.maskMoney.min.js',
			'assets/js/validator.min.js',
			'assets/js/jquery.loading.min.js'
		);

		$custom_js = "<script type=\"text/javascript\">
			var state_url = '".BASE_URL."search.php?action=state';
			
			$(document).ready(function(){

				var sector_url = '".BASE_URL."search.php?action=sector';

				$('#sector_id').select2({
					placeholder: 'Choose sector',
					minimumResultsForSearch: -1,
					theme: 'bootstrap',
					delay: 250,
				    ajax: {
				        url: sector_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#sector_id').on('change', function () {

					sector_id = $(this).val();
					console.log('sector_id:'+sector_id);

					var category_url = '".BASE_URL."search.php?action=category&sector_id='+sector_id;

					$('#category_id').select2({
						placeholder: 'Chooose category',
						minimumResultsForSearch: -1,
						theme: 'bootstrap',
					    ajax: {
					        url: category_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});

					var staff_url = '".BASE_URL."search.php?action=staff&sector_id='+sector_id;
					$('#assigned_to').select2({
						placeholder: 'Choose officer',
						minimumResultsForSearch: -1,
						theme: 'bootstrap',
					    ajax: {
					        url: staff_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});

				});

				$('#category_id').on('change', function () {
					sector_id = $('#sector_id').val();
					category_id = $(this).val();
					console.log('category_id:'+category_id);
					var subcategory_url = '".BASE_URL."search.php?action=subcategory&sector_id='+sector_id+'&category_id='+category_id;

					$('#subcategory_id').select2({
						placeholder: 'Chooose sub category',
						minimumResultsForSearch: -1,
						theme: 'bootstrap',
					    ajax: {
					        url: subcategory_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				$('#subcategory_id').on('change', function () {
					sector_id = $('#sector_id').val();
					console.log('sector_id:'+sector_id);

					var type_url = '".BASE_URL."search.php?action=type&sector_id='+sector_id;

					$('#type_id').select2({
						placeholder: 'Choose complaint type',
						minimumResultsForSearch: -1,
						theme: 'bootstrap',
						delay: 250,
					    ajax: {
					        url: type_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				$('.datepicker').datepicker({
					setValue: new Date(),
					autoclose: true,
					format: 'yyyy-mm-dd',
					todayBtn: 'linked',
					clearBtn: true,
				});

				$('#state_id').select2({
					placeholder: 'Choose state',
					minimumResultsForSearch: -1,
					theme: 'bootstrap',
				    ajax: {
				        url: state_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#state_id').change(function(){

					var state_id = $(this).val();
					var city_url = '".BASE_URL."search.php?action=city&state_id='+state_id;

					$('#city_id').select2({
						placeholder: 'Choose nearest city',
						minimumResultsForSearch: -1,
						theme: 'bootstrap',
					    ajax: {
					        url: city_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				$('#postcode').mask('99999');

				$('#respondent_phone_no').mask('+6099999?9999');

				$('.timepicker').timepicker({
					showMeridian: true,
					timeFormat: 'HH:mm:ss'
				});

				$('#estimated_loss').maskMoney({
					thousands:'',
					decimal:'.',
					allowZero: true
				});

				function submitComplaint(status){
					$('body').loading('start');
					var submitComplaint_url = '".BASE_URL."complaint/create';
					form_data = $('form#add-complaint').serialize();
					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: submitComplaint_url,
						data: form_data + '&status='+status
				    })
				}

				$('#add-complaint').validator().on('submit', function (e) {
					if (e.isDefaultPrevented()) {

						// handle the invalid form...

						swal('Oops', 'We could not complete the action! Some field are required field. Please fill them accordingly.', 'error');

					} else {

						e.preventDefault();

						// everything looks good!

						if($('input[name=\"submit\"]').length > 0) {

							submitComplaint('submitted')
							.done(function(data){
								$('body').loading('stop');
								swal({
									title: 'Success',
									text: 'The complaint is successfully submitted.',
									type: 'success'
								},function(){
									window.location.href = '".BASE_URL."dashboard';
								});
							})
							.error(function(){
								swal('Oops', 'We could not complete the action!', 'error');
							});

						}else{

							submitComplaint('draft')
							.done(function(data){
								$('body').loading('stop');
								swal({
									title: 'Success',
									text: 'The complaint is successfully save as draft.',
									type: 'success'
								},function(){
									window.location.href = '".BASE_URL."dashboard';
								});
							})
							.error(function(){
								swal('Oops', 'We could not complete the action!', 'error');
							});
						}
					}
				});
			});
        </script>";

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$template = $this->loadView('complaint/insert');
		$footer = $this->loadView('footer');

		$header->set('css', $css);
		$template->set('session', $session);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);

		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}

	function create()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		# load PHP filter helper
		$filter = $this->loadHelper('filter_helper');

		$model = $this->loadModel('Complaint_model');
		if(isset($_POST)){

			$date = DateTime::createFromFormat('H:i A', $_POST['incident_time']);
			$formatted_time = $date->format('H:i:s');
			$tracking_no = uniqid();
			
			$data = array(
				'user_id' => $filter->isInt($_POST['user_id']),
				'details' => $filter->sanitize($_POST['details']),
				'incident_date' => $filter->sanitize($_POST['incident_date']),
				'complaint_date' => date('Y-m-d'),
				'incident_time' => $formatted_time,
				'estimated_loss' => $filter->sanitize($_POST['estimated_loss']),
				'company_name' => $filter->sanitize($_POST['company_name']),
				'branch' => $filter->sanitize($_POST['branch']),
				'respondent_name' => $filter->sanitize($_POST['respondent_name']),
				'respondent_phone_no' => $filter->sanitize($_POST['respondent_phone_no']),
				'company_email' => $filter->isEmail($_POST['company_email']),
				'address1' => $filter->sanitize($_POST['address1']),
				'address2' => $filter->sanitize($_POST['address2']),
				'postcode' => $filter->sanitize($_POST['postcode']),
				'city_id' => $filter->isInt($_POST['city_id']),
				'state_id' => $filter->isInt($_POST['state_id']),
				'status' => $filter->isInt($_POST['status'])
			);

			$model->addRecord($data);

			// add ticket
			$complaint_id = $model->lastInsertedID();
			
			$t_model = $this->loadModel('Ticket_model');
			$t_data = array(
				'tracking_no' => $tracking_no,
				'complaint_id' => $complaint_id[0]['id'],
				'open_date' => date('Y-m-d H:i:s'),
				'sector_id' => $_POST['sector_id'],
				'category_id' => $_POST['category_id'],
				'subcategory_id' => $_POST['subcategory_id'],
				'type_id' => $_POST['type_id']
			);

			$t_model->insertTicket($t_data);

			global $config;

			$email = $this->loadHelper('Email_helper');
			$e_model = $this->loadModel('Mailer_model');
			
			if($_POST['status'] == 'submitted'){

				// send email to admin start
				$admin_template = $e_model->listSingle(4);
				$admin_body = $admin_template[0]['body'];
				$admin_subject = $admin_template[0]['subject'];

				$admin = $model->listAdmin();

				$admin_vars = array(
					"{{EMAIL}}" => $admin[0]['email'],
					"{{FULLNAME}}" => 'Admin',
					"{{LOGIN_LINK}}" => $config['base_url'].'auth/login',
					"{{DETAILS}}" => "Ticket tracking #: ".$tracking_no
				);

				$admin_email_content = strtr($admin_body, $admin_vars);

				$admin_email_data = array(
					'email' => $admin[0]['email'], 
					'subject' => $admin_subject, 
					'content' => $admin_email_content 
				);

				$email->send($admin_email_data);

				// send acknowledgment to user
				$user_template = $e_model->listSingle(6);
				$user_body = $user_template[0]['body'];
				$user_subject = $user_template[0]['subject'];

				$u_model = $this->loadModel('User_model');
				$user = $u_model->listSingleUser($_POST['user_id']);

				$user_vars = array(
					"{{EMAIL}}" => $user[0]['email'],
					"{{FULLNAME}}" => $user[0]['full_name'],
					"{{DETAILS}}" => "Ticket tracking #: ".$tracking_no
				);

				$user_email_content = strtr($user_body, $user_vars);

				$user_email_data = array(
					'email' => $user[0]['email'], 
					'subject' => $user_subject, 
					'content' => $user_email_content 
				);

				$email->send($user_email_data);

			}
		}
	}

	function edit($id)
	{
		$session = $this->loadHelper('session_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Complaint_model');
		$ticket_model = $this->loadModel('Ticket_model');
		$data = $model->listSingle($id);
		$category = $ticket_model->listSingleByComplaintId($id);
		$messages = $ticket_model->getReply($data[1]['id']);

		$channel_id = $session->get('user_id');

		$css = array(
			'assets/plugins/select2/select2.min.css',
			'assets/plugins/select2/select2-bootstrap.css',
			'assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css'
		);

		$js = array(
			'assets/plugins/select2/select2.min.js',
			'assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js'
		);

		$redirect_url = BASE_URL."complaint/edit/".$id;

		$custom_js = "<script type=\"text/javascript\">

			var state_url = '".BASE_URL."search.php?action=state';
			var redirect_url = '".$redirect_url."';
			
			$(document).ready(function() {

				var sector_url = '".BASE_URL."search.php?action=sector';

				$('#sector_id').select2({
					placeholder: 'Choose sector',
					theme: 'bootstrap',
					delay: 250,
				    ajax: {
				        url: sector_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#sector_id').on('change', function () {

					sector_id = $(this).val();
					console.log('sector_id:'+sector_id);

					var category_url = '".BASE_URL."search.php?action=category&sector_id='+sector_id;

					$('#category_id').select2({
						placeholder: 'Chooose category',
						theme: 'bootstrap',
					    ajax: {
					        url: category_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});

					var staff_url = '".BASE_URL."search.php?action=staff&sector_id='+sector_id;
					$('#assigned_to').select2({
						placeholder: 'Choose officer',
						theme: 'bootstrap',
					    ajax: {
					        url: staff_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});

				});

				$('#category_id').on('change', function () {
					sector_id = $('#sector_id').val();
					category_id = $(this).val();
					console.log('category_id:'+category_id);
					var subcategory_url = '".BASE_URL."search.php?action=subcategory&sector_id='+sector_id+'&category_id='+category_id;

					$('#subcategory_id').select2({
						placeholder: 'Chooose sub category',
						theme: 'bootstrap',
					    ajax: {
					        url: subcategory_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				$('#subcategory_id').on('change', function () {
					sector_id = $('#sector_id').val();
					console.log('sector_id:'+sector_id);

					var type_url = '".BASE_URL."search.php?action=type&sector_id='+sector_id;

					$('#type_id').select2({
						placeholder: 'Choose complaint type',
						theme: 'bootstrap',
						delay: 250,
					    ajax: {
					        url: type_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				$('.datepicker').datepicker({
					autoclose: true,
					format: 'yyyy-mm-dd',
					todayBtn: 'linked',
					clearBtn: true
				});

				$('#state_id').select2({
					placeholder: 'Choose state',
					theme: 'bootstrap',
				    ajax: {
				        url: state_url,
				        dataType: 'json',
				        processResults: function (data) {
				            return {
				            	results: data
				            };
				        },
				    }
				});

				$('#state_id').change(function(){

					var state_id = $(this).val();
					var city_url = '".BASE_URL."search.php?action=city&state_id='+state_id;

					$('#city_id').select2({
						placeholder: 'Choose nearest city',
						theme: 'bootstrap',
					    ajax: {
					        url: city_url,
					        dataType: 'json',
					        processResults: function (data) {
					            return {
					            	results: data
					            };
					        },
					    }
					});
				});

				function submitMessage(){
					var submitMessage_url = '".BASE_URL."ticket/insertMessage';
					form_data = $('form#add-message').serialize();
					return $.ajax({
						type: 'POST',
						dataType: 'html',
						url: submitMessage_url,
						data: form_data
				    })
				}

				$('button#submit-message').bind('click', function(e){
					e.preventDefault(); // Prevent Default Submission
					submitMessage()
					.done(function(data){
						swal({
							title: 'Success',
							text: 'The message is successfully sent.',
							type: 'success'
						},function(){
							$('#add-message').find('input:text').val('');
							window.location.href = redirect_url;
						});
					})
					.error(function(){
						swal('Oops', 'We could not complete the action!', 'error');
					});
					
				});

				$('.delete-file').click(function(){
			    	var fileId = $(this).attr('id');
					var delete_file = '".BASE_URL."ticket/deleteFile/' + fileId;
			        swal({
			            title: 'Are you sure?',
			            text: 'You will not be able to recover this attachment!',
			            type: 'warning',
			            showCancelButton: true,
			            confirmButtonText: 'Yes, delete it!',
			            cancelButtonText: 'Cancel',
			            closeOnConfirm: false,
			            closeOnCancel: true
			        },function(){
						$.ajax({
							type: 'POST',
							url: delete_file,
							success: function(){
								
							}
						})
						.done(function() {
							swal({
								title: 'Success',
								text: 'The attachment is successfully deleted.',
								type: 'success'
							},function() {
								window.location.reload();
							});
						})
						.error(function() {
							swal('Oops', 'Error connecting to the server!', 'error');
						});
					});
			    });

			});
        </script>";

		$header = $this->loadView('header');
		$navigation = $this->loadView('navigation');
		$template = $this->loadView('complaint/edit');
		$footer = $this->loadView('footer');
		$helper = $this->loadHelper('date_helper');
		
		$attachment = $model->listAttachment($id);

		$header->set('css', $css);
		$template->set('session', $session);
		$template->set('data', $data);
		$template->set('category', $category);
		$template->set('attachment', $attachment);
		$template->set('messages', $messages);
		$template->set('helper', $helper);
		$footer->set('js', $js);
		$footer->set('custom_js', $custom_js);
		$template->set('id', $id);

		$header->render();
		$navigation->render();
		$template->render();
		$footer->render();
	}

	function update()
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		# load PHP filter helper
		$filter = $this->loadHelper('filter_helper');

		$model = $this->loadModel('Complaint_model');
		
		if(isset($_POST)){

			if(isset($_POST['submitted'])){
				$_POST['status'] = 'submitted';
			}elseif (isset($_POST['processing'])) {
				$_POST['status'] = 'processing';
			}else{
				$_POST['status'] = 'draft';
			}
			
			$data = array(
				'user_id' => $filter->isInt($_POST['user_id']),
				'details' => $filter->sanitize($_POST['details']),
				'incident_date' => $filter->sanitize($_POST['incident_date']),
				'incident_time' => $filter->sanitize($_POST['incident_time']),
				'branch' => $filter->sanitize($_POST['branch']),
				'respondent_name' => $filter->sanitize($_POST['respondent_name']),
				'respondent_phone_no' => $filter->sanitize($_POST['respondent_phone_no']),
				'company_email' => $filter->isEmail($_POST['company_email']),
				'estimated_loss' => $filter->sanitize($_POST['estimated_loss']),
				'company_name' => $filter->sanitize($_POST['company_name']),
				'address1' => $filter->sanitize($_POST['address1']),
				'address2' => $filter->sanitize($_POST['address2']),
				'postcode' => $filter->sanitize($_POST['postcode']),
				'city_id' => $filter->isInt($_POST['city_id']),
				'state_id' => $filter->isInt($_POST['state_id']),
				'status' => $_POST['status']
			);
			
			$model->editRecord($data, $filter->isInt($_POST['id']));

			//upload attachment
			if($_FILES['attachment']['size'] > 0){

				require(APP_DIR .'helpers/upload_helper.php');

				// insert attachment
				$uploader = Upload::factory('files');
				$uploader->file($_FILES['attachment']);
				$results = $uploader->upload();
				$model->insertAttachment($results, $filter->isInt($_POST['id']));
			}

			if($_POST['status'] == 'submitted'){

				global $config;

				$email = $this->loadHelper('Email_helper');
				$e_model = $this->loadModel('Mailer_model');

				// send email to admin
				$template = $e_model->listSingle(4);
				$body = $template[0]['body'];
				$subject = $template[0]['subject'];

				$admin = $model->listAdmin();

				$vars = array(
					"{{EMAIL}}" => $admin[0]['email'],
					"{{FULLNAME}}" => 'Admin',
					"{{LOGIN_LINK}}" => $config['base_url'].'auth/login',
				);

				$content = strtr($body, $vars);

				$email_data = array(
					'email' => $admin[0]['email'], 
					'subject' => $subject, 
					'content' => $content 
				);

				$email->send($email_data);

				// send acknowledgment to user
				$user_template = $e_model->listSingle(6);
				$user_body = $user_template[0]['body'];
				$user_subject = $user_template[0]['subject'];

				$u_model = $this->loadModel('User_model');
				$user = $u_model->listSingleUser($_POST['user_id']);

				$user_vars = array(
					"{{EMAIL}}" => $user[0]['email'],
					"{{FULLNAME}}" => $user[0]['full_name'],
					"{{DETAILS}}" => "Ticket tracking #: ".$_POST['tracking_no']
				);

				$user_email_content = strtr($user_body, $user_vars);

				$user_email_data = array(
					'email' => $user[0]['email'], 
					'subject' => $user_subject, 
					'content' => $user_email_content 
				);

				$email->send($user_email_data);
			}
			
			$this->redirect('ticket/index');
			
		}else{
			die('Error: Unable to update the complaint.');
		}
	}

	/**
	* Add attachment
	*/
	function attachment()
	{
		$session = $this->loadHelper('session_helper');
		require(APP_DIR .'helpers/upload_helper.php');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		if(isset($_POST)){
			$id = $_POST['file_id'];
			$type = $_POST['file_type'];

			if (!empty($_FILES['attachment'])) {

				$uploader = Upload::factory('files');
				$uploader->file($_FILES['attachment']);
				$results = $uploader->upload();

				$model = $this->loadModel('Complaint_model');
				$model->insertAttachment($results, $id, $type);
			}
		}else{
			die('Error updating ticket incident.');
		}
	}

	/**
	* Process all complaint
	*/
	function process()
	{

		global $config;

		$session = $this->loadHelper('session_helper');
		$datatable = $this->loadHelper('datatable_helper');

		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		// DB table to use
		$table = 'view_complaints';
		
		// Table's primary key
		$primaryKey = 'id';
		
		// Array of database columns which should be read and sent back to DataTables. The `db` parameter represents the column name in the database, while the `dt` parameter represents the DataTables column identifier. In this case object parameter names

		$columns = array(
		    array( 'db' => 'user_id', 'dt' => 'user_id' ),
		    array( 'db' => 'company_name', 'dt' => 'company_name' ),
		    array( 'db' => 'incident_date', 'dt' => 'incident_date' ),
		    array( 'db' => 'estimated_loss', 'dt' => 'estimated_loss' ),
		    array( 'db' => 'full_name', 'dt' => 'full_name' ),
		    array(
		    	'db' => 'status',
		    	'dt' => 'status',
		    	'formatter' => function( $d, $row ) {
		    		switch($d){
		    			case 'draft': return "<span class=\"label label-default\">Draft</span>"; break;
		    			case 'submitted': return "<span class=\"label label-info\">Submitted</span>"; break;
		    			case 'open': return "<span class=\"label label-success\">Open</span>"; break;
		    			case 'processing': return "<span class=\"label label-primary\">Processing</span>"; break;
		    			case 'closed': return "<span class=\"label label-danger\">Closed</span>"; break;
		    		}
		    	}
		    ),
        	array(
		    	'db' => 'id',
		    	'dt' => 'action',
		    	'formatter' => function( $d, $row ) {
        			switch ($_SESSION['permission']) {
        				case 'user':
        					return "<a href=\"".BASE_URL."complaint/edit/".$d."\" class=\"btn btn-info btn-xs\">View</a>";
        					break;
        				case 'officer':
        					return "<a href=\"".BASE_URL."ticket/edit/".$d."\" class=\"btn btn-info btn-xs\">View</a>";
        					break;
        				case 'manager':
        					return "<a href=\"".BASE_URL."ticket/edit/".$d."\" class=\"btn btn-info btn-xs\">View</a> <a href=\"".BASE_URL."ticket/assign/".$d."\" class=\"btn btn-primary btn-xs\">Assign</a>";
        					break;
        				case 'admin':
        					return "<a href=\"".BASE_URL."complaint/edit/".$d."\" class=\"btn btn-info btn-xs\">View</a> <a href=\"".BASE_URL."ticket/edit/".$d."\" class=\"btn btn-primary btn-xs\">View</a>";
        					break;
        				case 'super':
        					return "<a href=\"".BASE_URL."complaint/edit/".$d."\" class=\"btn btn-success btn-xs\">Complaint</a> <a href=\"".BASE_URL."ticket/edit/".$d."\" class=\"btn btn-info btn-xs\">Ticket</a> <a href=\"".BASE_URL."ticket/assign/".$d."\" class=\"btn btn-primary btn-xs\">Assign</a>";
        					break;
        			}
        		}
        	)
		);
		 
		// SQL server connection information
		$sql_details = array(
		    'user' => $config['db_username'],
		    'pass' => $config['db_password'],
		    'db'   => $config['db_name'],
		    'host' => $config['db_host']
		);
		
		$data = json_encode(
		    $datatable::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
		);
		print_r($data);
	}

	// Download
	function downloadFile($id)
	{
		$session = $this->loadHelper('session_helper');
		if(empty($session->get('loggedin'))){
			$this->redirect('auth/login');
		}

		$model = $this->loadModel('Complaint_model');
		$file = $model->listSingleAttachment($id);

		$realFile = ROOT_DIR.'files/'.$file[0]['filename'];
		
		$fileDownload = "".$file[0]['original_filename']."";

        // Output headers.
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Type: ".$file[0]['mime']);
        header("Content-Disposition: attachment; filename=".$fileDownload);
        header("Content-Transfer-Encoding: binary");

        // Output file.
        readfile($realFile);
	}
	
}