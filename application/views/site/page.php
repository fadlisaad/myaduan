        <div class="wrapper">
            <div class="container">
            	<!-- Start Header -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title"><?php echo $data[0]['title'] ?></h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php echo $data[0]['content'] ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row-->