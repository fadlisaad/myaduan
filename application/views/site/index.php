        <div class="wrapper">
            <div class="container">

                <!-- Start Header -->
                <!-- Start Widget -->
                <div class="row">
                    <div class="col-lg-4">
                    	<div class="panel panel-primary">
                    		<div class="panel-heading">
                    			<h3 class="panel-title">Login</h3>
                            </div>
                            <div class="panel-body">
                            	<form class="form-horizontal m-t-20" action="<?php echo BASE_URL ?>auth/process_login" method="post">
				                    <div class="form-group">
				                        <div class="col-xs-12">
				                            <input class="form-control input-lg" type="text" name="email" required="" placeholder="E-mail">
				                        </div>
				                    </div>

				                    <div class="form-group">
				                        <div class="col-xs-12">
				                            <input class="form-control input-lg" id="password" type="password" name="password" required="" placeholder="Password">
				                        </div>
				                    </div>

				                    <div class="form-group">
				                        <div class="col-xs-12">
				                            <div class="checkbox checkbox-primary">
				                                <input id="checkbox-signup" type="checkbox">
				                                <label for="checkbox-signup">Remember me</label>
				                            </div>
				                            
				                        </div>
				                    </div>
				                    
				                    <div class="form-group text-center m-t-40">
				                        <div class="col-xs-12">
				                            <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" type="submit" name="submit">Log In</button>
				                        </div>
				                    </div>
				                </form>

			                    <!-- <div class="form-group m-t-30">
			                        <div class="col-sm-12 text-center">
			                            <a href="<?php echo BASE_URL ?>auth/reset_password"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a> or <a href="<?php echo BASE_URL ?>auth/register"> Register as a new user</a>
			                        </div>
			                    </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <img src="<?php echo BASE_URL ?>assets/images/logo.svg" class="img-resposive pull-right">
                                <p>You should state your complaint briefly and clearly by providing all the relevant information.</p>
                                <p>You should try to resolve the problem by talking directly with the trader before lodging a complaint. Many complaints can be settled this way. We recommend that you should do this first if you have not done so. </p>
                                <p>Your complaint must be supported with relevant information depending on cases, e.g. invoices, bills, any form of conversation, receipts, vehicle registration number, account number, repair or service documents and etc. </p>
                                <p>We endeavour to resolve consumers’ complaints, but do not guarantee that every complaint can be resolved. There is also no time-frame for resolving complaints as their nature and complexity varies. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End row-->