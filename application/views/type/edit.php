
        <div class="wrapper">
            <div class="container">
            	<!-- Start Header -->

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-color panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Edit Type Details</h3>
                            </div>
                            <div class="panel-body">
                            	<!-- Content start -->
                            	<form method="post" role="form" action="<?php echo BASE_URL ?>type/update" novalidate="novalidate" id="edit-sector">
                                    <div class="form-group">
                                        <label for="sector">Sector</label>
                                        <select id="sector_id" name="sector_id" class="form-control">
                                            <option value="<?php echo $data[0]['sector_id'] ?>"><?php echo $data[0]['sector'] ?></option>
                                        </select>
                                    </div>
    								<div class="form-group">
    									<label for="title">Title</label>
    									<input type="text" name="title" class="form-control" value="<?php echo $data[0]['title'] ?>">
    								</div>
                                    <input type="hidden" name="id" value="<?php echo $data[0]['id'] ?>">
    								<button type="submit" class="btn btn-success waves-effect waves-light m-b-5">Save</button>
    								<button class="btn btn-warning waves-effect waves-light m-b-5" id="back">Cancel</button>
                    				<a href="#" class="btn btn-danger waves-effect waves-light m-b-5" id="delete">Delete</a>
								</form>
								<!-- Content end -->
                            </div>
                        </div>
					</div>
            	</div> <!-- End Row -->