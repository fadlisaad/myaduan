<?php include('header.php'); ?>
		<div class="wrapper-page">
            <div class="panel panel-color panel-info panel-pages">

                <div class="panel-heading bg-img"> 
                    <div class="bg-overlay"></div>
                    <h3 class="text-center m-t-10 text-white"> Reset Password </h3>
                </div> 

                <div class="panel-body">
                 <form method="post" action="<?php echo BASE_URL ?>auth/process_reset_password" role="form" class="text-center"> 
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        Enter your <b>E-mail</b> and instructions will be sent to you!
                    </div>
                    <div class="form-group m-b-0">
                    	
                        <div class="input-group"> 
                            <input type="email" name="email" class="form-control input-lg" placeholder="Enter E-mail" required=""> 
                            <span class="input-group-btn"> <button type="submit" class="btn btn-lg btn-info waves-effect waves-light">Reset</button> </span> 
                        </div>
                        
                    </div>

                    <div class="form-group m-t-30">
                        <div class="col-sm-12 text-center">
                            <a href="<?php echo BASE_URL ?>"><i class="fa fa-lock"></i> Login</a>
                        </div>
                    </div>
                    
                </form>

                </div>                                 
                
            </div>
        </div>

        
    	<script>
            var resizefunc = [];
        </script>
<?php include('footer.php'); ?>