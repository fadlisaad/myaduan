<?php include('header.php'); ?>
		<div class="wrapper-page">
            <div class="panel panel-danger panel-pages">
                <div class="panel-heading"> 
                    <h3 class="text-center m-t-10 text-white">
                        Error 404
                    </h3>
                </div> 
                <div class="panel-body">
					<p class="text-center">
                        The page you're looking for is not available              
                    </p>
					<div class="text-center m-t-40">
						<button class="btn btn-danger btn-lg w-lg waves-effect waves-light" type="button" id="back">Return</button>
					</div>
				</div>
            </div>
        </div>
<?php include('footer.php'); ?>