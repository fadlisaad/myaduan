        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <img src="<?php echo BASE_URL ?>assets/images/logo.svg" style="height: 40px;margin-top: 10px;" class="pull-left"><a href="#" class="logo hidden-xs"><?php echo SITE_TITLE ?></a>
                    </div>
                    <!-- End Logo container-->

                    <div class="menu-extras">

                        <ul class="nav navbar-nav navbar-right pull-right">
                            
                        </ul>
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>

                </div>
            </div>
            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation"> 
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <li><a href="<?php echo BASE_URL ?>" title="Home"><i class="md md-home"></i> Home</a></li>
                            <li><a href="<?php echo BASE_URL ?>site/page/about">About</a></li>
                            <li><a href="<?php echo BASE_URL ?>site/page/help">Help</a></li>
                            <li><a href="<?php echo BASE_URL ?>site/page/contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- End Navigation Bar-->