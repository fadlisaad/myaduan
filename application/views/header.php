<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <title><?php echo SITE_TITLE ?></title>

        <link href="<?php echo BASE_URL; ?>assets/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
        <link href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo BASE_URL; ?>assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?php echo BASE_URL; ?>assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo BASE_URL; ?>assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?php echo BASE_URL; ?>assets/css/pages.css" rel="stylesheet" type="text/css">
        <link href="<?php echo BASE_URL; ?>assets/css/menu.css" rel="stylesheet" type="text/css">
        <link href="<?php echo BASE_URL; ?>assets/css/responsive.css" rel="stylesheet" type="text/css">

        <?php
        if(isset($css)){
            foreach ($css as $css_inc){
                echo '<link rel="stylesheet" href="'.BASE_URL.$css_inc.'" type="text/css">';
                echo "\n\t\t";
            }
        }

        if(isset($css_url)){
            foreach ($css_url as $css_inc_url){
                echo '<link rel="stylesheet" href="'.$css_inc_url.'" type="text/css">';
                echo "\n\t\t";
            }
        }
        

        if(isset($custom_css)){
            echo $custom_css;
            echo "\n\t";
        }
        ?>

        <link rel="shortcut icon" href="<?php echo BASE_URL; ?>assets/images/favicon.ico">

        <script src="<?php echo BASE_URL; ?>assets/js/modernizr.min.js"></script>
		<script src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>
	</head>
	
<body>