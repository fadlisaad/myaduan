<?php include('header.php'); ?>
		<div class="wrapper-page">
            <div class="panel panel-color panel-info panel-pages">

                <div class="panel-heading bg-img"> 
                    <div class="bg-overlay"></div>
                    <h3 class="text-center m-t-10 text-white"> Temporary Password </h3>
                </div> 

                <div class="panel-body">
                 <form method="post" action="<?php echo BASE_URL ?>auth/process_reset_password" role="form" class="text-center"> 
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        Please change your temporary password after login!
                    </div>
                    
                    <div class="stitched">
                        <?php echo $password; ?>
                    </div>

                    <div class="form-group m-t-30">
                        <div class="col-sm-12 text-center">
                            <a href="<?php echo BASE_URL ?>auth/login"><i class="fa fa-lock"></i> Login</a>
                        </div>
                    </div>
                    
                </form>

                </div>                                 
                
            </div>
        </div>
<?php include('footer.php'); ?>