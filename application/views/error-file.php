<?php include('header.php'); ?>
		<div class="wrapper-page">
            <div class="panel panel-danger panel-pages">
                <div class="panel-heading"> 
                    <h3 class="text-center m-t-10 text-white">
                        Upload Error
                    </h3>
                </div> 
                <div class="panel-body">
					<p class="text-center">
                        The file you're trying to upload is too big. The maximum limit is 4MB. Please try again.              
                    </p>
					<div class="text-center m-t-40">
						<button class="btn btn-danger btn-lg w-lg waves-effect waves-light" type="button" id="back">Return</button>
					</div>
				</div>
            </div>
        </div>
<?php include('footer.php'); ?>