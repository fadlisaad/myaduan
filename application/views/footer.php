				<!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                &copy; All copyright reserved 2016-<?php echo date('Y') ?> <?php global $config; echo $config['company'] ?>
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li><a href="<?php echo BASE_URL ?>site/page/about">About</a></li>
                                    <li><a href="<?php echo BASE_URL ?>site/page/help">Help</a></li>
                                    <li><a href="<?php echo BASE_URL ?>site/page/contact">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->

        </div>

        <!-- jQuery  -->
        <script src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo BASE_URL; ?>assets/js/detect.js"></script>
        <script src="<?php echo BASE_URL; ?>assets/js/fastclick.js"></script>
        <script src="<?php echo BASE_URL; ?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo BASE_URL; ?>assets/js/waves.js"></script>
        <script src="<?php echo BASE_URL; ?>assets/js/wow.min.js"></script>
        <script src="<?php echo BASE_URL; ?>assets/js/jquery.nicescroll.js"></script>
        <script src="<?php echo BASE_URL; ?>assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="<?php echo BASE_URL; ?>assets/js/jquery.app.js"></script>

        <!-- sweet alert  -->
        <script src="<?php echo BASE_URL; ?>assets/plugins/sweetalert/dist/sweetalert.min.js"></script>
        <script src="<?php echo BASE_URL; ?>assets/pages/jquery.sweet-alert.init.js"></script>

        <!-- Notify -->
        <script src="<?php echo BASE_URL; ?>assets/plugins/notify/notify.min.js"></script>
        <script src="<?php echo BASE_URL; ?>assets/plugins/notify/notify-metro.js"></script>
        <script src="<?php echo BASE_URL; ?>assets/plugins/notify/notifications.js"></script>

        <!-- Password -->
        <script src="<?php echo BASE_URL; ?>assets/plugins/password/hideShowPassword.min.js"></script>

        <!-- Plugins -->
        <?php

        if(isset($js_url)){
            foreach ($js_url as $js_inc){
                echo '<script src="'.$js_inc.'"></script>';
                echo "\n\t\t";
            }
        }

        if(isset($js)){
            foreach ($js as $js_inc){
                echo '<script src="'.BASE_URL.$js_inc.'"></script>';
                echo "\n\t\t";
            }
        }

        ?>

        <!-- base  -->
        <script src="<?php echo BASE_URL; ?>assets/js/base.js"></script>

        <!-- Page specific -->
        <?php
        if(isset($custom_js)){
            echo $custom_js;
            echo "\n\t";
        }
        ?>
</body>
</html>