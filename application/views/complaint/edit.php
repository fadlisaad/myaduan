<div class="wrapper">
	<div class="container">
		<!-- Start Header -->
		<div class="row">
            <div class="col-sm-12">
            	<div class="pull-right btn-group">
			</div>
			<div class="pull-right btn-group">
                <?php if($data[1]['assigned_name']): ?>
                	<?php
					switch ($data[0]['status']) {
						case 'open':
							$disabled = 'readonly';
							$button = "<button type=\"submit\" class=\"btn btn-primary waves-effect waves-light m-b\" name=\"processing\">Submit</button>";
							echo "<button type=\"button\" class=\"btn btn-success waves-effect waves-light m-b-5\" readonly>Open</button>";
							break;
						case 'processing':
							$disabled = '';
							$button = "<button type=\"submit\" class=\"btn btn-primary waves-effect waves-light m-b\" name=\"processing\">Submit</button>";
							echo "<button type=\"button\" class=\"btn btn-primary waves-effect waves-light m-b-5\" readonly>Processing</button>";
							break;
						case 'closed':
							$disabled = 'readonly';
							$button = '';
							echo "<button type=\"button\" class=\"btn btn-default waves-effect waves-light m-b-5\" readonly>Closed</button>";
							break;
					} ?>
					<button class="btn btn-default">Officer in-charge: <?php echo $data[1]['assigned_name'] ?></button>
				<?php else: ?>
					<?php
					switch ($data[0]['status']) {
						case 'draft':
							$disabled = '';
							$button = "<button type=\"submit\" class=\"btn btn-info waves-effect waves-light m-b\" id=\"save\">Save</button>
							<button type=\"submit\" class=\"btn btn-primary waves-effect waves-light m-b\" name=\"submitted\">Submit</button>";
							echo "<button type=\"button\" class=\"btn btn-warning waves-effect waves-light m-b-5\" readonly>Draft</button>";
							break;
						case 'submitted':
							$disabled = 'readonly';
							$button = "<button type=\"button\" class=\"btn btn-default waves-effect waves-light m-b-5\" readonly>Please allow the admin to assigned your complaint to the respective officer</button>";
							echo "<button type=\"button\" class=\"btn btn-info waves-effect waves-light m-b-5\" readonly>Submitted</button>";
							break;
					} ?>
					<button class="btn btn-default">Officer in-charge: Unassigned</button>
				<?php endif; ?>
				</div>
                <h4 class="page-title">Complaints &gt; View/Edit complaint</h4>
            </div>
        </div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-info panel-border">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-info-circle"></i> Complaint Details</h3>

					</div>
					<div class="panel-body">
						<div class="row">
						<form method="post" role="form" action="<?php echo BASE_URL ?>complaint/update" id="update-complaint" enctype="multipart/form-data">
							<div class="col-md-5 col-sm-12 col-xs-12">
								<div class="well">
									<div class="form-group">
										<label for="details">Details</label>
										<textarea <?php echo $disabled;?> name="details" class="form-control" required="" aria-required="true" rows="12"><?php echo $data[0]['details'] ?></textarea>
									</div>
									<div class="row">
										<div class="form-group col-md-6">
	                                        <label for="incident_date">Date of incident</label>
	                                        <div class="input-group">
	                                            <input <?php echo $disabled;?> type="text" name="incident_date" class="datepicker form-control" placeholder="yyyy-mm-dd" required data-date-start-date="1y" value="<?php echo $data[0]['incident_date'] ?>">
	                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
	                                        </div><!-- input-group -->
	                                    </div>
	                                    <div class="form-group col-md-6">
											<label for="incident_time">Time of Incident</label>
											<div class="input-group">
												<input <?php echo $disabled;?> type="text" name="incident_time" class="timepicker form-control" required value="<?php echo $data[0]['incident_time'] ?>">
												<span class="input-group-addon"><i class="glyphicon glyphicon-clock"></i></span>
											</div><!-- input-group -->
										</div>
									</div>
									<div class="row">
	                                    <div class="form-group col-md-6">
	                                        <label for="estimated_loss">Estimated Loss</label>
	                                        <div class="input-group">
	                                        	<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
	                                            <input <?php echo $disabled;?> type="text" name="estimated_loss" class="form-control" required value="<?php echo $data[0]['estimated_loss'] ?>">
	                                        </div><!-- input-group -->
	                                    </div>
									</div>
								</div>
								<div class="well">
									<div class="form-group">
										<label for="attachment">Attachment</label>
										<input <?php echo $disabled;?> type="file" id="fileToUpload" name="attachment" class="form-control">
										<?php if($attachment): ?>
										<table class="table table-striped">
											<thead>
												<tr>
													<th>File Name</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											<?php foreach ($attachment as $value) {
												if($value->size_in_mb == 0){
													$size = $value->size_in_bytes.' B';
												}else{
													$size = $value->size_in_mb.' MB';
												}
												echo '<tr>';
													echo '<td><a href="'.BASE_URL.'complaint/downloadFile/'.$value->id.'">'.$value->original_filename.'</a></td>';
													echo '<td><button type="button" class="delete-file btn btn-xs btn-danger" id="'.$value->id.'">X</button></td>';
												echo '</tr>';
											} ?>
											</tbody>
										</table>
									<?php else: ?>
										<p class="text-mute">No attachment</p>
									<?php endif; ?>
	              					</div>
	              				</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="well">
									<div class="form-group">
										<label for="company_name">Company Name</label>
										<input <?php echo $disabled;?> type="text" name="company_name" class="form-control" required="" value="<?php echo $data[0]['company_name'] ?>">
									</div>
									<div class="form-group">
										<label for="company_email">Company E-mail</label>
										<input <?php echo $disabled;?> type="text" name="company_email" class="form-control" value="<?php echo $data[0]['company_email'] ?>">
									</div>
									<div class="form-group">
										<label for="branch">Branch (if applicable)</label>
										<input <?php echo $disabled;?> type="text" name="branch" class="form-control" value="<?php echo $data[0]['branch'] ?>">
									</div>
									<div class="form-group">
										<label for="respondent_name">Respondent Name</label>
										<input <?php echo $disabled;?> type="text" name="respondent_name" class="form-control" value="<?php echo $data[0]['respondent_name'] ?>">
									</div>
									<div class="form-group">
										<label for="respondent_phone_no">Respondent phone number</label>
										<input <?php echo $disabled;?> type="text" name="respondent_phone_no" id="respondent_phone_no" class="form-control" value="<?php echo $data[0]['respondent_phone_no'] ?>">
									</div>
									<div class="form-group">
										<label for="address1">Address 1</label>
										<input <?php echo $disabled;?> type="text" name="address1" class="form-control" value="<?php echo $data[0]['address1'] ?>">
									</div>
									<div class="form-group">
										<label for="address2">Address 2</label>
										<input <?php echo $disabled;?> type="text" name="address2" class="form-control" value="<?php echo $data[0]['address2'] ?>">
									</div>
								</div>
							</div>
							
							<div class="col-md-3 col-sm-12 col-xs-12">
								<div class="well">
									<div class="form-group">
										<label for="postcode">Postcode</label>
										<input type="text" name="postcode" class="form-control" id="postcode" maxlength="5" value="<?php echo $data[0]['postcode'] ?>">
									</div>
									<div class="form-group">
										<label for="state_id">State</label>
										<select <?php echo $disabled;?> id="state_id" name="state_id" class="form-control">
											<option value="<?php echo $data[0]['state_id'] ?>"><?php echo $data[0]['state'] ?></option>
										</select>
									</div>
									<div class="form-group">
										<label for="city_id">City</label>
										<select <?php echo $disabled;?> id="city_id" name="city_id" class="form-control">
											<option value="<?php echo $data[0]['city_id'] ?>"><?php echo $data[0]['city'] ?></option>
										</select>
									</div>
								</div>

								<div class="well">
									<?php if($category[0]['sector_id'] > 0): ?>
									<div class="form-group">
										<label for="sector_id">Sector</label>
										<p class="form-control-static"><?php echo $category[0]['sector'] ?></p>
									</div>
									<?php endif; ?>

									<?php if($category[0]['category_id'] > 0): ?>
									<div class="form-group">
										<label for="category_id" class="control-label">Category</label>
										<p class="form-control-static"><?php echo $category[0]['category'] ?></p>
									</div>
									<?php endif; ?>

									<?php if($category[0]['subcategory_id'] > 0): ?>
									<div class="form-group">
		                                <label for="subcategory_id" class="control-label">Sub Category</label>
										<p class="form-control-static"><?php echo $category[0]['subcategory'] ?></p>
		                            </div>
		                        	<?php endif; ?>
		                        </div>
								
								<input type="hidden" name="id" value="<?php echo $data[0]['id'] ?>">
								<input type="hidden" name="status" value="<?php echo $data[0]['status'] ?>">
								<input type="hidden" name="user_id" value="<?php echo $data[0]['user_id'] ?>">
								<input type="hidden" name="tracking_no" value="<?php echo $data[1]['tracking_no'] ?>">
							</div>

							<?php if($data[0]['status'] == 'closed'): ?>
							<div class="col-md-4">
				                <div class="portlet">
				                    <div class="portlet-heading bg-primary">
				                        <h3 class="portlet-title">DESIRED OUTCOME</h3>
				                        <div class="clearfix"></div>
				                    </div>
				                    <div id="bg-default-1" class="panel-collapse collapse in">
				                        <div class="portlet-body">
				                            <?php
				                            	if($data[0]['status'] == 'closed') echo $data[0]['remarks'];
				                            	else echo "No desired outcome yet.";
				                            ?>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        	<?php endif; ?>
						</div>
					</div>
					<div class="panel-footer">
						<div class="btn-group">
							<?php echo $button ?>
							<button type="button" class="btn btn-warning waves-effect waves-light m-b-5" id="back">Back</button>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<?php if($data[1]['assigned_to']): ?>
		<?php if(in_array($_SESSION['permission'], array('user', 'officer'), true)): ?>
		<div class="row">
			<?php if(in_array($data[0]['status'], array('open', 'processing', 'submitted'), true)): ?>
            <div class="col-md-6">
                <!-- message -->
                <div class="panel panel-border panel-purple">
                    <div class="panel-heading">
                        <h3 class="panel-title">Chat</h3>
                    </div>
                    <div class="panel-body">
                        <form method="post" role="form" id="add-message">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Message</label>
                            <textarea class="form-control" name="message" rows="5"></textarea>
                        </div>
                        <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                        <input type="hidden" name="ticket_id" value="<?php echo $data[1]['id']; ?>">
                        <input type="hidden" name="channel_id" value="<?php echo $category[0]['assigned_to']; ?>">
                        <input type="hidden" name="tracking_no" value="<?php echo $category[0]['tracking_no']; ?>">
                        <input type="hidden" name="full_name" value="<?php echo ucfirst($data[0]['full_name']) ?>">
                        <input type="hidden" name="referer" value="complaint">
                    </div>
                    <div class="panel-footer">
                    	<button type="button" id="submit-message" class="btn btn-primary waves-effect waves-light m-b-5">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if($messages): arsort($messages);?>
            <div class="col-lg-6">
                <div class="panel panel-success panel-border">
                    <div class="panel-heading"> 
                        <h3 class="panel-title">Chat History</h3> 
                    </div> 
                    <div class="panel-body"> 
                        <div class="chat-conversation">
                            <ul class="conversation-list nicescroll" style="overflow: hidden; outline: none;" tabindex="5001">
                            <?php foreach($messages as $value):
                            if($_SESSION['user_id'] != $value->user_id) $li = "odd"; else $li = 'even'; ;
                            ?>
                                <li class="clearfix <?php echo $li ?>">
                                    <div class="chat-avatar">
                                        <img src="<?php echo BASE_URL."assets/images/users/".$value->avatar ?>" alt="male">
                                        <i><?php echo $helper->prettyDate($value->last_update) ?></i>
                                    </div>
                                    <div class="conversation-text">
                                        <div class="ctext-wrap">
                                            <i><?php echo $value->full_name ?></i>
                                            <p><?php echo htmlspecialchars($value->content, ENT_QUOTES) ?></p>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                            </ul>
                        </div>
                    </div> 
                </div>
            </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <?php endif; ?>
		<!-- End Row -->