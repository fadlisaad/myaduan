<div class="wrapper">
	<div class="container">
	
		<!-- Good Template -->
		<div id="notice_good" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
						<h4 class="modal-title" id="myModalLabel">Good Complaint</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-info">A good complaint must describe as accurate as possible all the details of the incident. Minimum wording must be not less than 100 characters</div>
						<h3>Bahasa Melayu</h3>
						<p>Saya telah melanggan service internet dengan syarikat XYZ pada 1 Nov 2017 dengan kadar bayaran RM100.00 sebulan (pakej Super Duper Deal 15Mbps). Pada 25 Feb 2018 saya telah menamatkan service internet degan menghadiri ke cawangan XYZ di Petaling Jaya, Selangor. Pada 5 Mei 2018 saya telah membuat semakan penyata akaun dan mendapati syarikat XYZ masih mengenakan bayaran walaupun service internet telahpun ditamatkan. Saya telah melampirkan borang penamatan saya dengan syarikat XYZ bersama dengan penyata bank saya sebagai bukti di sini untuk rujukan dan tindakan Tuan/Puan. Sekian aduan saya.. </p>
						<h3>English</h3>
						<p>I have visited ABC website to purchase a pair of shoes (model). The actual retail price of the item is RM500. However, ABC is selling the item at RM (lower/higher price). As the price was substantially lower as compare to the normal price, I then contacted the seller to confirm the authenticity of the goods and was told that the item is on sale and without giving much explanation as to the quality and the promotion period. The seller ID on ABC website is: WeSellFake101 (https://abcd.com/WeSellFake101...)</p>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
				
		<!-- Bed Template -->
		<div id="notice_bad" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
						<h4 class="modal-title" id="myModalLabel">Bad Complaint</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-danger">A bad complaint wouldn't be sufficient for us to help with the investigation of the complaint later</div>
						<h3>Bahasa Melayu</h3>
						<p>Dikenakan bayaran selama beberapa bulan walaupun servis internet telah dipotong.</p>
						<h3>English</h3>
						<p>Display price of the goods differed</p>
					 </div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
		<!-- Start Header -->
		<div class="row">
            <div class="col-sm-12">
            	<div class="pull-right btn-group">
            		<button class="btn btn-info" data-toggle="modal" data-target="#notice_good">Good Complaint</button>
            		<button class="btn btn-danger" data-toggle="modal" data-target="#notice_bad">Bad Complaint</button>
            	</div>
                <h4 class="page-title">Ticket &gt; Add new ticket</h4>
			</div>
        </div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-border panel-info">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-info-circle"></i> Ticket Details</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<form method="post" role="form" id="add-complaint">
								<div class="col-md-5 col-sm-12 col-xs-12">
									<!-- Content start -->
									<input type="hidden" name="user_id" value="<?php echo $session->get('user_id'); ?>">
									<div class="form-group">
										<label for="details">Your Complaint</label>
										<textarea name="details" class="form-control" required rows="12" data-minlength="100"></textarea>
										<div class="help-block with-errors">Please refer to the good complaint template on how to write a good complaint.</div>
									</div>
									<div class="row">
										<div class="form-group col-md-6">
											<label for="incident_date">Date of incident</label>
											<div class="input-group">
												<input type="text" name="incident_date" class="datepicker form-control" required data-date-end-date="0d">
												<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											</div><!-- input-group -->
											<div class="help-block with-errors">Please choose the incident date</div>
										</div>
										<div class="form-group col-md-6">
											<label for="incident_time">Time of Incident</label>
											<div class="input-group">
												<input type="text" name="incident_time" class="timepicker form-control" required>
												<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
											</div><!-- input-group -->
											<div class="help-block with-errors">Please choose the incident time</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-6">
											<label for="estimated_loss">Estimated Loss</label>
											<div class="input-group">
												<span class="input-group-addon">RM</span>
												<input type="text" name="estimated_loss" id="estimated_loss" class="form-control" required>
											</div><!-- input-group -->
											<span class="help-block has-success">Please write the estimated amount of losses</span>
										</div>
									</div>
									<div class="form-group">
										<label for="attachment">Attachment</label>
										<p>You can upload your attachment once your complaint has been accepted by the officer.</p>
									</div>
								</div>
								<div class="col-md-4 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="company_name">Company Name</label>
										<input type="text" name="company_name" class="form-control" required>
										<div class="help-block with-errors">The company you're complaining to</div>
									</div>
									<div class="form-group">
										<label for="company_email">Company E-mail</label>
										<input type="text" name="company_email" class="form-control">
										<div class="help-block with-errors">The e-mail of the company you're complaining to (if any)</div>
									</div>
									<div class="form-group">
										<label for="branch">Branch (if applicable)</label>
										<input type="text" name="branch" class="form-control">
									</div>
									<div class="form-group">
										<label for="respondent_name">Respondent Name</label>
										<input type="text" name="respondent_name" class="form-control">
										<div class="help-block with-errors">The person in-charge of the company you're complaining to (if any)</div>
									</div>
									<div class="form-group">
										<label for="respondent_phone_no">Respondent phone number</label>
										<input type="text" name="respondent_phone_no" id="respondent_phone_no" class="form-control">
										<div class="help-block with-errors">The person in-charge's phone number of the company you're complaining to (if any)</div>
									</div>
									<div class="form-group">
										<label for="address1">Company Address 1</label>
										<input type="text" name="address1" class="form-control">
									</div>
									<div class="form-group">
										<label for="address2">Company Address 2</label>
										<input type="text" name="address2" class="form-control">
									</div>
									<div class="form-group">
										<label for="postcode">Postcode</label>
										<input type="text" name="postcode" id="postcode" class="form-control" maxlength="5">
									</div>

									<div class="row">
										<div class="form-group col-md-6">
											<label for="state_id">State</label>
											<select id="state_id" name="state_id" class="form-control" required></select>
											<div class="help-block with-errors">Please select the state</div>
										</div>
										<div class="form-group col-md-6">
											<label for="city_id">City</label>
											<select id="city_id" name="city_id" class="form-control" required></select>
											<div class="help-block with-errors">Please select the nearest city</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-12 col-xs-12">
									
									<div class="form-group">
										<label for="sector_id" class="control-label">Sector</label>
										<select id="sector_id" name="sector_id" class="form-control" required></select>
										<div class="help-block with-errors"></div>
									</div>
									<div class="form-group">
										<label for="category_id" class="control-label">Category</label>
										<select id="category_id" name="category_id" class="form-control" required></select>
										<div class="help-block with-errors"></div>
									</div>

									<?php if($_SESSION['permission'] != 'user'): ?>
									<div class="form-group">
										<label for="subcategory_id" class="control-label">Sub Category</label>
										<select id="subcategory_id" name="subcategory_id" class="form-control" required></select>
										<div class="help-block with-errors"></div>
									</div>
									<div class="form-group">
										<label for="type_id" class="control-label">Complaint Type</label>
										<select id="type_id" name="type_id" class="form-control" required></select>
										<div class="help-block with-errors"></div>
									</div>
									<?php else: ?>
									<input type="hidden" name="subcategory_id" value="0">
									<input type="hidden" name="type_id" value="0">
									<?php endif; ?>

									<div class="alert alert-info">
										<p>Save as draft: Your complaint is saved, you can edit it later</p>
										<p>Submit: Your complaint is submitted, and wait for the admin to assigned to the respective officer</p>
									</div>

								</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-info waves-effect waves-light" name="draft">Save as Draft</button>
						<button type="submit" class="btn btn-success waves-effect waves-light" name="submit">Submit</button>
						<button class="btn btn-warning waves-effect waves-light pull-left" id="back">Cancel</button>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!-- End Row -->