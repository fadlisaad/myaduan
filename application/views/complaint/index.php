
	<div class="wrapper">
		<div class="container">
			<!-- Start Header -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo BASE_URL ?>complaint/add">Add new complaint</a></li>
                        </ul>
                    </div>
                    <h4 class="page-title">Complaints</h4>
                </div>
            </div>

			<div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">List of Complaints</h3>
                        </div>
                        <div class="panel-body">
                            <table id="datatable" class="table table-striped table-bordered">
								<thead>
									<tr>
                                        <th>user_id</th>
                                        <th>Company</th>
										<th>Date of Incident</th>
                                        <th>Loss (RM)</th>
                                        <th>Submitted by</th>
                                        <th>Status</th>
                                        <th>Action</th>
									</tr>
								</thead>
							</table>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->