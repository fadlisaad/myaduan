
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo BASE_URL ?>complaint/add">Add new ticket</a></li>
                        </ul>
                    </div>
                        <h4 class="page-title">Tickets</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-border panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">List of Tickets</h3>
                            </div>
                            <div class="panel-body">
                                <table id="table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Tracking #</th>
                                            <th>Date</th>
                                            <th>Company Name</th>
                                            <th>Full Name</th>
                                            <th>Phone No</th>
                                            <th>IC/Passport</th>
                                            <th>E-mail</th>
                                            <th>Status</th>
                                            <th>View</th>
                                            <th>Unread</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Row -->