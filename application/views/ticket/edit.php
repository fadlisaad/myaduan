<div class="wrapper">
    <div class="container">
        <!-- Start Header -->
        <?php
            $button_disabled = '';
            $disabled = '';
        ?>

        <!-- Start Complainant details -->
        <div class="row">
            <div class="col-md-9">
                <?php
                if ($_SESSION['permission'] == 'officer' || $_SESSION['permission'] == 'trainee') {
                    if ($complaint[0]['status'] == 'closed') {
                        $button_disabled = 'disabled';
                    } else {
                        $button_disabled = '';
                    }
                    $dropdown = 'disabled';
                } else {
                    $dropdown = '';
                }
                ?>
                <div class="panel panel-border panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-info-circle"></i> Complaint Details</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <form class="update-complaint">
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="details">Details</label>
                                        <textarea name="details" class="form-control" required="" aria-required="true" rows="12"><?php echo $complaint[0]['details'] ?></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="incident_date">Date of incident</label>
                                            <div class="input-group">
                                                <input type="text" name="incident_date" class="datepicker form-control" placeholder="yyyy-mm-dd" required data-date-start-date="1y" value="<?php echo $complaint[0]['incident_date']; ?>">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="incident_time">Time of Incident</label>
                                            <div class="input-group">
                                                <input type="text" name="incident_time" class="timepicker form-control" required value="<?php echo $complaint[0]['incident_time'] ?>">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-clock"></i></span>
                                            </div><!-- input-group -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="estimated_loss">Estimated Loss</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                            <input type="digit" name="estimated_loss" class="form-control" required value="<?php echo $complaint[0]['estimated_loss'] ?>">
                                        </div><!-- input-group -->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="company_name">Company Name</label>
                                        <input type="text" name="company_name" class="form-control" required="" value="<?php echo $complaint[0]['company_name'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="company_email">Company E-mail</label>
                                        <input type="text" name="company_email" class="form-control" value="<?php echo $complaint[0]['company_email'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="branch">Branch (if applicable)</label>
                                        <input type="text" name="branch" class="form-control" value="<?php echo $complaint[0]['branch'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="respondent_name">Respondent Name</label>
                                        <input type="text" name="respondent_name" class="form-control" value="<?php echo $complaint[0]['respondent_name'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="respondent_phone_no">Respondent phone number</label>
                                        <input type="text" name="respondent_phone_no" id="respondent_phone_no" class="form-control" value="<?php echo $complaint[0]['respondent_phone_no'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="address1">Address 1</label>
                                        <input type="text" name="address1" class="form-control" value="<?php echo $complaint[0]['address1'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="address2">Address 2</label>
                                        <input type="text" name="address2" class="form-control" value="<?php echo $complaint[0]['address2'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="postcode">Postcode</label>
                                        <input type="number" name="postcode" id="postcode" class="form-control" maxlength="5" value="<?php echo $complaint[0]['postcode'] ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="state_id">State</label>
                                        <select id="state_id" name="state_id" class="form-control">
                                            <option value="<?php echo $complaint[0]['state_id'] ?>"><?php echo $complaint[0]['state'] ?></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="city_id">City</label>
                                        <select id="city_id" name="city_id" class="form-control">
                                            <option value="<?php echo $complaint[0]['city_id'] ?>"><?php echo $complaint[0]['city'] ?></option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="id" value="<?php echo $complaint[0]['id'] ?>">
                                    <input type="hidden" name="user_id" value="<?php echo $complaint[0]['user_id'] ?>">
                                    <button id="submit-update-complaint" style="float:right;" <?php echo $button_disabled; ?> class="btn btn-success waves-effect waves-light m-b">Save</button>
                                </div>
                            </form>

                            <div class="col-md-4">
                                <form class="update-ticket">
                                    <input type="hidden" name="complaint_id" value="<?php echo $complaint[0]['id'] ?>">
                                    <input type="hidden" name="id" value="<?php echo $ticket[0]['id'] ?>">
                                    <input type="hidden" id="assigned_by" name="assigned_by" value="<?php echo $_SESSION['user_id'] ?>">
                                    <input type="hidden" id="assigned_date" name="assigned_date" value="<?php echo date('Y-m-d H:i:s') ?>">
                                    <div class="form-group">
                                        <label for="sector_id" class="control-label">Sector</label>
                                        <select id="sector_id" name="sector_id" <?php echo $button_disabled; ?> class="form-control">
                                            <option value="<?php echo $ticket[0]['sector_id']; ?>" selected=""><?php echo $ticket[0]['sector']; ?></option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="category_id" class="control-label">Category <?php $cat = $ticket[0]['category']; ?></label>
                                        <select id="category_id" name="category_id" <?php echo $button_disabled; ?> class="form-control select2">
                                            <option value="<?php echo $ticket[0]['category_id']; ?>" selected="selected"><?php echo $cat; ?></option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="subcategory_id" class="control-label">Sub Category</label>
                                        <select id="subcategory_id" name="subcategory_id" <?php echo $button_disabled; ?> class="form-control select2">
                                            <option value="<?php echo $ticket[0]['subcategory_id'] ?>" selected=""><?php echo $ticket[0]['subcategory'] ?></option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="type_id" class="control-label">Complaint Type</label>
                                        <select id="type_id" name="type_id" <?php echo $button_disabled; ?> class="form-control select2">
                                            <option value="<?php echo $ticket[0]['type_id'] ?>" selected=""><?php echo $ticket[0]['type'] ?></option>
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="priority" class="control-label">Priority</label>
                                                <select id="priority" <?php echo $button_disabled; ?> name="priority" class="form-control select2">
                                                    <option value="High" <?php if($ticket[0]['priority'] == 'High'){ echo 'selected'; } ?>>High</option>
                                                    <option value="Medium" <?php if($ticket[0]['priority'] == 'Medium'){ echo 'selected'; }?>>Medium</option>
                                                    <option value="Low" <?php if($ticket[0]['priority'] == 'Low'){ echo 'selected'; } ?>>Low</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="assigned_to" class="control-label">Assign to</label>
                                                <select id="assigned_to" <?php echo $button_disabled; ?> name="assigned_to" class="select2 form-control" required="">
                                                    <option value="<?php echo $ticket[0]['assigned_to'] ?>" selected=""><?php echo $ticket[0]['assigned_name'] ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet">
                                        <div class="portlet-heading bg-info">
                                            <h3 class="portlet-title ">
                                                REFERENCE
                                            </h3>
                                            <div class="portlet-widgets">
                                                <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                                <span class="divider"></span>
                                                <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i class="ion-minus-round"></i></a>
                                                <span class="divider"></span>
                                                <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div id="bg-default" class="panel-collapse collapse in">
                                            <div class="portlet-body">
                                                <dl class="dl-horizontal">
                                                    <dt>Tracking #</dt>
                                                    <dd><?php echo $ticket[0]['tracking_no'] ?></dd>
                                                    <dt>Status</dt>
                                                    <dd>
                                                    <?php
                                                    switch ($complaint[0]['status']) {
                                                        case 'draft': echo "<span class=\"label label-default\">Draft</span>";
                                                            break;
                                                        case 'submitted': echo "<span class=\"label label-info\">Submitted</span>";
                                                            break;
                                                        case 'open': echo "<span class=\"label label-success\">Open</span>";
                                                            break;
                                                        case 'processing': echo "<span class=\"label label-primary\">Processing</span>";
                                                            break;
                                                        case 'closed': echo "<span class=\"label label-danger\">Closed</span>";
                                                            break;
                                                    }
                                                    ?></dd>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <div class="portlet">
                                    <div class="portlet-heading bg-primary">
                                        <h3 class="portlet-title ">
                                            ATTACHMENT
                                        </h3>
                                        <div class="portlet-widgets">
                                            <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                            <span class="divider"></span>
                                            <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default-1"><i class="ion-minus-round"></i></a>
                                            <span class="divider"></span>
                                            <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="bg-default-1" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <form class="upload-file" enctype="multipart/form-data" action="<?php echo BASE_URL ?>ticket/attachment" method="post">
                                                <div class="form-group">
                                                    <label for="attachment">Maximum size 4MB</label>
                                                    <?php if ($attachment): ?>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>File Name</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                foreach ($attachment as $value) {
                                                                    if ($value->size_in_mb == 0) {
                                                                        $size = $value->size_in_bytes . ' B';
                                                                    } else {
                                                                        $size = $value->size_in_mb . ' MB';
                                                                    }
                                                                    echo '<tr>';
                                                                    echo '<td><a href="' . BASE_URL . 'complaint/downloadFile/' . $value->id . '">' . $value->original_filename . '</a></td>';
                                                                    echo '<td><button type="button" class="delete-file btn btn-xs btn-danger" id="'.$value->id.'">X</button></td>';
                                                                    echo '</tr>';
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    <?php else: ?>
                                                        <p class="text-mute">No attachment</p>
                                                    <?php endif; ?>
                                                    <input type="file" name="attachment" id="fileToUpload">
                                                </div>
                                                <input type="hidden" name="ticket_id" value="<?php echo $id; ?>">
                                                <input type="hidden" name="complaint_id" value="<?php echo $complaint[0]['id']; ?>">
                                                <button class="btn btn-success waves-effect waves-light m-b-5" id="upload-file" type="submit">Upload File</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="mini-stat clearfix bx-shadow bg-info">
                    <span class="mini-stat-icon"><img src="<?php echo BASE_URL ?>assets/images/users/<?php echo $user[0]['avatar'] ?>" alt="" class="img-circle img-responsive"></span>
                    <div class="mini-stat-info text-right ">
                        <span class="name "><?php echo ucfirst($user[0]['full_name']) ?></span>
                        <?php echo ucfirst($user[0]['permission']) ?><br>
                        <?php echo strtolower($user[0]['email']) ?>
                    </div>
                    <br>
                    <hr class="m-t-10">
                    <dl class="">
                        <dt>Address</dt>
                        <dd><?php echo ucfirst($user[0]['address']) ?></dd>
                        <dt>Income Range</dt>
                        <dd><?php echo ucfirst($user[0]['income']) ?></dd>
                        <dt>Ethnicity</dt>
                        <dd><?php echo ucfirst($user[0]['ethnicity']) ?></dd>
                        <dt>IC/Passport</dt>
                        <dd><?php echo strtoupper($user[0]['ic_passport']) ?></dd>
                        <dt>Age Range</dt>
                        <dd><?php echo strtoupper($user[0]['age']) ?></dd>
                    </dl>
                </div>
                <?php if(in_array($_SESSION['permission'], array('user', 'officer', 'trainee'), true)): ?>
                    <?php if(in_array($complaint[0]['status'], array('open', 'processing', 'submitted'), true)): ?>
                    <!-- message -->
                    <div class="panel panel-border panel-purple">
                        <div class="panel-heading">
                            <h3 class="panel-title">Chat</h3>
                        </div>
                        <div class="panel-body">
                            <form method="post" role="form" id="add-message">
                            <div class="form-group">
                                <textarea class="form-control" name="message" rows="3" placeholder="Enter your text here and press submit"></textarea>
                            </div>
                            <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                            <input type="hidden" name="channel_id" value="<?php echo $channel; ?>">
                            <input type="hidden" name="ticket_id" value="<?php echo $id; ?>">
                            <input type="hidden" name="tracking_no" value="<?php echo $ticket[0]['tracking_no']; ?>">
                            <input type="hidden" name="full_name" value="<?php echo ucfirst($user[0]['full_name']) ?>">
                            <input type="hidden" name="referer" value="ticket">
                        </div>
                        <div class="panel-footer">
                            <button type="button" id="submit-message" class="btn btn-primary waves-effect waves-light m-b-5">Submit</button>
                            </form>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if($messages): arsort($messages); ?>
                    <div class="panel panel-info panel-border">
                        <div class="panel-heading"> 
                            <h3 class="panel-title">Chat History</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <div class="chat-conversation">
                                <ul class="conversation-list nicescroll" style="overflow: hidden; outline: none;" tabindex="5001">
                                <?php foreach($messages as $value): ?>
                                    <li class="clearfix <?php if($_SESSION['user_id'] == $ticket[0]['user_id']) echo "odd"; ?>">
                                        <div class="chat-avatar">
                                            <img src="<?php echo BASE_URL ?>assets/images/users/<?php echo $value->avatar ?>" alt="male">
                                        </div>
                                        <div class="conversation-text">
                                            <div class="ctext-wrap">
                                                <i><?php echo $value->full_name ?> <span><?php echo $helper->prettyDate($value->last_update) ?></span></i>
                                                <p><?php echo htmlspecialchars($value->content, ENT_QUOTES) ?></p>
                                            </div>
                                        </div>
                                        <?php if($value->read == 'no') echo "<button class=\"btn btn-xs pull-right read-message\" data-id=\"".$value->id."\">Mark as read</button>"; ?>
                                    </li>
                                <?php endforeach; ?>
                                </ul>
                            </div>
                        </div> 
                    </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="portlet">
                    <div class="portlet-heading bg-danger">
                        <h3 class="portlet-title">TICKET CLOSURE</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div id="bg-default-1" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <form class="update-ticket-status form-inline">
                                <div class="form-group">
                                    <label class="sr-only">Status</label>
                                    <select id="status" name="status" class="form-control">        
                                        <option value="draft" <?php if($complaint[0]['status'] == 'draft'){ echo 'selected'; } ?>>Draft</option>
                                        <option value="submitted" <?php if($complaint[0]['status'] == 'submitted'){ echo 'selected'; } ?>>Submitted</option>
                                        <option value="open" <?php if($complaint[0]['status'] == 'open'){ echo 'selected'; } ?>>Open</option>
                                        <option value="processing" <?php if($complaint[0]['status'] == 'processing'){ echo 'selected'; } ?>>Processing</option>
                                        <option value="closed" <?php if($complaint[0]['status'] == 'closed'){ echo 'selected'; } ?>>Closed</option>
                                    </select>
                                </div>

                                <input type="hidden" name="open_date" value="<?php echo $ticket[0]['open_date'] ?>">
                                <input type="hidden" name="assigned_date" value="<?php echo $ticket[0]['assigned_date'] ?>">
                                <input type="hidden" name="complaint_id" value="<?php echo $complaint[0]['id'] ?>">
                                <input type="hidden" name="close_by" value="<?php echo $_SESSION['user_id'] ?>">
                                <input type="hidden" name="id" value="<?php echo $ticket[0]['id'] ?>">
                                <input type="hidden" name="user_id" value="<?php echo $ticket[0]['user_id'] ?>">
                                <input type="hidden" name="tracking_no" value="<?php echo $ticket[0]['tracking_no'] ?>">

                                <?php
                                if (in_array($_SESSION['permission'], array('officer', 'trainee'), true)) {
                                    if ($complaint[0]['status'] == 'processing') {
                                        ?><button type="button" <?php echo $button_disabled; ?> class="btn btn-danger waves-effect waves-light m-b-5"data-toggle="modal" data-target="#resolution">Close Ticket</button>
                                        <?php
                                    }

                                    if ($complaint[0]['status'] == 'closed') {
                                        ?><button type="button" class="btn btn-success waves-effect waves-light m-b-5" id="submit-status-ticket" rel="reopen">Reopen Ticket</button>
                                        <?php
                                    }
                                    
                                } else {
                                    ?><button <?php echo $button_disabled; ?> class="btn btn-success waves-effect waves-light m-b-5" id="submit-status-ticket" rel="update">Update Ticket</button>
                                <?php } ?>
                                <a class="btn btn-primary waves-effect waves-light m-b-5" data-toggle="modal" data-target="#preview"><i class="fa fa-print"></i> Print Preview</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="portlet">
                    <div class="portlet-heading bg-primary">
                        <h3 class="portlet-title">DESIRED OUTCOME</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div id="bg-default-1" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <?php echo $complaint[0]['remarks'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Ticket resolution -->
        <div id="resolution" class="modal fade" role="dialog" aria-labelledby="ticketResolution" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="modal-title" id="ticketResolution">Ticket #<?php echo $ticket[0]['tracking_no'] ?> Resolution</h4>
                    </div>
                    <div class="modal-body" id="content">
                        <form method="post" role="form" id="update-remarks">
                        <div class="form-group">
                            <label for="details">Please state the root cause of this ticket in order to close this ticket.</label>
                            <p><input type="radio" id="res1" name="resolution"> Resolved with Respondent</p>
                            <p><input type="radio" id="res2" name="resolution"> Not Resolved with Respondent</p>
                            <p><input type="radio" id="res3" name="resolution"> Other, please specify:</p>
                            <textarea id="remarks-text" name="remarks" class="form-control" rows="7"><?php echo $complaint[0]['remarks'] ?></textarea>
                        </div>
                        <input type="hidden" name="complaint_id" value="<?php echo $complaint[0]['id'] ?>">
                        <input type="hidden" name="ticket_id" value="<?php echo $id ?>">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-info" type="button" id="submit-resolution-ticket">Submit</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Start print preview -->
        <div id="preview" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="modal-title" id="myModalLabel">Ticket #<?php echo $ticket[0]['tracking_no'] ?></h4>
                    </div>
                    <div class="modal-body" id="content">
                        <h3>Personal Information</h3>
                            
                        <dl class="dl-horizontal dl-larger">
                            <dt>Name</dt>
                            <dd><?php echo ucfirst($user[0]['full_name']) ?></dd>
                            <dt>Permission</dt>
                            <dd><?php echo ucfirst($user[0]['permission']) ?></dd>
                            <dt>E-mail Address</dt>
                            <dd><?php echo strtolower($user[0]['email']) ?></dd>
                            <dt>Address</dt>
                            <dd><?php echo ucfirst($user[0]['address']) ?></dd>
                            <dt>Income Range</dt>
                            <dd><?php echo ucfirst($user[0]['income']) ?></dd>
                            <dt>Ethnicity</dt>
                            <dd><?php echo ucfirst($user[0]['ethnicity']) ?></dd>
                            <dt>IC/Passport number</dt>
                            <dd><?php echo strtoupper($user[0]['ic_passport']) ?></dd>
                            <dt>Age Range</dt>
                            <dd><?php echo strtoupper($user[0]['age']) ?></dd>
                        </dl>

                        <h3>Complaint Details</h3>
                        <dl class="dl-horizontal dl-larger">
                            <dt>Details</dt>
                            <dd><?php echo $complaint[0]['details'] ?></dd>
                            <dt>Date of Incident</dt>
                            <dd><?php echo $complaint[0]['incident_date'] ?></dd>
                            <dt>Time of Incident</dt>
                            <dd><?php echo $complaint[0]['incident_time'] ?></dd>
                            <dt>Estimated Loss</dt>
                            <dd>RM <?php echo number_format($complaint[0]['estimated_loss'],2) ?></dd>
                        </dl>

                        <h3>Respondent Details</h3>
                        <dl class="dl-horizontal dl-larger">
                            <dt>Company Name</dt>
                            <dd><?php echo $complaint[0]['company_name'] ?></dd>
                            <dt>Company E-mail</dt>
                            <dd><?php echo $complaint[0]['company_email'] ?></dd>
                            <dt>Branch</dt>
                            <dd><?php echo $complaint[0]['branch'] ?></dd>
                            <dt>Respondent Name</dt>
                            <dd><?php echo $complaint[0]['respondent_name'] ?></dd>
                            <dt>Respondent Phone No.</dt>
                            <dd><?php echo $complaint[0]['respondent_phone_no'] ?></dd>
                            <dt>Address</dt>
                            <dd><?php echo $complaint[0]['address1']."&nbsp;".$complaint[0]['address2'] ?></dd>
                            <dt>Postcode</dt>
                            <dd><?php echo $complaint[0]['postcode'] ?></dd>
                            <dt>State</dt>
                            <dd><?php echo $complaint[0]['state'] ?></dd>
                            <dt>City</dt>
                            <dd><?php echo $complaint[0]['city'] ?></dd>
                        </dl>

                        <h3>Ticket Details</h3>
                        <dl class="dl-horizontal dl-larger">
                            <dt>Sector</dt>
                            <dd><?php echo $ticket[0]['sector'] ?></dd>
                            <dt>Category</dt>
                            <dd><?php echo $ticket[0]['category'] ?></dd>
                            <dt>Sub-category</dt>
                            <dd><?php echo $ticket[0]['subcategory'] ?></dd>
                            <dt>Complaint Type</dt>
                            <dd><?php echo $ticket[0]['type'] ?></dd>
                            <dt>Priority</dt>
                            <dd><?php echo $ticket[0]['priority'] ?></dd>
                            <dt>Officer in-charge</dt>
                            <dd><?php echo $ticket[0]['assigned_name'] ?></dd>
                            <dt>Ticket Status</dt>
                            <dd><?php echo $ticket[0]['status'] ?></dd>
                            <dt>Ticket Resolution</dt>
                            <dd><?php echo $complaint[0]['remarks'] ?></dd>
                        </dl>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success"><i class="fa fa-print" id="print-modal"></i> Print</button>
                        <button class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
</div>