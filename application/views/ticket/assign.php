<div class="wrapper">
	<div class="container">
		<!-- Start Header -->
		<div class="row">
            <div class="col-sm-12">
            	<div class="btn-group pull-right">
                    <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a data-toggle="modal" data-target="#create-ticket">Create ticket</a></li>
                    </ul>
                </div>
                <h4 class="page-title">Complaints Details</h4>
            </div>
        </div>
        <!-- Create ticket to agent -->
        <div id="create-ticket" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="modal-title">Create Ticket</h4>
                    </div>
                    <form class="create-ticket">
                    	<input type="hidden" name="tracking_no" value="<?php echo uniqid() ?>">
                    	<input type="hidden" name="complaint_id" value="<?php echo $complaint[0]['id'] ?>">
                    	<input type="hidden" name="assigned_by" value="<?php echo $_SESSION['user_id'] ?>">
                    	<input type="hidden" name="open_date" value="<?php echo date('Y-m-d H:i:s') ?>">
	                    <div class="modal-body">
	                        <div class="row">
	                            <div class="col-md-6">
	                                <div class="form-group">
	                                    <label for="sector_id" class="control-label">Sector</label>
	                                    <select id="sector_id" name="sector_id" class="form-control" required=""></select>
	                                </div>
	                            </div>
	                            <div class="col-md-6">
	                                <div class="form-group">
	                                    <label for="category_id" class="control-label">Category</label>
	                                    <select id="category_id" name="category_id" class="form-control" required=""></select>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-12">
	                                <div class="form-group">
	                                    <label for="subcategory_id" class="control-label">Sub Category</label>
	                                    <select id="subcategory_id" name="subcategory_id" class="form-control" required=""></select>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-12">
	                                <div class="form-group">
	                                    <label for="type_id" class="control-label">Complaint Type</label>
	                                    <select id="type_id" name="type_id" class="form-control" required=""></select>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-6">
	                                <div class="form-group">
	                                    <label for="priority" class="control-label">Priority</label>
	                                    <select id="priority" name="priority" class="form-control" required="">
	                                    	<option value="Critical">Critical</option>
	                                    	<option value="High">High</option>
	                                    	<option value="Medium" selected>Medium</option>
	                                    	<option value="Low">Low</option>
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="col-md-6">
	                            	<div class="form-group">
	                                    <label for="assigned_to" class="control-label">Assign to</label>
	                                    <select id="assigned_to" name="assigned_to" class="select2 form-control" required=""></select>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info waves-effect waves-light" id="submit-create-ticket" data-dismiss="modal">Save changes</button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-color panel-info">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-info-circle"></i> Complaint Details</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12">
							<!-- Content start -->
								<form method="post" role="form" action="<?php echo BASE_URL ?>complaint/update" id="update-complaint">
									<div class="form-group">
										<label for="details">Details</label>
										<textarea name="details" class="form-control" required="" aria-required="true" rows="12"><?php echo $complaint[0]['details'] ?></textarea>
									</div>
									<div class="row">
										<div class="form-group col-md-6">
	                                        <label for="incident_date">Date of incident</label>
	                                        <div class="input-group">
	                                            <input type="text" name="incident_date" class="datepicker form-control" placeholder="yyyy-mm-dd" required data-date-start-date="1y" value="<?php echo $complaint[0]['incident_date'] ?>">
	                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
	                                        </div><!-- input-group -->
	                                    </div>
	                                </div>
	                                <div class="form-group">
	                                    <label for="estimated_loss">Estimated Loss</label>
	                                    <div class="input-group">
	                                    	<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
	                                        <input type="digit" name="estimated_loss" class="form-control" required value="<?php echo $complaint[0]['estimated_loss'] ?>">
	                                    </div><!-- input-group -->
	                                </div>
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="company_name">Company Name</label>
										<input type="text" name="company_name" class="form-control" required="" value="<?php echo $complaint[0]['company_name'] ?>">
									</div>
									<div class="form-group">
										<label for="company_email">Company E-mail</label>
										<input type="text" name="company_email" class="form-control" required="" value="<?php echo $complaint[0]['company_email'] ?>">
									</div>
									<div class="form-group">
										<label for="address1">Address 1</label>
										<input type="text" name="address1" class="form-control" value="<?php echo $complaint[0]['address1'] ?>">
									</div>
									<div class="form-group">
										<label for="address2">Address 2</label>
										<input type="text" name="address2" class="form-control" value="<?php echo $complaint[0]['address2'] ?>">
									</div>
									<div class="form-group">
										<label for="postcode">Postcode</label>
										<input type="number" name="postcode" class="form-control" maxlength="5" value="<?php echo $complaint[0]['postcode'] ?>">
									</div>
									<div class="form-group">
										<label for="state_id">State</label>
										<select id="state_id" name="state_id" class="form-control">
											<option value="<?php echo $complaint[0]['state_id'] ?>"><?php echo $complaint[0]['state'] ?></option>
										</select>
									</div>
									<div class="form-group">
										<label for="city_id">City</label>
										<select id="city_id" name="city_id" class="form-control">
											<option value="<?php echo $complaint[0]['city_id'] ?>"><?php echo $complaint[0]['city'] ?></option>
										</select>
									</div>
									<input type="hidden" name="id" value="<?php echo $complaint[0]['id'] ?>">
									<input type="hidden" name="user_id" value="<?php echo $complaint[0]['user_id'] ?>">

									<button type="submit" class="btn btn-danger waves-effect waves-light m-b-5" name="draft">Mark as incomplete</button>
									<button type="submit" class="btn btn-success waves-effect waves-light m-b-5" name="submitted">Save</button>
									<button class="btn btn-warning waves-effect waves-light m-b-5" id="back">Cancel</button>
								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<form class="upload-file" enctype="multipart">
									<div class="form-group">
										<label for="attachment">Attachment</label>
										<input type="file" name="attachment" class="form-control">
		          					</div>
		          					<input type="hidden" name="file_id" value="<?php echo $complaint[0]['id'] ?>">
		          					<input type="hidden" name="file_type" value="complaint">
		          					<button id="upload-file" type="button" class="btn btn-purple waves-effect">Upload</button>
		          				</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
				<!-- End Row -->