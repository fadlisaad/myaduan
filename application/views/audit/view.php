<?php include('header.php'); ?>
	<div id="wrapper">
		<!-- Navigation -->
		<?php include('nav.php'); ?>
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Transaction Details</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Summary</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<dl>
								<dt>Transaction ID</dt>
								<dd><?php echo $data[0]['trans_id'];?></dd>
								<dt>Receipt No.</dt>
								<dd><?php echo $data[0]['receipt_no'];?></dd>
								<dt>Payment Date/Time</dt>
								<dd><?php echo $data[0]['payment_datetime'];?></dd>
								<dt>Payment Mode</dt>
								<dd><?php echo $data[0]['payment_mode'];?></dd>
								<dt>Payment Transaction ID.</dt>
								<dd><?php echo $data[0]['payment_trans_id'];?></dd>
								<dt>Status</dt>
								<dd><?php echo $data[0]['status'];?></dd>
								<dt>Amount (RM)</dt>
								<dd><?php echo $data[0]['amount'];?></dd>
								<dt>Billling Address</dt>
								<dd><?php echo $data[0]['billing_address'];?></dd>
								<dt>Referer</dt>
								<dd><?php echo $data[0]['referer'];?></dd>
								<dt>User Agent</dt>
								<dd><?php echo $data[0]['agent'];?></dd>
								<dt>IP Address</dt>
								<dd><?php echo $data[0]['ip_address'];?></dd>
							</dl>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">User Agent Details</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<?php
								// Get cURL resource
								$curl = curl_init();
								
								// Set some options - we are passing in a useragent too here
								curl_setopt_array($curl, array(
									CURLOPT_RETURNTRANSFER => 1,
									CURLOPT_URL => 'http://www.useragentstring.com/?uas='.urlencode($data[0]['agent']).'&getJSON=all',
									CURLOPT_USERAGENT => 'User Agent Details'
								));
								
								// Store the data temporarily in JSON
								$json = curl_exec($curl);
								curl_close($curl);
								
								// Decode JSON response:
								$user_agent = json_decode($json, true);
								//var_dump($user_agent);
								
								function build_list($array) {
									$list = "<table><tbody>";
									foreach($array as $key => $value) {
										$list .= "<tr><td>$key</td><td>:</td><td>$value</td></tr>";
									}
									$list .= "</tbody></table>";
									return $list;
								}
								echo build_list($user_agent);
							?>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Geolocation Details</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<?php
								// Get cURL resource
								$curl = curl_init();
								
								// Set some options - we are passing in a useragent too here
								curl_setopt_array($curl, array(
									CURLOPT_RETURNTRANSFER => 1,
									CURLOPT_URL => 'http://www.telize.com/geoip/'.$data[0]['ip_address'],
									CURLOPT_USERAGENT => 'Geolocation Details'
								));
								
								// Store the data temporarily in JSON
								$json = curl_exec($curl);
								curl_close($curl);
								
								// Decode JSON response:
								$geolocation = json_decode($json, true);
								echo build_list($geolocation);
							?>
						</div>
					</div>
					<button onclick="window.history.back()" class="btn btn-alert"><i class="fa fa-arrow-left fa-fw"></i> Back</button>
					<button onclick="window.print()" class="btn btn-primary"><i class="fa fa-print fa-fw"></i> Print</button>
				</div>
			</div>
		</div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
<?php include('footer.php'); ?>