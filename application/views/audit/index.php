<?php include('header.php'); ?>
	<div id="wrapper">
		<!-- Navigation -->
		<?php include('nav.php'); ?>
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Audit Trail</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">E-Payment Audit Trail (Payment by credit card only)</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Transaction ID</th>
										<th>Receipt No.</th>
										<th>Date/Time</th>
										<th>Payment Mode</th>
										<th>Amount (RM)</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($track as $row){ ?>
									<tr>
										<td><?php echo $row->trans_id ?></td>
										<td><?php echo $row->receipt_no ?></td>
										<td><?php echo $row->payment_datetime ?></td>
										<th><?php echo $row->payment_mode ?></th>
										<th><?php echo $row->amount ?></th>
										<th><a href="<?php echo BASE_URL."audit/view/".$row->id ?>" class="btn btn-info btn-xs">View</a></th>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					<?php //var_dump($rates) ?>
					</div>
					<!-- /.panel -->
				</div>
			</div>
		</div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
<?php include('footer.php'); ?>