
        <div class="wrapper">
            <div class="container">
            	<!-- Start Header -->

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-color panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add new sub category</h3>
                            </div>
                            <div class="panel-body">
                            	<!-- Content start -->
                            	<form method="post" role="form" action="<?php echo BASE_URL ?>subcategory/create" id="add-subcategory">
                                    <div class="form-group">
                                        <label for="sector_id">Sector</label>
                                        <select id="sector_id" name="sector_id" class="form-control">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="category_id">Category</label>
                                        <select id="category_id" name="category_id" class="select2 form-control">
                                        </select>
                                    </div>
    								<div class="form-group">
    									<label for="title">Title</label>
    									<input type="text" name="title" class="form-control" required>
    								</div>
    								<button type="submit" class="btn btn-success waves-effect waves-light m-b-5">Save</button>
    								<button class="btn btn-warning waves-effect waves-light m-b-5" id="back">Cancel</button>
								</form>
								<!-- Content end -->
                            </div>
                        </div>
					</div>
            	</div> <!-- End Row -->