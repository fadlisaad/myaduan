        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <img src="<?php echo BASE_URL ?>assets/images/logo.svg" style="height: 40px;margin-top: 10px;" class="pull-left"><a href="#" class="logo"><?php echo SITE_TITLE ?></a>
                    </div>
                    <!-- End Logo container-->

                    <div class="menu-extras">

                        <ul class="nav navbar-nav navbar-right pull-right">
                            
                            <li class="dropdown user-box">
                                <a href="" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown" aria-expanded="true">
                                    <img src="<?php echo BASE_URL ?>assets/images/users/<?php echo $_SESSION['avatar'] ?>" alt="user-img" class="img-circle user-img">
                                    <div class="user-status away"><i class="zmdi zmdi-dot-circle"></i></div>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo BASE_URL ?>user/profile"><i class="md md-face-unlock"></i> Profile</a></li>
                                    <li><a href="<?php echo BASE_URL ?>auth/logout" title="Logout"><i class="md md-settings-power"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>

                </div>
            </div>

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <li><a href="<?php echo BASE_URL ?>dashboard"><i class="md md-home"></i> <span> Dashboard </span> </a></li>
                            <?php if($_SESSION['permission'] == 'user'): ?>
                            <li><a href="<?php echo BASE_URL ?>ticket"><i class="md md-book"></i> <span> Ticket </span> </a></li>
                            <?php endif; ?>
                            <?php if($_SESSION['permission'] != 'user'): ?>
                            <li><a href="<?php echo BASE_URL ?>ticket/admin"><i class="md md-book"></i> <span> Ticket </span> </a></li>
                            <?php
                            $pattern = "/^[a-z0-9._%+-]+@[a-z0-9.-]*(" . implode('|', array('myaduan.com.my')) . ")$/i";
                            if (preg_match($pattern, $_SESSION['user_email'])) {
                                echo "<li><a href=\"".BASE_URL."api/email\" target=\"_blank\"><i class=\"md md-mail\"></i> <span> Inbox </span> </a></li>";
                            } ?> 
                            <?php endif; ?>
                            <?php if($_SESSION['permission'] == 'super'): ?>
                            <li><a href="<?php echo BASE_URL ?>user"><i class="md md-account-circle"></i> <span> User </span> </a></li>
                            <li class="has-submenu">
                                <a href="#"><i class="md md-settings"></i> <span> Setting <span class="caret"></span></span> </a>
                                <ul class="submenu">
                                    <li><a href="<?php echo BASE_URL ?>sector">Sector</a></li>
                                    <li><a href="<?php echo BASE_URL ?>category">Category</a></li>
                                    <li><a href="<?php echo BASE_URL ?>subcategory">Sub Category</a></li>
                                    <li><a href="<?php echo BASE_URL ?>type">Type</a></li>
                                    <li><a href="<?php echo BASE_URL ?>user/roles">User Roles</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                        </ul>
                        <!-- End navigation menu  -->
                    </div>
                </div>
            </div>
        </header>
        <!-- End Navigation Bar-->