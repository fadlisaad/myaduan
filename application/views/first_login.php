<?php include('header.php'); ?>
		<div class="wrapper-page">
            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-heading bg-img"> 
                    <div class="bg-overlay"></div>
                    <h3 class="text-center m-t-10 text-white"><?php echo SITE_TITLE ?></h3>
                </div> 

                <div class="panel-body">
                    <form class="form-horizontal m-t-20" action="<?php echo BASE_URL ?>auth/process_first_login" method="post">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            Your account has been created by the system, please proceed with your MyAduan e-mail address. Once your e-mail is valid, you can create your new password and profile.
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>MyAduan E-mail Address</label>
                                <input class="form-control input-lg" type="text" name="email" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>New password</label>
                                <input class="form-control input-lg" type="password" name="password" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>Full name</label>
                                <input class="form-control input-lg" type="text" name="full_name" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>Age</label>
                                <select class="form-control select2" name="age" required="">
                                    <option value="">Choose your age range</option>
                                    <option value="15-20">15-20</option>
                                    <option value="21-25">21-25</option>
                                    <option value="26-30">26-30</option>
                                    <option value="31-35">31-35</option>
                                    <option value="36-40">36-40</option>
                                    <option value="41-45">41-45</option>
                                    <option value="46-50">46-50</option>
                                    <option value="51-55">51-55</option>
                                    <option value="56-60">56-60</option>
                                    <option value="&gt; 60">&gt; 60</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>Nationality</label>
                                <select class="form-control" id="country_id" name="country_id" required="">
                                    <option value="135" selected="">Malaysian</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="ic">
                            <div class="col-xs-12">
                                <label>IC number</label>
                                <input class="form-control" type="text" id="ic_passport" name="ic_passport">
                            </div>
                        </div>

                        <div class="form-group" id="passport">
                            <div class="col-xs-12">
                                <label>Passport number</label>
                                <input class="form-control" type="text" name="ic_passport">
                            </div>
                        </div>

                        <div class="form-group" id="ic">
                            <div class="col-xs-12">
                                <label>Mobile phone number</label>
                                <input class="form-control" type="text" id="phone_no" name="phone_no">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>Address Line 1</label>
                                <input class="form-control" type="text" name="address1" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>Address Line 2</label>
                                <input class="form-control" type="text" name="address2">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>Postcode</label>
                                <input class="form-control" type="text" id="postcode" name="postcode" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>State</label>
                                <select class="form-control" id="state" name="state_id" required="">
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>City</label>
                                <select class="form-control" id="city" name="city_id" required="">
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group text-center m-t-40">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" type="submit" name="first_time">Next</button>
                            </div>
                        </div>

                        <div class="form-group m-t-30">
                            <div class="col-sm-12 text-center">
                                <a href="<?php echo BASE_URL ?>auth/reset_password"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a> or <a href="<?php echo BASE_URL ?>"> Login</a>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
        </div>

    	<script>
            var resizefunc = [];
        </script>
<?php include('footer.php'); ?>