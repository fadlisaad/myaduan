
        <div class="wrapper">
            <div class="container">
            	<!-- Start Header -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-color panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add new User Role</h3>
                            </div>
                            <div class="panel-body">
                            	<!-- Content start -->
                            	<form method="post" role="form" action="<?php echo BASE_URL ?>user/create_role" id="add-user-role">
								<div class="form-group">
									<label for="user_id">User</label>
                                    <select id="user_id" name="user_id" class="form-control"></select>
								</div>
								<div class="form-group">
									<label for="sector_id">Sector</label>
                                    <select id="sector_id" name="sector_id" class="form-control">
                                        <option value="0">All</option>
                                    </select>
								</div>
								<button type="submit" class="btn btn-success waves-effect waves-light m-b-5">Save</button>
								<button class="btn btn-warning waves-effect waves-light m-b-5" id="back">Cancel</button>
								</form>
								<!-- Content end -->
                            </div>
                        </div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-color panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Help</h3>
                            </div>
                            <div class="panel-body">
                            	<h5>Information</h5>
                            	<p>There is no limit on how many sector a user can have access to. To assign a user to all sector, select All from sector drop-down.</p>
                            </div>
                        </div>
					</div>
            	</div> <!-- End Row -->