        <div class="wrapper">
            <div class="container">
                <div id="avatar_list" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                <h4 class="modal-title" id="myModalLabel">List of avatar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <?php for ($x = 1; $x <= 8; $x++) { ?>
                                    <div class="col-sm-6 col-lg-3 col-md-4">
                                        <div class="gal-detail thumb">
                                            <img src="<?php echo BASE_URL ?>assets/images/users/male_avatar_<?php echo $x ?>.png" class="thumb-img img-circle" alt="work-thumbnail">
                                            <button class="btn btn-info waves-effect btn-xs avatar" type="button" value="male_avatar_<?php echo $x ?>.png" data-dismiss="modal">Choose</button>
                                        </div>
                                    </div>
                                    <?php  } ?>
                                </div>
                                <div class="row">
                                    <?php for ($y = 1; $y <= 8; $y++) { ?>
                                    <div class="col-sm-6 col-lg-3 col-md-4">
                                        <div class="gal-detail thumb">
                                            <img src="<?php echo BASE_URL ?>assets/images/users/female_avatar_<?php echo $y ?>.png" class="thumb-img" alt="work-thumbnail">
                                            <button class="btn btn-info waves-effect btn-xs avatar" type="button" value="female_avatar_<?php echo $y ?>.png" data-dismiss="modal">Choose</button>
                                        </div>
                                    </div>
                                    <?php  } ?>
                                </div>
                            </div>
                            <div class="modal-footer">
                                submit
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="bg-picture text-center" style="background-image:url('<?php echo BASE_URL ?>assets/images/big/bg.jpg'); margin: 0;">
                            <div class="bg-picture-overlay"></div>
                            <div class="profile-info-name">
                                <img src="<?php echo BASE_URL ?>assets/images/users/<?php echo $_SESSION['avatar'] ?>" class="thumb-lg img-circle img-thumbnail" alt="profile-image">
                                <h3 class="text-white"><?php echo ucfirst($data[0]['full_name']) ?></h3>
                                <button class="btn btn-info waves-effect btn-xs" type="button" data-toggle="modal" data-target="#avatar_list">Change avatar</button>
                            </div>
                        </div>

                        <div class="panel panel-success panel-border" style="margin-top: 35px;">
                            <div class="panel-heading">
                                <h3 class="panel-title">Change Password</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="Password">New Password</label><br/>
                                    <input type="password" class="form-control" name="user_pass" id="user_pass" value="">
                                </div>
                                <input type="hidden" name="user_id" id="user_id" value="<?php echo $data[0]['id'] ?>">
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-success waves-effect waves-light w-md" type="button" id="self_update">Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <!-- Personal-Information -->
                        <div class="panel panel-primary panel-border">
                            <div class="panel-heading">
                                <h3 class="panel-title">Personal Information</h3>
                            </div>
                            <div class="panel-body">
                                <div class="about-info-p">
                                    <strong>Full Name</strong>
                                    <br>
                                    <p class="text-muted"><?php echo ucfirst($data[0]['full_name']) ?></p>
                                </div>
                                <div class="about-info-p">
                                    <strong>IC/Passport</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $data[0]['ic_passport'] ?></p>
                                </div>
                                <div class="about-info-p">
                                    <strong>Email</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $data[0]['email'] ?></p>
                                </div>
                                <div class="about-info-p m-b-0">
                                    <strong>Permission</strong>
                                    <br>
                                    <p class="text-muted"><?php echo ucfirst($data[0]['permission']) ?></p>
                                </div>
                                <div class="about-info-p m-b-0">
                                    <strong>Address</strong>
                                    <br>
                                    <p class="text-muted"><?php echo ucfirst($data[0]['address']) ?></p>
                                </div>
                                <div class="about-info-p">
                                    <strong>Age</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $data[0]['age'] ?></p>
                                </div>
                                <div class="about-info-p">
                                    <strong>Ethnicity</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $data[0]['ethnicity'] ?></p>
                                </div>
                                <div class="about-info-p">
                                    <strong>Income Range</strong>
                                    <br>
                                    <p class="text-muted"><?php echo $data[0]['income'] ?></p>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-primary waves-effect waves-light w-md" type="button" data-toggle="modal" data-target="#profile_update">Update My Profile</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="profile_update" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                <h4 class="modal-title" id="myModalLabel">Update Profile</h4>
                            </div>
                            <div class="modal-body">
                                <form id="profile_update_form">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Full Name</label>
                                                    <input class="form-control" type="text" name="full_name" required="" value="<?php echo ucfirst($data[0]['full_name']) ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Age</label>
                                                    <select class="form-control select2 age" name="age" required="">
                                                        <option value="15-20">15-20</option>
                                                        <option value="21-25">21-25</option>
                                                        <option value="26-30">26-30</option>
                                                        <option value="31-35">31-35</option>
                                                        <option value="36-40">36-40</option>
                                                        <option value="41-45">41-45</option>
                                                        <option value="46-50">46-50</option>
                                                        <option value="51-55">51-55</option>
                                                        <option value="56-60">56-60</option>
                                                        <option value="&gt; 60">&gt; 60</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Nationality</label>
                                                    <select class="form-control" id="country_id" name="country_id" required="" style="width: 100%">
                                                        <option value="<?php echo $data[0]['country_id'] ?>" selected><?php echo $data[0]['country_name'] ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group" id="passport">
                                                <div class="col-xs-12">
                                                    <label>Passport number</label>
                                                    <input class="form-control" type="text" name="ic_passport" value="<?php echo $data[0]['ic_passport'] ?>">
                                                </div>
                                            </div>

                                            <div class="form-group" id="ic">
                                                <div class="col-xs-12">
                                                    <label>IC number</label>
                                                    <input class="form-control" type="text" id="ic_passport" name="ic_passport" value="<?php echo $data[0]['ic_passport'] ?>">
                                                </div>
                                            </div>

                                            <div class="form-group" id="ic">
                                                <div class="col-xs-12">
                                                    <label>Mobile phone number</label>
                                                    <input class="form-control" type="text" id="phone_no" name="phone_no" value="<?php echo $data[0]['phone_no'] ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Address 1</label>
                                                    <input class="form-control" type="text" name="address1" required="" value="<?php echo $data[0]['address1'] ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Address 2</label>
                                                    <input class="form-control" type="text" name="address2" value="<?php echo $data[0]['address2'] ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Postcode</label>
                                                    <input class="form-control" type="text" id="postcode" name="postcode" required="" value="<?php echo $data[0]['postcode'] ?>">
                                                </div>
                                            </div>

                                            <div class="form-group" id="state-my">
                                                <div class="col-xs-12">
                                                    <label>State</label>
                                                    <select class="form-control" id="state" name="state_id" style="width: 100%">
                                                        <option value="<?php echo $data[0]['state_id'] ?>" selected><?php echo $data[0]['state_name'] ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group" id="city-my">
                                                <div class="col-xs-12">
                                                    <label>City</label>
                                                    <select class="form-control" id="city" name="city_id" style="width: 100%">
                                                        <option value="<?php echo $data[0]['city_id'] ?>" selected><?php echo $data[0]['city_name'] ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group" id="state-other">
                                                <div class="col-xs-12">
                                                    <label>State</label>
                                                    <input type="text" class="form-control" name="state_id" style="width: 100%" value="">
                                                </div>
                                            </div>

                                            <div class="form-group" id="city-other">
                                                <div class="col-xs-12">
                                                    <label>City</label>
                                                    <input type="text" class="form-control" name="city_id" style="width: 100%" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group" id="ethnicity-my">
                                                <div class="col-xs-12">
                                                    <label>Ethnicity</label>
                                                    <p>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="ethnicity1" value="Melayu" name="ethnicity" class="ethnicity">
                                                        <label for="ethnicity1"> Malay </label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="ethnicity2" value="Cina" name="ethnicity" class="ethnicity">
                                                        <label for="ethnicity2"> Chinese </label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="ethnicity3" value="India" name="ethnicity" class="ethnicity">
                                                        <label for="ethnicity3"> Indian </label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="ethnicity4" value="Others" name="ethnicity" class="ethnicity">
                                                        <label for="ethnicity4"> Others </label>
                                                    </div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="form-group" id="income-my">
                                                <div class="col-xs-12">
                                                    <label>Income Range</label>
                                                    <p>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income1" name="income" value="Below RM 2000" class="income">
                                                        <label for="income1">Below RM 2000</label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income2" name="income" value="RM 2001 - RM 3000" class="income">
                                                        <label for="income2">RM 2001 - RM 3000</label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income3" name="income" value="RM 3001 - RM 4000" class="income">
                                                        <label for="income3">RM 3001 - RM 4000</label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income4" name="income" value="RM 4001 - RM 5000" class="income">
                                                        <label for="income4">RM 4001 - RM 5000</label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income5" name="income" value="RM 5000 - RM 6000" class="income">
                                                        <label for="income5">RM 5000 - RM 6000</label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income6" name="income" value="Above RM 6001" class="income">
                                                        <label for="income6">Above RM 6001</label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income7" name="income" value="No income" class="income">
                                                        <label for="income7">No income</label>
                                                    </div>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>