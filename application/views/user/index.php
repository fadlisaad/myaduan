
	<div class="wrapper">
		<div class="container">
			<!-- Start Header -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo BASE_URL ?>user/add">Add new user</a></li>
                        </ul>
                    </div>
                    <h4 class="page-title"><i class="fa fa-gear"></i> User </h4>
                </div>
            </div>

			<div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">List of User</h3>
                        </div>
                        <div class="panel-body">
                            <table id="datatable" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Full Name</th>
                                        <th>E-mail</th>
                                        <th>Permission</th>
                                        <th>Action</th>
									</tr>
								</thead>
							</table>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->