
        <div class="wrapper">
            <div class="container">
            	<!-- Start Header -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-color panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add new User</h3>
                            </div>
                            <div class="panel-body">
                            	<!-- Content start -->
                            	<form method="post" role="form" action="<?php echo BASE_URL ?>user/create" id="edit-user">
								<div class="form-group">
                                    <label for="full_name">Full Name</label>
                                    <input type="text" name="full_name" class="form-control" required>
                                </div>
								<div class="form-group">
									<label for="email">E-mail Address</label>
									<input type="text" name="email" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="password">Password</label>
									<input type="password" name="password" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="permission">Role/Permission</label>
                                    <div class="radio radio-primary">
                                        <input name="permission" type="radio" value="admin">
                                        <label for="radio1">Admin</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input name="permission" type="radio" value="officer">
                                        <label for="radio1">Officer</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input name="permission" type="radio" value="manager">
                                        <label for="radio1">Manager</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input name="permission" type="radio" value="staff">
                                        <label for="radio2">Staff</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input name="permission" type="radio" value="user">
                                        <label for="radio1">User</label>
                                    </div>
								</div>
								<button type="submit" class="btn btn-success waves-effect waves-light m-b-5">Save</button>
								<button class="btn btn-warning waves-effect waves-light m-b-5" id="back">Cancel</button>
								</form>
								<!-- Content end -->
                            </div>
                        </div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-color panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Help</h3>
                            </div>
                            <div class="panel-body">
                            	<h5>Contact ID</h5>
                            	<p>Contact ID will match this user login with it's complete profile. To edit the profile instead, click on the <code>Edit Profile</code> button</p><p>If no profile selected, the details will remain blank.</p>
                            	<h5>E-mail Address</h5>
                            	<p>E-mail address is unique, as it is used for the login</p>
                            	<h5>Password</h5>
                            	<p>No specific password policy has been enforce.</p><p>To keep the same password, do not change anything on the password field.</p>
                            </div>
                        </div>
					</div>
            	</div> <!-- End Row -->