
        <div class="wrapper">
            <div class="container">
            	<!-- Start Header -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-color panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">User Details</h3>
                            </div>
                            <div class="panel-body">
                            	<!-- Content start -->
                            	<form method="post" role="form" action="<?php echo BASE_URL ?>user/update/<?php echo $data[0]['id'] ?>" novalidate="novalidate" id="edit-user">
                                <div class="form-group">
                                    <label for="full_name">Full Name</label>
                                    <input type="text" name="full_name" class="form-control" value="<?php echo $data[0]['full_name'] ?>">
                                </div>
								<div class="form-group">
									<label for="content">E-mail Address</label>
									<input type="text" name="email" class="form-control" value="<?php echo $data[0]['email'] ?>" readonly>
								</div>
								<div class="form-group">
									<label for="content">Password</label>
									<input type="password" name="password" class="form-control" placeholder="Edit only if you want to update the password">
								</div>
								<div class="form-group">
									<label for="permission">Role/Permission</label>
                                    <div class="radio radio-primary">
                                        <input name="permission" type="radio" value="super" <?php if($data[0]['permission'] == 'super') echo "checked=\"checked\""; ?>>
                                        <label for="radio1">Super Admin</label>
                                    </div>
									<div class="radio radio-primary">
                        				<input name="permission" type="radio" value="admin" <?php if($data[0]['permission'] == 'admin') echo "checked=\"checked\""; ?>>
                        				<label for="radio1">Admin</label>
                        			</div>
                                    <div class="radio radio-primary">
                                        <input name="permission" type="radio" value="officer" <?php if($data[0]['permission'] == 'officer') echo "checked=\"checked\""; ?>>
                                        <label for="radio1">Officer</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input name="permission" type="radio" value="manager" <?php if($data[0]['permission'] == 'manager') echo "checked=\"checked\""; ?>>
                                        <label for="radio1">Manager</label>
                                    </div>
                        			<div class="radio radio-primary">
                        				<input name="permission" type="radio" value="staff" <?php if($data[0]['permission'] == 'staff') echo "checked=\"checked\""; ?>>
                        				<label for="radio2">Staff</label>
                        			</div>
                                    <div class="radio radio-primary">
                                        <input name="permission" type="radio" value="user" <?php if($data[0]['permission'] == 'user') echo "checked=\"checked\""; ?>>
                                        <label for="radio1">User</label>
                                    </div>
								</div>
								<button type="submit" class="btn btn-success waves-effect waves-light m-b-5">Save</button>
								<button class="btn btn-warning waves-effect waves-light m-b-5" id="back">Cancel</button>
                				<a href="#" class="btn btn-danger waves-effect waves-light m-b-5" id="delete">Delete</a>
								</form>
								<!-- Content end -->
                            </div>
                        </div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-color panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Help</h3>
                            </div>
                            <div class="panel-body">
                            <?php //print_r($data) ?>
                            	<h5>Contact ID</h5>
                            	<p>Contact ID will match this user login with it's complete profile.</p>
                            	<h5>E-mail Address</h5>
                            	<p>E-mail address is unique, as it is used for the login. Thus, it can't be edited.</p>
                            	<h5>Password</h5>
                            	<p>No specific password policy has been enforce.</p><p>To keep the same password, do not change anything on the password field.</p>
                            </div>
                        </div>
					</div>
            	</div> <!-- End Row -->