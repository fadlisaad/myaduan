
	<div class="wrapper">
		<div class="container">
			<!-- Start Header -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal">Select Sector</button>
                        <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Action <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo BASE_URL ?>user/add_role">Add new user roles</a></li>
                        </ul>
                    </div>
                    <h4 class="page-title"><i class="fa fa-gear"></i> User Roles</h4>
                </div>
            </div>

			<div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">List of User Roles</h3>
                        </div>
                        <div class="panel-body">
                            <table id="datatable" class="table table-striped table-bordered">
								<thead>
									<tr>
                                        <th>Sector</th>
										<th>Full Name</th>
                                        <th>Permission</th>
                                        <th>Action</th>
									</tr>
								</thead>
							</table>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->
            <div id="con-close-modal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                            <h4 class="modal-title">Choose sector</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select id="sector">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal -->