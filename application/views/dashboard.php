    <?php include('header.php'); ?>
    <?php include('navigation.php'); ?>
        <div class="wrapper">
            <div class="container">

                <!-- Start Header -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Welcome, <?php echo $_SESSION['full_name'] ?> </h4>
                    </div>
                </div>
                <div class="row">
                    <?php if($_SESSION['permission'] == 'user'): ?>
                    <a href="<?php echo BASE_URL ?>complaint/add">
                    <div class="col-sm-12 col-lg-3 col-lg-offset-3">
                        <div class="mini-stat clearfix bx-shadow bg-info">
                            <span class="mini-stat-icon"><i class="ion-plus-circled"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter">Add Ticket</span>
                                Create new trouble ticket
                            </div>
                        </div>
                    </div>
                    </a>
                    <a href="<?php echo BASE_URL ?>ticket">
                    <div class="col-sm-12 col-lg-3">
                        <div class="mini-stat clearfix bg-purple bx-shadow">
                            <span class="mini-stat-icon"><i class="ion-navicon-round"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter">View Ticket</span>
                                View all your submitted ticket
                            </div>
                        </div>
                    </div>
                    </a>
                    <?php else: ?>
                    
                    <?php if(in_array($_SESSION['permission'], array('admin'), true)): ?>

                    <div class="col-sm-12 col-lg-2">
                        <div class="mini-stat clearfix bg-primary bx-shadow">
                            <span class="mini-stat-icon"><i class="ion-ios7-browsers"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter"><?php echo @$total[0]['total'] ?></span>
                                All tickets
                            </div>
                            <div class="tiles-progress">
                                <div class="m-t-20">
                                    <h5 class="text-uppercase text-white m-0">Unassigned Ticket <span class="pull-right"><?php echo @$count_total_submitted[0]['total'] ?></span></h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-2">
                        <div class="mini-stat clearfix bg-purple bx-shadow">
                            <span class="mini-stat-icon"><i class="ion-ios7-people"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter"><?php echo @$total_user[0]['total'] ?></span>
                                All User
                            </div>
                            <div class="tiles-progress">
                                <div class="m-t-20">
                                    <h5 class="text-uppercase text-white m-0">General User <span class="pull-right"><?php echo @$user[0]['total'] ?></span></h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-2">
                        <div class="mini-stat clearfix bx-shadow bg-info">
                            <span class="mini-stat-icon"><i class="ion-ios7-browsers"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter"><?php echo @$count_total_submitted[0]['total'] ?></span>
                                &nbsp;
                            </div>
                            <div class="tiles-progress">
                                <div class="m-t-20">
                                    <h5 class="text-uppercase text-white m-0">Submitted</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-2">
                        <div class="mini-stat clearfix bx-shadow bg-primary">
                            <span class="mini-stat-icon"><i class="ion-ios7-browsers"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter"><?php echo @$count_total_processing[0]['total'] ?></span>
                                &nbsp;
                            </div>
                            <div class="tiles-progress">
                                <div class="m-t-20">
                                    <h5 class="text-uppercase text-white m-0">Processing</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-2">
                        <div class="mini-stat clearfix bg-success bx-shadow">
                            <span class="mini-stat-icon"><i class="ion-ios7-browsers"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter"><?php echo @$count_total_open[0]['total'] ?></span>
                                &nbsp;
                            </div>
                            <div class="tiles-progress">
                                <div class="m-t-20">
                                    <h5 class="text-uppercase text-white m-0">Open</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-2">
                        <div class="mini-stat clearfix bg-danger bx-shadow">
                            <span class="mini-stat-icon"><i class="ion-ios7-browsers"></i></span>
                            <div class="mini-stat-info text-right">
                                <span class="counter"><?php echo @$count_total_closed[0]['total'] ?></span>
                                &nbsp;
                            </div>
                            <div class="tiles-progress">
                                <div class="m-t-20">
                                    <h5 class="text-uppercase text-white m-0">Closed</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php endif; ?>

                    <?php endif; ?>
                </div>
                <?php if($_SESSION['permission'] != 'user'): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info panel-border">
                            <div class="panel-heading">
                                <h3 class="panel-title">Tickets Breakdown Todate</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="text-center"><span class="label label-info">Submitted</span></th>
                                                    <th class="text-center"><span class="label label-success">Open</span></th>
                                                    <th class="text-center"><span class="label label-primary">Processing</span></th>
                                                    <th class="text-center"><span class="label label-danger">Closed</span></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if($count_staff_open):
												foreach ($count_staff_open as $value) { ?>
                                                <tr>
                                                    <td><?php echo $value['assigned_name']; ?></td>
                                                    <td class="text-center open"><?php echo @$value['Submitted'];?></td>
                                                    <td class="text-center open"><?php echo @$value['Open'];?></td>
													<td class="text-center open"><?php echo @$value['Processing'];?></td>
													<td class="text-center open"><?php echo @$value['Closed'];?></td>
                                                </tr>
                                                <?php } else: ?>
                                                <tr>
                                                    <td><?php echo $_SESSION['full_name']; ?></td>
                                                    <td class="text-center open">0</td>
                                                    <td class="text-center open">0</td>
                                                    <td class="text-center open">0</td>
                                                    <td class="text-center open">0</td>
                                                    <?php if($_SESSION['permission'] == 'admin'): ?><td class="text-center open">0</td><?php endif; ?>
                                                </tr>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                
            <?php include('footer.php'); ?>