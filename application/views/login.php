<?php include('header.php'); ?>
		<div class="wrapper-page">
            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-heading bg-img"> 
                    <div class="bg-overlay"></div>
                    <h3 class="text-center m-t-10 text-white"><?php echo SITE_TITLE ?></h3>
                </div> 

                <div class="panel-body">
                    <form class="form-horizontal m-t-20" action="<?php echo BASE_URL ?>auth/process_login" method="post">
                        
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control input-lg" type="text" name="email" required="" placeholder="E-mail">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control input-lg" id="password" type="password" name="password" required="" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup">Remember me</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group text-center m-t-40">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" type="submit" name="submit">Log In</button>
                            </div>
                        </div>

                        <div class="form-group m-t-30">
                            <div class="col-sm-12 text-center">
                                <!--<a href="<?php echo BASE_URL ?>auth/reset_password"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a> or <a href="<?php echo BASE_URL ?>auth/first_login"> First time login?</a>-->
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td>Role</td>
                                            <td>Username</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Super Admin</td>
                                            <td>super@demo.site</td>
                                        </tr>
                                        <tr>
                                            <td>Admin 1</td>
                                            <td>admin1@demo.site</td>
                                        </tr>
                                        <tr>
                                            <td>Officer 1</td>
                                            <td>officer1@demo.site</td>
                                        </tr>
                                        <tr>
                                            <td>Officer 2</td>
                                            <td>officer2@demo.site</td>
                                        </tr>
                                        <tr>
                                            <td>Officer 3</td>
                                            <td>officer3@demo.site</td>
                                        </tr>
                                        <tr>
                                            <td>Normal User 1</td>
                                            <td>user1@demo.site</td>
                                        </tr>
                                        <tr>
                                            <td>Normal User 2</td>
                                            <td>user2@demo.site</td>
                                        </tr>
                                        <tr>
                                            <td>Normal User 3</td>
                                            <td>user3@demo.site</td>
                                        </tr>
                                        <tr>
                                            <td>Normal User 4</td>
                                            <td>user4@demo.site</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p>Default password: <strong>0000</strong></p>
                            </div>
                        </div>
                    </form>
                </div>                                 
                
            </div>
        </div>

    	<script>
            var resizefunc = [];
        </script>
<?php include('footer.php'); ?>