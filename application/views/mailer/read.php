
	<div class="wrapper">
		<div class="container">
			<!-- Start Header -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title"><i class="fa fa-mail"></i> Read e-mail </h4>
                </div>
            </div>

			<div class="row">
                <div class="col-md-3">
                    <div class="list-group mail-list">
                      <a href="<?php echo BASE_URL ?>mailer/inbox" class="list-group-item no-border active"><i class="fa fa-download m-r-5"></i>Inbox</a>
                      <a href="<?php echo BASE_URL ?>mailer/sent" class="list-group-item no-border"><i class="fa fa-paper-plane-o m-r-5"></i>Sent Mail</a>
                      <a href="<?php echo BASE_URL ?>mailer/trash" class="list-group-item no-border"><i class="fa fa-trash-o m-r-5"></i>Trash</a>
                    </div>
                    <form role="form" id="send-email">
                        <input type="hidden" name="id" value="<?php echo $data->id ?>">
                        <input type="hidden" name="folder" value="Deleted Items">
                        <button type="button" class="btn btn-block btn-danger" id="move-message">Move to trash</button>
                    </form>
                    <form role="form" id="junk-email">
                        <input type="hidden" name="id" value="<?php echo $data->id ?>">
                        <input type="hidden" name="folder" value="Junk E-Mail">
                        <button type="button" class="btn btn-block btn-warning" id="junk-message">Mark as Junk</button>
                    </form>
                </div>
                <div class="col-md-9">
                    <div class="panel panel-default m-t-20">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo $data->subject ?></h3>
                        </div>
                        <div class="panel-body">
                            <div class="media m-b-30">
                                <a href="#" class="pull-left">
                                    <img alt="" src="<?php echo BASE_URL ?>assets/images/users/default.jpg" class="media-object thumb-sm">
                                </a>
                                <div class="media-body">
                                    <span class="media-meta pull-right"><?php echo $data->date ?></span>
                                    <h4 class="text-primary m-0"><?php echo $data->fromAddress ?></h4>
                                    <small class="text-muted">From: <?php echo $data->fromName ?></small>
                                </div>
                            </div> <!-- media -->
                        <?php if($data->textHtml): ?>
                            <p><?php echo $data->textHtml ?></p>
                        <?php else: ?>
                            <p><?php echo $data->textPlain ?></p>
                        <?php endif; ?>
                        <?php if($attachments): ?>
                            <hr>
                            <h4> <i class="fa fa-paperclip m-r-10 m-b-10"></i> Attachments </h4>
                            <div class="row">
                            <?php foreach($attachments as $attachment): ?>
                                <div class="col-sm-4 col-md-3">
                                    <a href="<?php echo BASE_URL."1files/".basename($attachment->filePath) ?>"><?php echo $attachment->name; ?></a>
                                </div>
                            <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        </div> <!-- panel-body -->
                    </div>

                    <!-- reply from DB -->
                    <?php if($reply):
                    foreach($reply as $value): ?>
                    <div class="panel panel-default m-t-20">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo $value['subject'] ?></h3>
                        </div>
                        <div class="panel-body">
                            <div class="media m-b-30">
                                <a href="#" class="pull-left">
                                    <img alt="" src="<?php echo BASE_URL ?>assets/images/users/default.jpg" class="media-object thumb-sm">
                                </a>
                                <div class="media-body">
                                    <span class="media-meta pull-right"><?php echo $value['last_update'] ?></span>
                                    <h4 class="text-primary m-0"><?php echo $value['email'] ?></h4>
                                </div>
                                <p><?php echo $value['content'] ?></p>
                            </div> <!-- media -->
                        </div>
                    </div>
                    <?php endforeach;
                    endif; ?>
                    <!-- end reply -->

                    <!-- Reply -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                        <form role="form" id="send-email" action="<?php echo BASE_URL ?>mailer/reply" method="post" enctype="multipart/form-data">
                            <div class="media">
                                <a href="#" class="media-left">
                                    <img alt="" src="<?php echo BASE_URL ?>assets/images/users/default.jpg" class="media-object thumb-sm">
                                </a>
                                <div class="media-body">
                                    <textarea class="wysihtml5 form-control" rows="9" placeholder="Reply here..." name="content"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="attachment">Attachment</label>
                                <input type="file" name="attachment" id="fileToUpload" class="form-control">
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary waves-effect waves-light m-t-30 w-md" id="send">Send</button>
                            </div>
                            <input type="hidden" name="to" value="<?php echo $data->fromAddress ?>">
                            <input type="hidden" name="from" value="<?php echo $_SESSION['full_name'] ?>">
                            <input type="hidden" name="subject" value="RE:<?php echo $data->subject ?>">
                            <input type="hidden" name="id" value="<?php echo $data->id ?>">
                        </div>
                        </form>
                    </div>
                    <!-- Reply -->
                </div>
            </div> <!-- End Row -->