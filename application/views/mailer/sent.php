
	<div class="wrapper">
		<div class="container">
			<!-- Start Header -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title"><i class="fa fa-mail"></i> Sent </h4>
                </div>
            </div>

			<div class="row">
                <div class="col-md-3">
                    <div class="list-group mail-list">
                      <a href="<?php echo BASE_URL ?>mailer/inbox" class="list-group-item no-border"><i class="fa fa-download m-r-5"></i>Inbox</a>
                      <a href="<?php echo BASE_URL ?>mailer/sent" class="list-group-item no-border active"><i class="fa fa-paper-plane-o m-r-5"></i>Sent Mail</a>
                      <a href="<?php echo BASE_URL ?>mailer/trash" class="list-group-item no-border"><i class="fa fa-trash-o m-r-5"></i>Trash</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sent</h3>
                        </div>
                        <div class="panel-body">
                            <table id="datatable" class="table table-striped table-bordered">
								<thead>
									<tr>
                                        <th>Subject</th>
										<th>From</th>
                                        <th>Received</th>
									</tr>
								</thead>
                                <tbody>
                                    <?php
                                        foreach ($email as $value) {
                                            echo "<tr>";
                                            echo "<td><a href=\"".BASE_URL."mailer/read/".$value->header->uid."\">".$value->header->subject."</a></td>";
                                            echo "<td>".$value->header->from."</td>";
                                            echo "<td>".$value->header->date."</td>";
                                            echo "</tr>";
                                        }
                                    ?>
                                </tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->