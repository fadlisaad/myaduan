        <div class="wrapper-site">
            <div class="container">

                <!-- Start Header -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Registration</h4>
                    </div>
                </div>

                <!-- Intro notice -->
                <div id="notice" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                <h4 class="modal-title" id="myModalLabel">Notice and Information</h4>
                            </div>
                            <div class="modal-body">
                                <p>You should state your complaint briefly and clearly by providing all the relevant information.</p>
                                <p>You should try to resolve the problem by talking directly with the trader before lodging a complaint. Many complaints can be settled this way. We recommend that you should do this first if you have not done so. </p>
                                <p>Your complaint must be supported with relevant information depending on cases, e.g. invoices, bills, any form of conversation, receipts, vehicle registration number, account number, repair or service documents and etc. </p>
                                <p>We endeavour to resolve consumers’ complaints, but do not guarantee that every complaint can be resolved. There is also no time-frame for resolving complaints as their nature and complexity varies. </p>
                                <p>We solely act as a Mediator to resolve dispute between parties and do not have the authority to take action or compel any party. In the event the dispute could not be resolved from our end, the complaint should be directed to the Authority or you may seek help from a legal advisor who specialises in your problem such as Solicitor.</p>
                                <p>After receiving a letter from us, the party complained against may choose to deal directly with the complainant and resolve the matter. If this happens the consumer must give his/her co-operation and inform us so that we can close our file.
                                <p>By filling in the complaint form you are confirming that: </p>
                                <ol>
                                    <li>You have read and understood the way the Consumer Complaints Section at NCCC operates and the extent to which the NCCC may help you with your complaint.</li>
                                    <li>We have your authorisation to contact the service provider or operator you are complaining about and to request copies of any documentation relating to your complaint.</li>
                                    <li>We will need to handle personal details about you, which could include sensitive information, in order to deal with your complaint effectively.</li>
                                    <li>We may need to exchange information about your complaint with other organizations.</li>
                                    <li>We handle complaints by means of the exchange of correspondence and/or meetings. We may use the facts in your complaint as an example of where and how things can go wrong, but we will respect your privacy and keep your personal information strictly confidential, except as authorized above.</li>
                                    <li>To the best of your knowledge, the information furnished in the complaint form is true, accurate, correct and complete.</li>
                                </ol>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">I understand</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!-- Start Header -->
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal m-t-20" action="<?php echo BASE_URL ?>auth/process_register" method="post" name="register" id="register">
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="panel panel-color panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Login Details</h4>
                                        </div>

                                        <div class="panel-body">

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>E-mail Address</label>
                                                    <input class="form-control" type="email" name="email" required placeholder="youremail@somewhere.com" data-remote="<?php echo BASE_URL ?>auth/check_user">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Password</label>
                                                    <input class="form-control" type="password" name="password" minlength="4" required placeholder="a unique password">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-color panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Personal Details</h4>
                                        </div>

                                        <div class="panel-body">
                                        
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Full Name</label>
                                                    <input class="form-control" type="text" name="full_name" required placeholder="Your full name">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Age</label>
                                                    <select class="form-control select2" name="age" required>
                                                        <option value="">Choose your age range</option>
                                                        <option value="15-20">15-20</option>
                                                        <option value="21-25">21-25</option>
                                                        <option value="26-30">26-30</option>
                                                        <option value="31-35">31-35</option>
                                                        <option value="36-40">36-40</option>
                                                        <option value="41-45">41-45</option>
                                                        <option value="46-50">46-50</option>
                                                        <option value="51-55">51-55</option>
                                                        <option value="56-60">56-60</option>
                                                        <option value="&gt; 60">&gt; 60</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Nationality</label>
                                                    <select class="form-control" id="country_id" name="country_id" required="">
                                                        <option value="135" selected="">Malaysian</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group" id="passport">
                                                <div class="col-xs-12">
                                                    <label>Passport number</label>
                                                    <input class="form-control" type="text" name="ic_passport" placeholder="">
                                                </div>
                                            </div>

                                            <div class="form-group" id="ic">
                                                <div class="col-xs-12">
                                                    <label>IC number</label>
                                                    <input class="form-control" type="text" id="ic_passport" name="ic_passport" placeholder="" required="">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group" id="ic">
                                                <div class="col-xs-12">
                                                    <label>Mobile phone number</label>
                                                    <input class="form-control" type="text" id="phone_no" name="phone_no" placeholder="" required="">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">

                                    <div class="panel panel-color panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Contact Details</h4>
                                        </div>

                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Address 1</label>
                                                    <input class="form-control" type="text" name="address1" required="" placeholder="">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Address 2</label>
                                                    <input class="form-control" type="text" name="address2" placeholder="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label>Postcode</label>
                                                    <input class="form-control" type="text" id="postcode" name="postcode" required="" placeholder="">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group" id="state-other">
                                                <div class="col-xs-12">
                                                    <label>State</label>
                                                    <input type="text" class="form-control" name="state_id">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group" id="city-other">
                                                <div class="col-xs-12">
                                                    <label>City</label>
                                                    <input type="text" class="form-control" name="city_id">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group" id="state-my">
                                                <div class="col-xs-12">
                                                    <label>State</label>
                                                    <select class="form-control" id="state" name="state_id" required="">
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                            <div class="form-group" id="city-my">
                                                <div class="col-xs-12">
                                                    <label>City</label>
                                                    <select class="form-control" id="city" name="city_id" required="">
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="panel panel-color panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Extra Details</h4>
                                        </div>

                                        <div class="panel-body">

                                            <div class="form-group" id="ethnicity-my">
                                                <div class="col-xs-12">
                                                    <label>Ethnicity</label>
                                                    <p>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="ethnicity1" value="Melayu" name="ethnicity" required="">
                                                        <label for="ethnicity1"> Malay </label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="ethnicity2" value="Cina" name="ethnicity" required="">
                                                        <label for="ethnicity2"> Chinese </label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="ethnicity3" value="India" name="ethnicity" required="">
                                                        <label for="ethnicity3"> Indian </label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="ethnicity4" value="Others" name="ethnicity" required="">
                                                        <label for="ethnicity4"> Others </label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="form-group" id="income-my">
                                                <div class="col-xs-12">
                                                    <label>Income Range</label>
                                                    <p>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income1" name="income" value="Below RM 2000" required="">
                                                        <label for="income1">Below RM 2000</label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="radio radio-info radio-inline" style="padding-left: 44px;">
                                                        <input type="radio" id="income2" name="income" value="RM 2001 - RM 3000" required="">
                                                        <label for="income2">RM 2001 - RM 3000</label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    </p>
                                                    <p>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income3" name="income" value="RM 3001 - RM 4000" required="">
                                                        <label for="income3">RM 3001 - RM 4000</label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income4" name="income" value="RM 4001 - RM 5000" required="">
                                                        <label for="income4">RM 4001 - RM 5000</label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    </p>
                                                    <p>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income5" name="income" value="RM 5000 - RM 6000" required="">
                                                        <label for="income5">RM 5000 - RM 6000</label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income6" name="income" value="Above RM 6001" required="">
                                                        <label for="income6">Above RM 6001</label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    </p>
                                                    <p>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="income7" name="income" value="No income" required="">
                                                        <label for="income7">No income</label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                    </p>
                                                </div>
                                            </div>

                                            <!-- Agree to term -->
                                            <div id="agreetoterm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                                                            <h4 class="modal-title" id="myModalLabel">Agree to term and condition</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>By checking this box I acknowledge that the information given above is true to the best of my knowledge and belief.</p>
                                                            <p>I understand that any information I submit to the National Consumer Complaints Centre is considered public information and may be released in a public records request.</p>
                                                            <p>I understand a copy of this form and all documents relating to my complaint will be forwarded to the company that is the subject of my complaint.</p>
                                                            <p>I further hereby authorize the National Consumer Complaints Centre (NCCC) to embark on any action they see fit in connection with this complaint and agree not to hold them responsible for any loss or legal action that may arise therefrom.</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">I agree</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- /.modal -->

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox-signup" type="checkbox" data-toggle="modal" data-target="#agreetoterm" required="">
                                                        <label for="checkbox-signup">Agree to term and condition</label>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="alert alert-info">
                                                PLEASE NOTE: Any information you submit with your complaint is considered public and may be released as part of a public records request. Remove any EPF/SOCSO numbers, credit card numbers, debit card numbers and other bank account numbers from any documents you submit with your complaint.
                                            </div>
                                            
                                            <div class="form-group text-center m-t-40">
                                                <div class="col-xs-12">
                                                    <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" type="submit" name="register">Register</button>
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                        <div class="form-group m-t-30">
                                            <div class="col-sm-12 text-center">
                                                <a href="<?php echo BASE_URL ?>auth/reset_password"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a> or <a href="<?php echo BASE_URL ?>"> Login </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>