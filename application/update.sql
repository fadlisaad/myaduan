CREATE TABLE `email_reply` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `email_id` int NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `last_update` int NOT NULL
);


ALTER TABLE `attachment2`
DROP `file_type`,
RENAME TO `email_attachment`;


ALTER TABLE `email_attachment`
DROP FOREIGN KEY `email_attachment_ibfk_1`