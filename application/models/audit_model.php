<?php

class Audit_model extends Model {
	
	public function listAll()
	{
		$result = $this->selectAll('audits');
		return $result;
	}
	
	public function listSingle($id)
	{
		$result = $this->selectSingleById('audits','id',$id);
		return $result;
	}
	
	public function add($data)
	{
		$query = $this->insert('audits', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
}