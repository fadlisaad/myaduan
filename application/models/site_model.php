<?php

class Site_model extends Model {
	
	public function listAll()
	{
		$result = $this->selectAll('pages');
		return $result;
	}
	
	public function listSingle($slug)
	{
		$result = $this->selectSingleById('pages','slug',$slug);
		return $result;
	}
	
	public function addRecord($data)
	{
		$query = $this->insert('pages', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function editRecord($data,$id)
	{
		$query = $this->update('pages', $id, $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function deleteRecord($id)
	{
		$query = $this->delete('pages', $id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
}