<?php
/*
*  Title: Ticket Model
*  Version: 1.0 from 28 July 2016
*  Author: Fadli Saad
*  Website: https://fadli.my
*/
class Ticket_model extends Model {
	
	/**
	* List single ticket by ticket id
	*/
	public function listSingle($id)
	{
		$result = $this->selectSingleById('view_tickets','id',$id);
		return $result;
	}

	/**
	* List single ticket by complaint id
	*/
	public function listSingleByComplaintId($id)
	{
		$result = $this->selectSingleById('view_tickets','complaint_id',$id);
		return $result;
	}

	/**
	* Add ticket report by ticket id
	*/
	public function insertTicket($data)
	{
		$query = $this->insert('tickets', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* Add ticket message by ticket id and user id
	*/
	public function insertTicketMessage($data)
	{
		$query = $this->insert('ticket_reply', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* Add email by ticket id 
	*/
	public function assignEmail($data)
	{
		$query = $this->insert('ticket_email', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* Update ticket
	*/
	public function updateTicket($data)
	{
		$query = $this->update('tickets', $data['id'], array(
			'sector_id' => $data['sector_id'],
			'category_id' => $data['category_id'],
			'subcategory_id' => $data['subcategory_id'],
			'sector_id' => $data['sector_id'],
			'type_id' => $data['type_id'],
			'priority' => $data['priority'],
			'assigned_by' => $data['assigned_by'],
			'assigned_date' => $data['assigned_date'],
			'assigned_to' => $data['assigned_to']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* Close ticket
	*/
	public function closeTicket($data)
	{
		$query = $this->update('tickets', $data['id'], array(
			'sector_id' => $data['sector_id'],
			'category_id' => $data['category_id'],
			'sector_id' => $data['sector_id'],
			'type_id' => $data['type_id'],
			'priority' => $data['priority']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* Update ticket reply as read
	*/
	public function markAsRead($data)
	{
		$query = $this->update('ticket_reply', $data['id'], array(
			'read' => 'yes'
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* Update ticket summary
	*/
	public function updateSummary($data)
	{
		$query = $this->update('ticket_summary', $data['id'], array(
			'status' => $data['status']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* Update ticket closing
	*/
	public function updateClose($data)
	{
		$query = $this->update('tickets', $data['id'], array(
			'close_date' => $data['close_date'],
			'close_by' => $data['close_by']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* Update ticket timer
	*/
	public function updateTicketTimer($data)
	{
		$query = $this->update('tickets', $data['id'], array(
			'time_worked' => $data['time_worked']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* List single ticket report by ticket id
	*/
	public function getReport($id)
	{
		$result = $this->selectSingleById('ticket_report','ticket_id',$id);
		//die(var_dump($result));
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

	/**
	* List all attachment by complaint id
	*/
	public function listAttachment($ticket_id)
	{
		$result = $this->selectById('attachment','file_id', $ticket_id);
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

	/**
	* List single attachment by id
	*/
	public function listSingleAttachment($id)
	{
		$result = $this->selectSingleById('attachment','id', $id);
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

	/**
	* Add attachment by ticket id
	*/
	public function insertAttachment($data, $id)
	{
		$query = $this->insert('attachment', array(
			'file_id' => $id,
			'file_type' => 'complaint',
			'filename' => $data['filename'],
			'size_in_bytes' => $data['size_in_bytes'],
			'size_in_mb' => $data['size_in_mb'],
			'mime' => $data['mime'],
			'original_filename' => $data['original_filename']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* Get latest ticket ID
	*/
	public function lastInsertedID()
	{
		$result = $this->selectLastInsertID('tickets');
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

	public function getUser($user_id)
	{		
		$query = $this->selectSingleById('view_users','id',$user_id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function getReply($ticket_id)
	{		
		$query = $this->selectById('view_ticket_reply','ticket_id',$ticket_id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function deleteFile($id)
	{
		$query = $this->delete('attachment', $id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
}