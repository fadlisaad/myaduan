<?php

class Auth_model extends Model {

	/**
	* Get latest ticket ID
	*/
	public function lastInsertedID()
	{
		$result = $this->selectLastInsertID('users');
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}
	
	public function processRegisterUser($data)
	{
		$query = $this->insert('users', array(
			'email' => $data['email'],
			'password' => password_hash($data['password'], PASSWORD_DEFAULT),
			'permission' => $data['permission']
		));

		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function processRegisterContact($data)
	{
		$query = $this->insert('user_profile', array(
			'user_id' => $data['user_id'],
			'full_name' => $data['full_name'], 
			'age' => $data['age'], 
			'ic_passport' => $data['ic_passport'], 
			'ethnicity' => $data['ethnicity'], 
			'income' => $data['income'], 
			'address1' => $data['address1'], 
			'address2' => $data['address2'], 
			'postcode' => $data['postcode'], 
			'city_id' => $data['city_id'], 
			'state_id' => $data['state_id'], 
			'country_id' => $data['country_id'],
			'phone_no' => $data['phone_no']
		));

		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function processLogin($data)
	{
		$email = $data['email'];
		$password = $data['password'];
		$error_msg = NULL;
		
		$user = $this->selectSingleById('users','email',$email);
		$result = password_verify($password, $user[0]['password']);
		$success = ($result) ? 'true': 'false';

		if(empty($user)){
			return $error_msg = "Wrong e-mail.";
		}else{

			if($success == 'false') {
				return $error_msg = "Wrong password.";
			}else{
				// TODO: incomplete session storage
				$user_data = $this->selectCustom('id', 'users');
				if(empty($user_data)){
					return $error_msg = "Cannot create session.";
				}else{
					return $user_data;
				}
			}
		}

	}

	public function getUser($data)
	{
		$email = $data['email'];
		
		// Check if user exist
		$query = $this->selectSingleById('view_users','email',$email);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function checkEmail($email)
	{
		// Check if user exist
		$test_email = $this->selectSingleById('users','email',email);
		if(empty($test_email)){
			return false;
		}else{
			return $test_email;
		}
	}

	public function checkHash($hash)
	{
		// Check if hash exist
		$check_hash = $this->selectSingleById('user_hash','password_hash',$hash);
		if(empty($check_hash)){
			return false;
		}else{
			return $check_hash;
		}
	}

	public function storeHash($data)
	{
		$store_hash = $this->insert('user_hash',$data);
		if(empty($store_hash)){
			return false;
		}else{
			return $store_hash;
		}
	}

	public function resetPassword($data)
	{
		$query = $this->update('users', $data['id'], array(
			'password' => password_hash($data['password'], PASSWORD_DEFAULT),
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
}