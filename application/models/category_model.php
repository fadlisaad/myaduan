<?php

class Category_model extends Model {
	
	public function listSingle($id)
	{
		$result = $this->selectSingleById('view_categories','id',$id);
		return $result;
	}
	
	public function addRecord($data)
	{
		$query = $this->insert('complaint_categories', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function editRecord($data,$id)
	{
		$query = $this->update('complaint_categories', $id, $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function deleteRecord($id)
	{
		$query = $this->delete('complaint_categories', $id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
}