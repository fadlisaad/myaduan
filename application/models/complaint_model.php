<?php

class Complaint_model extends Model {

	/**
	* Get latest ID
	*/
	public function lastInsertedID()
	{
		$result = $this->selectLastInsertID('complaints');
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}
	
	public function listSingle($id)
	{
		$result = $this->selectSingleById('view_complaints','id',$id);
		$result2 = $this->selectSingleById('view_tickets','complaint_id',$id);
		$res = array_merge($result,$result2);
		return $res;
	}
	
	public function listSingle2($id)
	{
		$result = $this->selectSingleById('view_tickets','complaint_id',$id);
		return $result;
	}
	
	public function addRecord($data)
	{
		$query = $this->insert('complaints', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function editRecord($data,$id)
	{
		$query = $this->update('complaints', $id, $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	public function editRecordTicket($data,$id)
	{
		$query = $this->update('tickets',$id, $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function deleteRecord($id)
	{
		$query = $this->delete('complaints', $id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* Add attachment by complaint id
	*/
	public function insertAttachment($data, $id)
	{
		
		$query = $this->insert('attachment', array(
			'file_id' => $id,
			'file_type' => 'complaint',
			'filename' => $data['filename'],
			'size_in_bytes' => $data['size_in_bytes'],
			'size_in_mb' => $data['size_in_mb'],
			'mime' => $data['mime'],
			'original_filename' => $data['original_filename']
		));
		
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* List all attachment by complaint id
	*/
	public function listAttachment($id)
	{
		$result = $this->selectById('attachment','file_id', $id);
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

	/**
	* List single attachment by id
	*/
	public function listSingleAttachment($id)
	{
		$result = $this->selectSingleById('attachment','id', $id);
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

	/**
	* List admin
	*/
	public function listAdmin()
	{
		$result = $this->selectSingleById('users','permission', 'admin');
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

}