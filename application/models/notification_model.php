<?php

class Notification_model extends Model {
	
	public function listAll()
	{
		$query = $this->selectAll('notifications');
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function listSingle($user_id)
	{
		$query = $this->selectById('notifications','user_id',$user_id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function add($data)
	{
		$query = $this->insert('tracks', array(
			'user_id' => $_POST['user_id'],
			'notify_type' => $_POST['type'],
			'title' => $_POST['title'],
			'content' => $_POST['content']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function countSingle($id)
	{
		$result = $this->selectCountWhere('notifications', 'user_id', $id);
		return $result;
	}
	
}