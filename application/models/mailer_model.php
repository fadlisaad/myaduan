<?php

class Mailer_model extends Model {
	
	public function listAll()
	{
		$result = $this->selectAll('email_template');
		return $result;
	}
	
	public function listSingle($id)
	{
		$result = $this->selectSingleById('email_template','id',$id);
		return $result;
	}
	
	public function addRecord($data)
	{
		$query = $this->insert('email_template', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function addReply($data)
	{
		$query = $this->insert('email_reply', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* List all reply by email id
	*/
	public function listReply($id)
	{
		$result = $this->selectById('email_reply','ticket_id', $id);
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

	/**
	* Add attachment by ticket id
	*/
	public function insertAttachment($data, $id)
	{
		
		$query = $this->insert('email_attachment', array(
			'file_id' => $id,
			'filename' => $data['filename'],
			'size_in_bytes' => $data['size_in_bytes'],
			'size_in_mb' => $data['size_in_mb'],
			'mime' => $data['mime'],
			'original_filename' => $data['original_filename']
		));
		
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	/**
	* List all attachment by complaint id
	*/
	public function listAttachment($id)
	{
		$result = $this->selectById('email_attachment','file_id', $id);
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

	/**
	* List all email by ticket id
	*/
	public function listAssignedEmail($ticket_id)
	{
		$result = $this->selectById('ticket_email','ticket_id', $ticket_id);
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

	/**
	* List single attachment by id
	*/
	public function listSingleAttachment($id)
	{
		$result = $this->selectSingleById('email_attachment','id', $id);
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}
	
	public function editRecord($data,$id)
	{
		$query = $this->update('email_template', $id, $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function deleteRecord($id)
	{
		$query = $this->delete('email_template', $id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
}