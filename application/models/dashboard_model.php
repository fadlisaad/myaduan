<?php

class Dashboard_model extends Model {

	public function countDaily($status, $whereDate, $permission, $user_id)
	{
		if(in_array($permission, array('admin', 'officer', 'trainee'), true)){
			$query = $this->selectSQL("SELECT COUNT(id) AS total FROM view_tickets WHERE status = '$status' AND DATE($whereDate) = CURDATE() AND assigned_to = '".$user_id."'");
		}else{
			$query = $this->selectSQL("SELECT COUNT(id) AS total FROM view_tickets WHERE status = '$status' AND DATE($whereDate) = CURDATE()");
		}
		if(empty($query)){
			return '0';
		}else{
			return $query;
		}
	}

	public function countTotalTicket()
	{
		$query = $this->selectSQL("SELECT COUNT(id) as total FROM view_tickets");
		if(empty($query)){
			return '0';
		}else{
			return $query;
		}
	}

	public function countTotalUser()
	{
		$query = $this->selectSQL("SELECT COUNT(id) as total FROM view_users");
		if(empty($query)){
			return '0';
		}else{
			return $query;
		}
	}

	public function countUser()
	{
		$query = $this->selectSQL("SELECT COUNT(id) as total FROM view_users WHERE permission = 'user'");
		if(empty($query)){
			return '0';
		}else{
			return $query;
		}
	}
	
	public function countTotal($status, $permission, $user_id)
	{
		if(in_array($permission, array('admin', 'officer', 'trainee'), true)){
			$query = $this->selectSQL("select `view_tickets`.`status` AS `status`,count(`view_tickets`.`id`) AS `total` from `view_tickets` WHERE `view_tickets`.`status`='".$status."' AND `view_tickets`.`assigned_to`='".$user_id."' group by `view_tickets`.`status`");
		}else{
			$query = $this->selectSQL("select `view_tickets`.`status` AS `status`,count(`view_tickets`.`id`) AS `total` from `view_tickets` WHERE `view_tickets`.`status`='".$status."' group by `view_tickets`.`status`");
		}
		if(empty($query)){
			return '0';
		}else{
			return $query;
		}
	}
	
	
	
	
	public function countStaff($permission, $user_id)
	{
		if(in_array($permission, array('admin', 'officer', 'trainee'), true)){
			$query = $this->selectSQL("select `view_tickets`.`status` AS `status`,`view_tickets`.`assigned_name` AS `assigned_name`,count(`view_tickets`.`assigned_name`) AS `total` from `view_tickets` where `view_tickets`.`assigned_to` = '".$user_id."' AND (`view_tickets`.`assigned_name` is not null) group by `view_tickets`.`assigned_name`,`view_tickets`.`status`");
		}else{
			$query = $this->selectSQL("select `view_tickets`.`status` AS `status`,`view_tickets`.`assigned_name` AS `assigned_name`,count(`view_tickets`.`assigned_name`) AS `total` from `view_tickets` where (`view_tickets`.`assigned_name` is not null) group by `view_tickets`.`assigned_name`,`view_tickets`.`status`");
		}
		
		if(empty($query)){
			return '0';
		}else{
			return $query;
		}
	}
	public function countStaffRow($permission, $user_id)
	{
		if(in_array($permission, array('admin', 'officer', 'trainee'), true)){
			$query = $this->selectSQL("select (SELECT count(*) from `view_tickets` as `v` where `v`.`status` = 'Submitted' and `v`.`assigned_name` = `view_tickets`.`assigned_name`) as `Submitted`, (SELECT count(*) from `view_tickets` as `v` where `v`.`status`='Open' and `v`.`assigned_name` = `view_tickets`.`assigned_name`) as `Open`, (SELECT count(*) from `view_tickets` as `v1` where `v1`.`status` = 'processing' and `v1`.`assigned_name` = `view_tickets`.`assigned_name`) as `Processing`, (SELECT count(*) from `view_tickets` as `v2` where `v2`.`status` = 'closed' and `v2`.`assigned_name` = `view_tickets`.`assigned_name`) as `Closed`, `view_tickets`.`assigned_name` AS `assigned_name` from `view_tickets` where `view_tickets`.`assigned_to` = '".$user_id."' group by `view_tickets`.`assigned_name`");
		}else{
			$query = $this->selectSQL("select (SELECT count(*) from `view_tickets` as `v` where `v`.`status` = 'Submitted' and `v`.`assigned_name` = `view_tickets`.`assigned_name`) as `Submitted`, (SELECT count(*) from `view_tickets` as `v` where `v`.`status` = 'Open' and `v`.`assigned_name` = `view_tickets`.`assigned_name`) as `Open`, (SELECT count(*) from `view_tickets` as `v1` where `v1`.`status` = 'processing' and `v1`.`assigned_name` = `view_tickets`.`assigned_name`) as `Processing`, (SELECT count(*) from `view_tickets` as `v2` where `v2`.`status` = 'closed' and `v2`.`assigned_name` = `view_tickets`.`assigned_name`) as `Closed`, `view_tickets`.`assigned_name` AS `assigned_name` from `view_tickets` where (`view_tickets`.`assigned_name` is not null) group by `view_tickets`.`assigned_name`");
		}
		
		if(empty($query)){
			return '0';
		}else{
			return $query;
		}
	}
	
	
}