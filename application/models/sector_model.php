<?php

class Sector_model extends Model {
	
	public function listSingle($id)
	{
		$result = $this->selectSingleById('complaint_sectors','id',$id);
		return $result;
	}
	
	public function addRecord($data)
	{
		$query = $this->insert('complaint_sectors', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function editRecord($data,$id)
	{
		$query = $this->update('complaint_sectors', $id, $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function deleteRecord($id)
	{
		$query = $this->delete('complaint_sectors', $id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
}