<?php

class User_model extends Model {
	
	/**
	* Get latest ID
	*/
	public function lastInsertedID()
	{
		$result = $this->selectLastInsertID('users');
		if(empty($result)){
			return false;
		}else{
			return $result;
		}
	}

	public function listUser()
	{
		$query = $this->selectAll('view_users');
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function listSingleUser($id)
	{
		$query = $this->selectSingleById('view_users','id',$id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function listSingleUserByEmail($email)
	{
		$query = $this->selectSQL("SELECT full_name, designation FROM view_users WHERE email = '$email' LIMIT 1");
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function getCustomUser($id)
	{
		$query = $this->selectSQL("SELECT full_name, designation FROM view_users LEFT JOIN tickets ON tickets.close_by = view_users.id WHERE tickets.id = '$id' LIMIT 1");
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function addUser($data)
	{
		$query = $this->insert('users', array(
			'email' => $data['email'],
			'password' => password_hash($data['password'], PASSWORD_DEFAULT),
			'permission' => $data['permission']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function addRole($data)
	{
		$query = $this->insert('user_roles', array(
			'user_id' => $data['user_id'],
			'sector_id' => $data['sector_id']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function checkRole($data)
	{
		$user = $data['user_id'];
		$sector = $data['sector_id'];

		$query = $this->selectSQL("SELECT COUNT(id) AS id FROM user_roles WHERE user_id = '$user' AND sector_id = '$sector'");
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function listSector()
	{
		$query = $this->selectAll('complaint_sectors');
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function addUserProfile($data)
	{
		$query = $this->insert('user_profile', array(
			'user_id' => $data['user_id'],
			'full_name' => $data['full_name']
		));

		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function editUser($data,$id)
	{
		$query = $this->update('users', $id, array(
			'password' => password_hash($data['password'], PASSWORD_DEFAULT),
			'permission' => $data['permission']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function editUserProfile($data, $id)
	{
		
		$query = $this->update('user_profile', $id, array(
			'full_name' => $data['full_name']
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function selfEditUser($data)
	{
		$query = $this->update('users', $data['id'], array(
			'password' => password_hash($data['password'], PASSWORD_DEFAULT)
		));
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}

	public function editAvatar($data)
	{
		$query = $this->updateWhere('user_profile', 'user_id', $data['user_id'], array(
			'avatar' => $data['avatar'])
		);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function deleteUser($id)
	{
		$query = $this->delete('users', $id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
}