<?php

class Type_model extends Model {
	
	public function listSingle($id)
	{
		$result = $this->selectSingleById('view_types','id',$id);
		return $result;
	}
	
	public function addRecord($data)
	{
		$query = $this->insert('complaint_types', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function editRecord($data,$id)
	{
		$query = $this->update('complaint_types', $id, $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function deleteRecord($id)
	{
		$query = $this->delete('complaint_types', $id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
}