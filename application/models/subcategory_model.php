<?php

class Subcategory_model extends Model {
	
	public function listSingle($id)
	{
		$result = $this->selectSingleById('view_subcategories','id',$id);
		return $result;
	}
	
	public function addRecord($data)
	{
		$query = $this->insert('complaint_subcategories', $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function editRecord($data,$id)
	{
		$query = $this->update('complaint_subcategories', $id, $data);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
	
	public function deleteRecord($id)
	{
		$query = $this->delete('complaint_subcategories', $id);
		if(empty($query)){
			return false;
		}else{
			return $query;
		}
	}
}