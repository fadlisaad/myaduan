<?php

	// Defines
	define('ROOT_DIR', realpath(dirname(__FILE__)) .'/');
	define('APP_DIR', ROOT_DIR .'application/');

	//Start the Session
	session_save_path(ROOT_DIR.'tmp');
	session_start();

	// Includes
	require(APP_DIR .'config/config.php');
	require(ROOT_DIR .'system/model.php');
	require(ROOT_DIR .'system/view.php');
	require(ROOT_DIR .'system/controller.php');
	require(ROOT_DIR .'system/pip.php');

	// Autoload by composer
	require(ROOT_DIR .'vendor/autoload.php');

	// Define base URL
	global $config;
	define('BASE_URL', $config['base_url']);
	define('SITE_TITLE', $config['site_title']);
	define('HASH_PASSWORD', $config['pw_hash']);
	pip();