$(document).ready(function($) {
	
	// Global back button
	$("#back").bind("click", function(event){
		window.history.back();
	});

	// Search ticket by ID
	$("#searchTicket").click(function(){
		var q = $("#q").val();
		$.ajax({
                url : 'ticket/searchTicket',
                data : 'q='+ q,
                type : 'POST'
            })
    });

    // Remove duplicate option
	/*var seen = {};
	$('option').each(function() {
		var txt = $(this).val();
		if (seen[txt])
			$(this).remove();
		else
			seen[txt] = true;
	});*/

});