//Flot Multiple Axes Line Chart
$(function() {

    function euroFormatter(v, axis) {
        return v.toFixed(axis.tickDecimals) + "MYR";
    }

    function doPlot(position) {
        $.plot($("#flot-line-chart-multi"), [{
            data: exchangerates,
            label: "USD/MYR exchange rate",
            yaxis: 2
        }], {
            xaxes: [{
                mode: 'time',
				timeformat: "%d/%m",
				tickLength: 7
            }],
            yaxes: [{
                min: 0
            }, {
                // align if we are to the right
                alignTicksWithAxis: position == "right" ? 1 : null,
                position: position,
                tickFormatter: euroFormatter
            }],
            legend: {
                position: 'ne'
            },
            grid: {
                hoverable: true //IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s for %x was %y",
                xDateFormat: "%d/%m/%y",

                onHover: function(flotItem, $tooltipEl) {
                    // console.log(flotItem, $tooltipEl);
                }
            }

        });
    }

    doPlot("right");

    $("button").click(function() {
        doPlot($(this).text());
    });
});