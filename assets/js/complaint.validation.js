!function($) {
    "use strict";

    var FormValidator = function() {
        this.$submitForm = $("#add-ticket");
    };

    //init
    FormValidator.prototype.init = function() {

        // validate signup form on keyup and submit
        this.$submitForm.validate({
            rules: {
                details: {
                    required: true,
                    minlength: 100
                },
                incident_date: "required",
                incident_time: "required",
                estimated_loss: "required",
                state_id: "required",
                city_id: "required",
                company_email: {
                    email: true
                }
            },
            messages: {
                details:  {
                    required: "Please enter your complaint details here",
                    minlength: "Your complaint must not be less than 100 characters"
                },
                incident_date: "Please enter date of the incident",
                incident_time: "Please enter time of the incident",
                state_id: "Please choose the state",
                city_id: "Please choose nearest city",
                estimated_loss: "Please enter your estimated loss, in RM",
                company_email: "Please enter a valid email address"
            }
        });
    },
    //init
    $.FormValidator = new FormValidator, $.FormValidator.Constructor = FormValidator
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.FormValidator.init()
}(window.jQuery);