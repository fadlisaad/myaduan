// click to copy to clipboard, works on all browser except mobile Safari and IE :(
function copyToClipboard(element) {
	var temp = $("<div>");
	$("body").append(temp);
	temp.val($(element).val()).select();
	document.execCommand("copy");
	temp.remove();
}

$(document).ready(function(){

	$('#copyToClipboard').on('click',function(){

		// try to detect if the browser is mobile safari
		var isIphone = /(iPhone)/i.test(navigator.userAgent);
		var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);

		if(isIphone && isSafari){
			swal({
				title: 'Instruction for iPhone/iPad',
				text: REFERER_URL + '<br>To copy the content, please long press on the content above and choose Copy.',
				type: 'info',
				html: true
			});
		}else{
			copyToClipboard('#content-to-copy');
			swal({
				title: 'Copied',
				text: 'The content has been copied to your clipboard.',
				type: 'success',
				html: true
			});
		}
	});

});