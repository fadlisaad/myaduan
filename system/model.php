<?php

class Model {

	private $connection;

	public function __construct()
	{
		global $config;

		$this->connection = new mysqli($config['db_host'], $config['db_username'], $config['db_password'], $config['db_name']);
		if(!$this->connection) {
			die("connection error: " . mysqli_connect_error);
		}
	}
	
	/* 
	 * Select all item in table
	 */
	public function selectAll($table)
	{
		$result = $this->connection->query("SELECT * FROM $table");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_object()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}

	/* 
	 * Select all and count
	 */
	public function selectCount($table, $id)
	{
		$result = $this->connection->query("SELECT $id FROM $table");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			return mysqli_num_rows($result);
		}
		
	}

	/* 
	 * Select count with where
	 */
	public function selectCountWhere($table, $where, $id)
	{
		$result = $this->connection->query("SELECT * FROM $table WHERE $where = '$id'");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_assoc()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}
	
	/* 
	 * Select single item in table
	 */
	public function selectSingleById($table, $where, $id, $limit = 1)
	{
		$result = $this->connection->query("SELECT * FROM $table WHERE $where = '$id' LIMIT $limit");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_assoc()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}
	
	/* 
	 * Select single item in table with extra params
	 */
	public function selectSingleByWhere($table, $where1, $param1, $where2, $param2, $limit = 1)
	{
		$result = $this->connection->query("SELECT * FROM $table WHERE $where1 = '$param1' AND $where2 = '$param2' LIMIT $limit");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_assoc()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}
	
	/* 
	 * Select all item by id using where
	*/
	public function selectById($table, $where, $id)
	{
		$result = $this->connection->query("SELECT * FROM $table WHERE $where = '$id'");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_object()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}
	
	/* 
	 * Select condition by id using where with join
	*/
	public function selectByJoin($what, $table, $join, $where, $id)
	{
		$result = $this->connection->query("SELECT $what FROM $table $join WHERE $where = '$id'");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_object()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}
	
	/* 
	 * Select single row with condition by id using where with join
	*/
	public function selectSingleByJoin($what, $table, $join, $where, $id)
	{
		$result = $this->connection->query("SELECT $what FROM $table $join WHERE $where = '$id' LIMIT 1");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_assoc()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}

	/* 
	 * Select all row with condition group by
	*/
	public function selectByGroup($what, $table, $group)
	{
		$result = $this->connection->query("SELECT $what FROM $table GROUP BY $group");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_assoc()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}

	/* 
	 * Select all row with custom filter
	*/
	public function selectCustom($what, $table)
	{
		$result = $this->connection->query("SELECT $what FROM $table");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_assoc()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}

	/* 
	 * Select custom query and condition
	*/
	public function selectSQL($query)
	{
		$result = $this->connection->query("$query");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_assoc()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}

	/* 
	 * Select custom query and count
	*/
	public function selectCountSQL($query)
	{
		$result = $this->connection->query("$query");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			return mysqli_num_rows($result);
		}
		
	}
	
	/* 
	 * Insert item
	 */
	public function insert($table, $data)
	{
		// build query...
		$sql  = "INSERT INTO $table";
		
		// implode keys of $array...
		$sql .= " (`".implode("`, `", array_keys($data))."`)";
		
		// implode values of $array...
		$sql .= " VALUES ('".implode("', '", $data)."') ";
		
		// execute query...
		$result = $this->connection->query($sql) or die(mysqli_error($this->connection));
		
		//die(var_dump($sql));
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			return;
		}
		
	}
	
	/* 
	 * Update item
	 */
	public function update($table, $id, $data)
	{
		$fieldDetails = NULL;
		foreach($data as $key => $value){
			$fieldDetails .= "`$key` = '$value',";
		}
		$fieldDetails = rtrim($fieldDetails, ',');
		
		// build query...
		$sql  = "UPDATE $table SET $fieldDetails WHERE `id` = $id";
		
		//die(var_dump($sql));
		// execute query...
		$result = $this->connection->query($sql) or die(mysqli_error($this->connection));
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			return;
		}
		
	}

	/* 
	 * Update with custom id item
	 */
	public function updateWhere($table, $where, $id, $data)
	{
		$fieldDetails = NULL;
		foreach($data as $key => $value){
			$fieldDetails .= "`$key` = '$value',";
		}
		$fieldDetails = rtrim($fieldDetails, ',');
		
		// build query...
		$sql  = "UPDATE $table SET $fieldDetails WHERE $where = '$id'";
		
		//die(var_dump($sql));
		// execute query...
		$result = $this->connection->query($sql) or die(mysqli_error($this->connection));
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			return;
		}
		
	}
	
	/* 
	 * Delete item
	 */
	public function delete($table, $id)
	{
		$sql = "DELETE FROM $table WHERE `id` = $id LIMIT 1";
		$result = $this->connection->query($sql);
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			return;
		}
		
	}
	
	/* 
	 * Last item
	 */
	public function selectLast($table)
	{
		$sql = "SELECT * FROM $table ORDER BY `id` DESC LIMIT 1";
		$result = $this->connection->query($sql);
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_assoc()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}

	/* 
	 * Last inserted ID
	 */
	public function selectLastInsertID($table)
	{
		$sql = "SELECT `id` FROM $table ORDER BY `id` DESC LIMIT 1";
		$result = $this->connection->query($sql);
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			$resultObjects = array();
			while($row = $result->fetch_assoc()) $resultObjects[] = $row;
			return $resultObjects;
		}
		
	}

	/* 
	 * Last inserted ID
	 */
	public function selectSingleSQL($query)
	{
		$result = $this->connection->query("$query");
		if(!$result){
			die(mysqli_error($this->connection));
		}else{
			return;
		}
		
	}

}